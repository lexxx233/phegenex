<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<html>
<head>
<title>Gene Finder</title>
<%@ page import="java.io.*"%>
<%@ page import="java.util.*" %>
<%@ page import="com.ppi.*"%>
<%@ page import="com.ppi.PhenotypeFinder"%>
<%@ page import="com.ppi.GenetypeFinder"%>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="com.ppi.web.LogHelper" %>

<script type="text/javascript" src="./js/json2.min.js"></script>
<script type="text/javascript" src="./js/AC_OETags.min.js"></script>
<script type="text/javascript" src="./js/cytoscapeweb.min.js"></script>

<style>
	#cytoscapeweb { width: 100%; height: 100%;}
</style>

<link href="css/style.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
	function show_full_list(){
		document.getElementById("phenotype_full_list").style.display = "block";
		document.getElementById("phenotype_abstract_list").style.display = "none";
	}
	function show_abstract_list(){
		document.getElementById("phenotype_abstract_list").style.display = "block";
		document.getElementById("phenotype_full_list").style.display = "none";
	}
        
        
        function calcHeight(){
                    //find the height of the internal page
                    var the_height= parent.document.getElementById('frmContent').contentWindow.document.body.scrollHeight;

                    //change the height of the iframe
                    parent.document.getElementById('frmContent').height= the_height;
                }
              
        var ori_height = parent.document.getElementById('frmContent').contentWindow.document.body.scrollHeight;
        function show_cytoscape(){
            document.getElementById('showcytoscape').style.display = "block";
            document.getElementById('show1').style.visibility = 'hidden';
            calcHeight();
        }
        
        function hide_cytoscape(){
            document.getElementById('showcytoscape').style.display = "none";
            document.getElementById('show1').style.visibility = 'visible';
            parent.document.getElementById('frmContent').height= ori_height;
        }
        
	</script>
</head>


<body>
<%
//        Logger log = Logger.getLogger(ContextManager.class);
//	log.info(LogHelper.GetLogItemFromRequest(request));
        
        ArrayList<Entry> PhenotypeNetwork = ContextManager.getPhenotypeEntry();
        ArrayList<String> geneName = ContextManager.getGeneName();
        ArrayList<Entry> GeneNetwork = ContextManager.getGeneEntry();

        ArrayList<MatchedGeneEntry> fmg = new ArrayList<MatchedGeneEntry>();

	int AlgId = 0;
	int TopK = 10;
	AlgId = Integer.parseInt(request.getParameter("AlgId"));
	try{
		TopK = Integer.parseInt(request.getParameter("txtTopK"));
	}catch (Exception e){
		response.sendRedirect("error.jsp?code=501");
		if (true) return;
	}

	GenetypeFinder gf = new GenetypeFinder();
        
	ArrayList<Phenotype> phenotype = gf.ValidatePhenotypes(request.getParameter("Phenotypes"));
	if (phenotype.size() == 0)
	{
		response.sendRedirect("error.jsp?code=502");
		if (true) return;
	}
	ArrayList<MatchedGeneEntry> mge = gf.SearchByPhenotype(phenotype, AlgId);
        //add by zhanghy at 20120903
        
        int regionFlag = Integer.parseInt(request.getParameter("Region"));
        if (regionFlag == 2)
        {
            String regionInfo = request.getParameter("ChrRegions");
            mge = gf.getSpecifiedGenes(mge, regionInfo);
        }
        if(TopK > mge.size())
        {
            TopK = mge.size();
        }
%>

<table width="90%" border="0" align="center">
<!--The 1st row for listing header of the page -->
    <tr>
    	<td>
    	<div id="phenotype_full_list" style="display:none" >
            <font size="4">Query Phenotypes:</font>
			<%
				int i = 0;
				for ( i = 0 ; i < phenotype.size() - 1 ; i ++)
				{
					Phenotype p =phenotype.get(i);
			%>
					&nbsp;&nbsp;<a href="http://omim.org/entry/<%=p.OMIM_ID %>" target="_blank"><%=p.OMIM_ID %></a>&nbsp;&nbsp;|
			<%
				}
			%>
			&nbsp;&nbsp;<a href="http://omim.org/entry/<%=phenotype.get(i).OMIM_ID %>" target="_blank"><%=phenotype.get(i).OMIM_ID %></a>
			&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript:show_abstract_list()" style="color:red">LESS</a>
		</div>
		<div id="phenotype_abstract_list" style="display:block" >
                    <font size="4">Query Phenotype set:</font>
			<%
				int size = (phenotype.size() > 5) ? 5 : phenotype.size();
				for ( i = 0 ; i < size - 1 ; i ++)
				{
					Phenotype p =phenotype.get(i);
			%>
					&nbsp;&nbsp;<a href="http://omim.org/entry/<%=p.OMIM_ID %>" target="_blank"><%=p.OMIM_ID %></a>&nbsp;&nbsp;|
			<%
				}
			%>
			&nbsp;&nbsp;<a href="http://omim.org/entry/<%=phenotype.get(i).OMIM_ID %>" target="_blank"><%=phenotype.get(i).OMIM_ID %></a>
			<%  if (phenotype.size() > 5 ) { %>
				&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript:show_full_list()" style="color:red">MORE</a>
			<%} %>
		</div>
		</td>
  	</tr>
        
    <!-- The 2nd row for listing a 3-columns information-->
    
    <!-- <tr><td><form id="goform" action="GOanalysis.jsp" target="_blank"><input type="button" value="Run GO Analysis >" style="color:#005FA9;float:right; font-weight:bold" onclick="runGO()"/><input type="hidden" id="GOgenes"/></form></td></tr> -->
    <tr>
    
    	<td  align="left" width="80%">
            
			<table class="valueListTableStyle" id="resulttable">
				<tr>
                                    <td width = 40>Rank</td><td width = 100>Gene Name</td><td width = 0></td><td width="440">Known Disease Association</td><td width = 70>Relevance<br>Score</td>
				</tr>
			<% 	for (i = 0 ; i < TopK ; i ++)
				{
					MatchedGeneEntry smge =(MatchedGeneEntry)mge.get(i);
                                        fmg.add(smge);
			%>
					<tr id="row_<%=i+1%>">
						<td>
							<%=(i + 1) %>
						</td>

						<td><a href = "genefo.jsp?genename=<%= smge.GeneName%>" target="_blank"><%= smge.GeneName %></a></td>
                                                <td>
                                                    
                                                </td>
                                                <td>
                                                    <% if(smge.ism == true){%>
                                                    <%= smge.disease %>
                                                    <a href="http://www.omim.org/entry/<%=smge.OMIM_ID%>" target="_blank"> &nbsp(more)</a>
                                                    <% }else{%>
                                                    ---
                                                    <%}%>
                                                    
                                                </td>
						<td>
							<%=String.format("%1.2e",smge.RelScore) %> <!---"%1.2g" -->
						</td>
                                                <td style="display:none"><%= smge.GeneName%></td>
					</tr>
			<%
				}
			%>

                        
                        <% // Generate genes node
            int displayNum = 30;
            if(displayNum > mge.size())
            {
                displayNum = mge.size();
            }
            ArrayList<MatchedGeneEntry> matchedgenes = gf.getDisplayGene(mge, displayNum);
            ArrayList<MatchedPhenotypeEntry> matchedphenotypes = gf.getDisplayPhenotype(request.getParameter("Phenotypes"), displayNum);

            ArrayList<Edge> GG = gf.getGeneEdges(matchedgenes);
            ArrayList<Edge> GP = gf.getNetworkEdges(matchedgenes, matchedphenotypes);
            ArrayList<Edge> PP = gf.getPhenotypeEdges(matchedphenotypes);
            
            
            gf.getAllQueryNetworkEdges(matchedgenes, matchedphenotypes, GP);

            
            matchedgenes = gf.deleteAloneGenes(matchedgenes, GG, GP);
            matchedphenotypes = gf.deleteAlonePhenotype(matchedphenotypes, PP, GP);
            

        %>

       
       
        
      </table>

                <table width="90%" border="0" align="center">

                    <tr><td><span id="assoc"></span></td></tr>
                    <tr><td height="530px" valign="top"><div id="cytoscapeweb">Visualizing... Please wait...</div></td></tr>
                    <tr><td><span id="legend"></span></td></tr>

                    <tr>
            <td>
            <div id="showcytoscape" style="display:none" >
                <input id="hide1" type="button" onclick="hide_cytoscape()" value ="Hide Cytoscape file"></input>
                <br>
                <br>
                <% for(int x = 0; x < GG.size(); x++){ %>
                <%= GG.get(x).getSource() %> &nbsp; <%= GG.get(x).getTarget() %><br>
                <% } %>
                <% for(int x = 0; x < GP.size(); x++){ %>
                <%= GP.get(x).getSource() %> &nbsp; OMIM_<%= GP.get(x).getTarget() %><br>
                <% } %>
                <% for(int x = 0; x < PP.size(); x++){ %>
                OMIM_<%= PP.get(x).getSource() %> &nbsp; OMIM_<%= PP.get(x).getTarget() %><br>
                <% } %>
                <br>
                <input id="hide2" type="button" onclick="hide_cytoscape()" value ="Hide Cytoscape file"></input>
            </div>
                <input id="show1" type="button" onclick="show_cytoscape()" value="Show Cytoscape file"></input>
            </td>
        </tr>
                </table>

<script type="text/javascript">
	window.onload = function() {
	// id of Cytoscape Web container div
	var div_id = "cytoscapeweb";

<% if(PP.size() !=0 && GP.size() != 0 && GG.size() != 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int h = 0; h < matchedphenotypes.size(); h ++){ %><node id="<%= matchedphenotypes.get(h).OMIM_ID%>"><data key="label"><%= matchedphenotypes.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';
<% } %>
<% if(PP.size() !=0 && GP.size() == 0 && GG.size() != 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int h = 0; h < matchedphenotypes.size(); h ++){ %><node id="<%= matchedphenotypes.get(h).OMIM_ID%>"><data key="label"><%= matchedphenotypes.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               </graph>\
	</graphml>\
	';
        document.getElementById('assoc').innerHTML = "INFO: There is no Genes - Phenotypes association to visualize";
<% } %>
<% if(GG.size() ==0 && GP.size() == 0 && PP.size() != 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedphenotypes.size(); h ++){ %><node id="<%= matchedphenotypes.get(h).OMIM_ID%>"><data key="label"><%= matchedphenotypes.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               </graph>\
	</graphml>\
	';
        document.getElementById('assoc').innerHTML = "INFO: There is no Genes-Genes/ Genes - Phenotypes association to visualize";
<% } %>
<% if(PP.size() ==0 && GP.size() == 0 && GG.size() != 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               </graph>\
	</graphml>\
	';
        document.getElementById('assoc').innerHTML = "INFO: There is no Phenotypes-Phenotypes / Genes - Phenotypes association to visualize";
<% } %>
	var visual_style =
	{
		global: { backgroundColor: "#EDEDED" },
		nodes:
		{
			labelFontSize: 16.5,
			shape: {
				passthroughMapper: { attrName: "s"}
			},
			color: {
				passthroughMapper: { attrName: "c"}
			},
			borderWidth: 1,
			borderColor: "#EDEDED",
			size:
			{
				defaultValue: 66,
				continuousMapper: { attrName: "weight", minValue: 66, maxValue: 120}
			},
			labelHorizontalAnchor: "center"

		},
		edges: { width: 2, color: "#0B94B1" }
	};

	// initialization options
	var options =
	{
		swfPath: "./swf/CytoscapeWeb",
		flashInstallerPath: "./swf/playerProductInstall"
	};

	var vis = new org.cytoscapeweb.Visualization(div_id, options);

	// callback when Cytoscape Web has finished drawing
	vis.ready(function()
	{
		// add a listener for when nodes and edges are clicked
		vis.addListener("click", "nodes", function(event)
		{
			handle_click(event);
		});
                
                vis.addListener("mouseover", "nodes", function(event)
		{
			handle_mouseover(event);
		});

                vis.addListener("mouseout", "nodes", function(event)
		{
			handle_mouseout(event);
		});

	function handle_click(event)
	{
            var temp = event.target;
            var temp2 = temp.data["s"];
            if(temp2=="ROUNDRECT"){
		var target = event.target;
                window.open("http://omim.org/entry/"+target.data["id"], "OMIM", "channelmode=yes");
            }else{
            	var target = event.target;
                window.open("genefo.jsp?genename="+target.data["id"], "GENE", "channelmode=yes");
            }
	}
        
        var mostcurrent_i;

        function handle_mouseover(event)
        {
            
            var temp = event.target;
            var temp2 = temp.data["s"];
            
            if(temp2=="CIRCLE"){
                
                table = document.getElementById('resulttable');
                
                for(var i = 1; i < table.rows.length; i++){
                    if(table.rows[i].cells[5].firstChild.nodeValue == temp.data["label"]){
                          var idx = "row_" + i;
                          
                          document.getElementById(idx).style.color = "red";
                          document.getElementById(idx).style.fontWeight = "bold";
                          mostcurrent_i = idx;
                          break;
                    }
                }
            }

        }

        function handle_mouseout(event)
        {
           table = document.getElementById('resulttable');

                for(var i = 1; i < table.rows.length; i++){
                          var idx = "row_" + i;

                          document.getElementById(idx).style.color = "black";
                          document.getElementById(idx).style.fontWeight = "normal";

                }
        }


	});

	// draw options
	var draw_options =
	{
		// your data goes here
		network: xml,

		// hide pan zoom
		panZoomControlVisible: true,

		// show edge labels too
		edgeLabelsVisible: false,

 		edgesMerged: true,

		// set the style at initialisation
		visualStyle: visual_style,

		// let's try another layout
		layout: "Tree"
	};


	vis.draw(draw_options);
        ori_height = parent.document.getElementById('frmContent').contentWindow.document.body.scrollHeight;


//find the height of the internal page
                    var the_height= document.body.clientHeight;

                    //change the height of the iframe
                    parent.document.getElementById('frmContent').height= the_height;

};
/*
function runGO(){
    table = document.getElementById('resulttable');
                
                for(var i = 1; i < table.rows.length; i++){
                    if(i == 1){
                        document.getElementById('GOgenes').value = table.rows[i].cells[5].firstChild.nodeValue;
                        continue;
                    }
                    document.getElementById('GOgenes').value = document.getElementById('GOgenes').value + "," + table.rows[i].cells[5].firstChild.nodeValue;

                }
                
    document.getElementById('goform').submit();
    
}
*/
</script>

</body>
</html>