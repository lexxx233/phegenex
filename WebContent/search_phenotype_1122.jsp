<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page isThreadSafe="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>Insert title here</title>
<%@ page import="java.io.*"%>
<%@ page import="java.util.*" %>
<%@ page import="com.ppi.*"%>
<%@ page import="com.ppi.PhenotypeFinder"%>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="com.ppi.web.LogHelper" %>

<script type="text/javascript" src="./js/json2.min.js"></script>
<script type="text/javascript" src="./js/AC_OETags.min.js"></script>
<script type="text/javascript" src="./js/cytoscapeweb.min.js"></script>

<style>
	#cytoscapeweb { width: 100%; height: 100%;}
</style>

<link href="css/style.css" rel="stylesheet" type="text/css"/>

	<script type="text/javascript">
	function show_full_list(){
		document.getElementById("gene_full_list").style.display = "block";
		document.getElementById("gene_abstract_list").style.display = "none";
	}
	function show_abstract_list(){
		document.getElementById("gene_abstract_list").style.display = "block";
		document.getElementById("gene_full_list").style.display = "none";
	}
        function show_full_list_others(){
		document.getElementById("gene_full_list_others").style.display = "block";
		document.getElementById("gene_abstract_list_others").style.display = "none";
	}
	function show_abstract_list_others(){
		document.getElementById("gene_abstract_list_others").style.display = "block";
		document.getElementById("gene_full_list_others").style.display = "none";
	}
        
        
        function calcHeight(){
                    //find the height of the internal page
                    var the_height= parent.document.getElementById('frmContent').contentWindow.document.body.scrollHeight;

                    //change the height of the iframe
                    parent.document.getElementById('frmContent').height= the_height;
                }
              
        var ori_height = parent.document.getElementById('frmContent').contentWindow.document.body.scrollHeight;
        
        function show_cytoscape(){
            document.getElementById('showcytoscape').style.display = "block";
            document.getElementById('show1').style.visibility = 'hidden';
            calcHeight();
        }
        
        function hide_cytoscape(){
            document.getElementById('showcytoscape').style.display = "none";
            document.getElementById('show1').style.visibility = 'visible';
            parent.document.getElementById('frmContent').height= ori_height;
        }
        
	</script>

</head>


<body>
    
<%

    Logger log = Logger.getLogger(ContextManager.class);
    //log.info(LogHelper.GetLogItemFromRequest(request));
    ArrayList<Entry> PhenotypeNetwork = ContextManager.getPhenotypeEntry();
    ArrayList<String> geneName = ContextManager.getGeneName();
    ArrayList<Entry> GeneNetwork = ContextManager.getGeneEntry();
    ArrayList<MatchedPhenotypeEntry> fmpe = new ArrayList<MatchedPhenotypeEntry>();
    
    int AlgId = 0;
    int TopK = 10;
    String querygene = "";
    if(request.getParameter("m_AlgId") != null){
         AlgId = Integer.parseInt(request.getParameter("m_AlgId"));
         TopK = Integer.parseInt(request.getParameter("m_TopK")); 
         if(TopK>5080){
                    TopK = 5080;
                }
         querygene = request.getParameter("m_gene");   
    }
    else{
        AlgId = Integer.parseInt(request.getParameter("AlgId"));
        try{
                TopK = Integer.parseInt(request.getParameter("txtTopK"));
                if(TopK>5080){
                    TopK = 5080;
                }

        }catch (Exception e){
                response.sendRedirect("error.jsp?code=501");
                if (true) return;
        }
        querygene = request.getParameter("Genes");
    }
    PhenotypeFinder pf = new PhenotypeFinder();

    ArrayList<Gene> genes = pf.ValidateGenes(querygene);
    if (genes == null){
        response.sendRedirect("error.jsp?code=503");
        if (true) return;
    }
    if (genes.size() == 0)
    {
            response.sendRedirect("error.jsp?code=502");
            if (true) return;
    }

    //Because Local variables are used : AlgId, TopK, and genes, then passed to this is threadsafe
    //<%! are JSP declaration tag
    ArrayList<MatchedPhenotypeEntry> mpes = pf.SearchByGenes(genes, AlgId);

%>
<table width="90%" border="0" align="center">
<!--The 1st row for listing header of the page -->
<tr>
    <td>
        <div style ="float:right">
         <form id="downloadfile" name="downloadfile" enctype="multipart/form-data" action="export.jsp" method="post"  target="_blank">
                <input name="btn_download" type="button" value="download the result"  onclick="download_file()"/>
        </form>
            </div>
    </td>
</tr>
    <tr>
    
    	<td>
        
    	<div id="gene_full_list" style="display:none" >
            <font size="4">Query Genes:</font>
			<%
				int i = 0;
				for ( i = 0 ; i < genes.size() - 1 ; i ++)
				{
					Gene g =genes.get(i);
			%>
					&nbsp;&nbsp;<a href="genefo.jsp?genename=<%=g.Name %>" target="_blank"><%=g.Name %></a>&nbsp;&nbsp;|
			<%
				}
			%>
			&nbsp;&nbsp;<a href="genefo.jsp?genename=<%=genes.get(i).Name %>" target="_blank"><%=genes.get(i).Name%></a>
			&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript:show_abstract_list()" style="color:red">LESS</a>
		</div>
		<div id="gene_abstract_list" style="display:block" >
                    <font size="4">Query Genes:</font>
			<%
				int size = (genes.size() > 5) ? 5 : genes.size();
				for ( i = 0 ; i < size - 1 ; i ++)
				{
					Gene g =genes.get(i);
			%>
					&nbsp;&nbsp;<a href="genefo.jsp?genename=<%=g.Name %>" target="_blank"><%=g.Name %></a>&nbsp;&nbsp;|
			<%
				}
			%>
			&nbsp;&nbsp;<a href="genefo.jsp?genename=<%=genes.get(i).Name %>" target="_blank"><%=genes.get(i).Name%></a>
			<%  if (genes.size() > 5 ) { %>
				&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript:show_full_list()" style="color:red">MORE</a>
			<%} %>
		</div>
		</td>
  	</tr>
        <tr>
            <td>
                <% 
                 ArrayList<Gene> othergenes = pf.getOtherGenes(querygene);
                if(othergenes!=null&&othergenes.size()>0){%>
                 <div id="gene_full_list_others" style="display:none" >
                <font size="4">Unused Query Genes:</font>
                <%
                    if (othergenes == null)
                    {
                        response.sendRedirect("error.jsp?code=503");
                        if (true) return;
                    }
                    int msize = 0;
                    int totalsize = 0;
                    if(othergenes == null){
                        msize = 0;
                        totalsize = 0;
                    }
                    else{
                        msize = (othergenes.size() > 5) ? 5 : othergenes.size();
                        totalsize = othergenes.size();
                    }
                    for ( i = 0 ; i <= totalsize - 1 ; i ++)
                    {
                            Gene g =othergenes.get(i);
                            %>
                            &nbsp;&nbsp;<a href="genefo.jsp?genename=<%=g.Name %>" target="_blank"><%=g.Name %></a>&nbsp;&nbsp;|
                            <%
                    }
                %>
		&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript:show_abstract_list_others()" style="color:red">LESS</a>
                    </div>
                <div id="gene_abstract_list_others" style="display:block" >
                    <font size="4">Unused Query Genes:</font>
			<%
                                if(othergenes == null){
                                    msize = 0;
                                    totalsize = 0;
                                }
                                else{
                                    msize = (othergenes.size() > 5) ? 5 : othergenes.size();
                                    totalsize = othergenes.size();
                                }
				for ( i = 0 ; i <= msize - 1 ; i ++)
				{
					Gene g =othergenes.get(i);
			%>
					&nbsp;&nbsp;<a href="genefo.jsp?genename=<%=g.Name %>" target="_blank"><%=g.Name %></a>&nbsp;&nbsp;|
                        <%
				}
			%>
			<%  if (totalsize > 5 ) { %>
				&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript:show_full_list_others()" style="color:red">MORE</a>
			<%} %>
		</div>
            <br /><%}%></td>
        </tr>
        <tr>
            <td>
            
            <%
				ArrayList<Gene> unmatched = pf.getUnmatched(querygene);
				
                                if(unmatched.size() > 0){ %>
                                    <font size="4">Unmatched Query:</font>
                                <% }
                                
                                for (int k = 0 ; k < unmatched.size() ; k ++)
				{
					Gene g = unmatched.get(k);
			%>
					&nbsp;&nbsp;<a href="genefo.jsp?genename=<%=g.Name %>" target="_blank"><%=g.Name %></a>&nbsp <% if(k < unmatched.size()-1){ %>|<% }%>
			<%
				}
			%>
            </td>
        </tr>
        <tr><td><br></td></tr>
        
<!-- The 2nd row for listing a 3-columns information-->
    <tr>
        
    	<td  align="left" width="80%"> 
			<table class="valueListTableStyle"  id="resulttable">
				<tr>
                                    <td width = "40">Rank</td><td width = "300">Phenotype Name</td><td width="100">OMIM ID</td><td width ="80">Known Gene Association</td><td width = 60>Relevance<br>Score</td>
				</tr>
			<% 	
                        if(TopK>5080){
                            TopK = 5080;
                        }
                        int m_num = TopK;
                        if(m_num>30){m_num=30;}
                        for (i = 0 ; i < m_num ; i ++)
                        {
                            MatchedPhenotypeEntry mpe =(MatchedPhenotypeEntry)mpes.get(i);
                            fmpe.add(mpe);
                        }
                        for (i = 0 ; i < TopK ; i ++)
                        {
                                MatchedPhenotypeEntry mpe =(MatchedPhenotypeEntry)mpes.get(i);
                                //fmpe.add(mpe);
			%>
					<tr id="row_<%=i+1%>">
						<td>
							<%=(i + 1) %>
						</td>

						<td>
							<a href = "http://www.ncbi.nlm.nih.gov/omim/<%=mpe.OMIM_ID  %>" target="_blank">
							<%=mpe.Name %>
							</a>
						</td>
                                                <td>OMIM_<%=mpe.OMIM_ID%></td>
                                                <td>
                                                    <% if(mpe.ism == true){%>
                                                    <%= mpe.causalgene %>
                                                    <a href="genefo.jsp?genename=<%=mpe.causalgene %>" target="_blank"> &nbsp(more)</a>
                                                    <% }else{%>
                                                    ---
                                                    <%}%>
                                                </td>
						<td>
							<%=String.format("%1.2e",mpe.RelScore) %> <!---"%1.2g" -->
						</td>
					</tr>
			<%
				}
                            
			%>
                        
                                <% // Generate genes node
            String justgame = "49";
            ArrayList<MatchedGeneEntry> matchedgenes = pf.getDisplayGenotype(querygene, 20);

            ArrayList<Edge> GG = pf.getGeneEdges(matchedgenes);
            ArrayList<Edge> GP = pf.getNetworkEdges(matchedgenes, fmpe);
            ArrayList<Edge> PP = pf.getPhenotypeEdges(fmpe);

            //here is the new code to delete nodes which have no edge to any onther node
            matchedgenes = pf.deleteAloneGenes(matchedgenes, GG, GP);
            ArrayList<MatchedGeneEntry> query = pf.displayedQueryGene(querygene, matchedgenes);
            ArrayList<MatchedGeneEntry> nonquery = pf.displayedNonQueryGene(query, matchedgenes);
            fmpe = pf.deleteAlonePhenotype(fmpe, PP, GP);
            
            String[] querygenes = querygene.split("\r\n");
            querygene = "";
            for(i=0; i<querygenes.length; i++){
                querygene = querygene + querygenes[i] + "$";
            }
        %>
			</table>
		</td>
	</tr>
        <tr></tr>
        <tr><td><span id="assoc"></span></td></tr>
        <tr><td height="530px" valign="top"><div id="cytoscapeweb">Visualizing... Please wait...</div></td></tr>
        <tr><td><span id="legend"></span></td></tr>
        <tr>
            <td>
            <div id="showcytoscape" style="display:none" >
                <input id="hide1" type="button" onclick="hide_cytoscape()" value ="Hide Cytoscape file"></input>
                <br>
                <br>
                <% for(int x = 0; x < GG.size(); x++){ %>
                <%= GG.get(x).getSource() %> &nbsp; <%= GG.get(x).getTarget() %><br>      
                <% } %>
                <% for(int x = 0; x < GP.size(); x++){ %>
                <%= GP.get(x).getSource() %> &nbsp; OMIM_<%= GP.get(x).getTarget() %><br>      
                <% } %>
                <% for(int x = 0; x < PP.size(); x++){ %>
                OMIM_<%= PP.get(x).getSource() %> &nbsp; OMIM_<%= PP.get(x).getTarget() %><br>      
                <% } %>
                <br>
                <input id="hide2" type="button" onclick="hide_cytoscape()" value ="Hide Cytoscape file"></input>
            </div>
                <input id="show1" type="button" onclick="show_cytoscape()" value="Show Cytoscape file"></input>
            </td>
        </tr>
</table>
<br>

<script type="text/javascript">
    var vis;
    window.onload = function() {
    // id of Cytoscape Web container div
    var div_id = "cytoscapeweb";
//add by zhanghy at 20120904
<% if(GG.size() != 0 && GP.size() != 0 && PP.size() !=0 && query.size() != 0 && nonquery.size() != 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).GeneName%>"><data key="label"><%= query.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int h = 0; h < nonquery.size(); h ++){ %><node id="<%= nonquery.get(h).GeneName%>"><data key="label"><%= nonquery.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#CCFF33</data></node>\<% } %>
               <% for(int x = 0; x < fmpe.size(); x ++){ %><node id="<%= fmpe.get(x).OMIM_ID%>"><data key="label">OMIM_<%= fmpe.get(x).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';
        document.getElementById('legend').innerHTML = "<font color='blue'>Blue Circle: Query Genes</font> <br><font color='green'>Green Circle: Other Associated Genes</font><br><font color='orange'>Orange Rectangular: Disease Phenotypes</font>";
<% } %>
<% if(GG.size() != 0 && GP.size() != 0 && PP.size() !=0 && (query.size() == 0 || nonquery.size() == 0)){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int x = 0; x < fmpe.size(); x ++){ %><node id="<%= fmpe.get(x).OMIM_ID%>"><data key="label">OMIM_<%= fmpe.get(x).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';
<% } %>
<% if(GP.size() == 0 && GG.size() != 0 && PP.size() !=0 && query.size() != 0 && nonquery.size() != 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).GeneName%>"><data key="label"><%= query.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int h = 0; h < nonquery.size(); h ++){ %><node id="<%= nonquery.get(h).GeneName%>"><data key="label"><%= nonquery.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#CCFF33</data></node>\<% } %>
               <% for(int x = 0; x < fmpe.size(); x ++){ %><node id="<%= fmpe.get(x).OMIM_ID%>"><data key="label">OMIM_<%= fmpe.get(x).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';
        
        document.getElementById('assoc').innerHTML = "INFO: There is no Genes - Phenotypes association to visualize";
<% } %>
<% if(GP.size() == 0 && GG.size() != 0 && PP.size() !=0 && (query.size() == 0 || nonquery.size() == 0)){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int x = 0; x < fmpe.size(); x ++){ %><node id="<%= fmpe.get(x).OMIM_ID%>"><data key="label">OMIM_<%= fmpe.get(x).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';
        
        document.getElementById('assoc').innerHTML = "INFO: There is no Genes - Phenotypes association to visualize";
<% } %>
<% if(PP.size() == 0 && query.size() != 0 && nonquery.size() != 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).GeneName%>"><data key="label"><%= query.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int h = 0; h < nonquery.size(); h ++){ %><node id="<%= nonquery.get(h).GeneName%>"><data key="label"><%= nonquery.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#CCFF33</data></node>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';

        document.getElementById('assoc').innerHTML = "INFO: There is no Phenotype-Phenotype and Genes-Phenotype association to visualize";
<% } %>
<% if(PP.size() == 0 && GP.size() != 0 && query.size() != 0 && nonquery.size() != 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).GeneName%>"><data key="label"><%= query.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int h = 0; h < nonquery.size(); h ++){ %><node id="<%= nonquery.get(h).GeneName%>"><data key="label"><%= nonquery.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#CCFF33</data></node>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int x = 0; x < fmpe.size(); x ++){ %><node id="<%= fmpe.get(x).OMIM_ID%>"><data key="label">OMIM_<%= fmpe.get(x).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';
        document.getElementById('assoc').innerHTML = "";
        document.getElementById('legend').innerHTML = "<font color='blue'>Blue Circle: Query Genes</font> <br><font color='green'>Green Circle: Other Associated Genes</font><br><font color='orange'>Orange Rectangular: Disease Phenotypes</font>";
<% } %>    
<% if(PP.size() == 0 && (query.size() == 0 || nonquery.size() == 0)){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';

        document.getElementById('assoc').innerHTML = "INFO: There is no Phenotype-Phenotype and Genes-Phenotype association to visualize";
<% } %>
<% if(GG.size() == 0 && GP.size() == 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int x = 0; x < fmpe.size(); x ++){ %><node id="<%= fmpe.get(x).OMIM_ID%>"><data key="label">OMIM_<%= fmpe.get(x).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';

        document.getElementById('assoc').innerHTML = "INFO: There is no Gene-Gene Gene-Phenotype association to visualize";
<% } %>
<% if(GG.size() == 0 && GP.size() != 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int x = 0; x < fmpe.size(); x ++){ %><node id="<%= fmpe.get(x).OMIM_ID%>"><data key="label">OMIM_<%= fmpe.get(x).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';

<% } %>    


	var visual_style =
	{
		global: { backgroundColor: "#EDEDED" },
		nodes:
		{
			labelFontSize: 16.5,
			shape: {
				passthroughMapper: { attrName: "s"}
			},
			color: {
				passthroughMapper: { attrName: "c"}
			},
			borderWidth: 1,
			borderColor: "#EDEDED",
			size:
			{
				defaultValue: 66,
				continuousMapper: { attrName: "weight", minValue: 66, maxValue: 120}
			},
			labelHorizontalAnchor: "center"

		},
		edges: { width: 2, color: "#0B94B1" }
	};

	// initialization options
	var options =
	{
		swfPath: "./swf/CytoscapeWeb",
		flashInstallerPath: "./swf/playerProductInstall"
	};

	//var vis = new org.cytoscapeweb.Visualization(div_id, options);
 vis = new org.cytoscapeweb.Visualization(div_id, options);
	// callback when Cytoscape Web has finished drawing
	vis.ready(function()
	{
		// add a listener for when nodes and edges are clicked
		vis.addListener("click", "nodes", function(event)
		{
			handle_click(event);
		});

                vis.addListener("mouseover", "nodes", function(event)
		{
			handle_mouseover(event);
		});

                vis.addListener("mouseout", "nodes", function(event)
		{
			handle_mouseout(event);
		});

	function handle_click(event)
	{
            var temp = event.target;
            var temp2 = temp.data["s"];
            if(temp2=="ROUNDRECT"){
		var target = event.target;
                window.open("http://omim.org/entry/"+target.data["id"], "OMIM", "channelmode=yes");
            }else{
            	var target = event.target;
                window.open("genefo.jsp?genename="+target.data["id"], "GENE", "channelmode=yes");
            }
	}

        var mostcurrent_i;

        function handle_mouseover(event)
        {
            var temp = event.target;
            var temp2 = temp.data["s"];
            if(temp2=="ROUNDRECT"){
                
                table = document.getElementById('resulttable');
                
                for(var i = 1; i < table.rows.length; i++){
                    if(table.rows[i].cells[2].firstChild.nodeValue == temp.data["label"]){
                          var idx = "row_" + i;
                          
                          document.getElementById(idx).style.color = "red";
                          document.getElementById(idx).style.fontWeight = "bold";
                          mostcurrent_i = idx;
                          break;
                    }
                }
            }

        }

        function handle_mouseout(event)
        {
           table = document.getElementById('resulttable');
                for(var i = 1; i < table.rows.length; i++){
                          var idx = "row_" + i;
                          document.getElementById(idx).style.color = "black";
                          document.getElementById(idx).style.fontWeight = "normal";
                }
        }
	});

	// draw options
	var draw_options =
	{
		// your data goes here
		network: xml,

		// hide pan zoom
		panZoomControlVisible: true,

		// show edge labels too
		edgeLabelsVisible: false,

 		edgesMerged: true,

		// set the style at initialisation
		visualStyle: visual_style,

		// let's try another layout
		layout: "Tree"
	};
       vis.draw(draw_options);
       ori_height = parent.document.getElementById('frmContent').contentWindow.document.body.scrollHeight;
       var the_height= document.body.clientHeight;

       //change the height of the iframe
       parent.document.getElementById('frmContent').height= the_height;
      
};
function download_file()
{
    vis.exportNetwork('png', 'export.jsp?m_AlgId=<%=AlgId%>&m_TopK=<%=TopK%>&query=<%=querygene%>&type=1');
}
</script>

</body>
</html>