<%@page import="java.util.ArrayList"%>
<%@page import="java.util.StringTokenizer"%>
<%@page import="java.io.*" %>
<%@page import="java.net.URLEncoder" %>
<%@page import="java.util.zip.*" %>
<%@page import="com.ppi.ContextManager" %>
<%@page import="com.ppi.Gene" %>
<%@page import="com.ppi.Phenotype" %>
<%@page import="com.ppi.MatchedPhenotypeEntry" %>
<%@page import="com.ppi.PhenotypeFinder" %>
<%@page import="com.ppi.GenetypeFinder" %>
<%@page import="com.ppi.MatchedGeneEntry" %>
<%@ page import="java.io.*"%>
<%@ page import="java.util.*" %>
<%@ page import="com.ppi.*"%>

<html>
<body>       
<%
    String type =  request.getParameter("type");
    if(type == null){
        return;
    }
   
    String m_query = request.getParameter("query");
    String contentType = request.getContentType();
    int AlgId = Integer.parseInt(request.getParameter("AlgId"));
    int TopK = Integer.parseInt(request.getParameter("TopK"));
    int TopP = Integer.parseInt(request.getParameter("topp"));;
    int TopG = Integer.parseInt(request.getParameter("topg"));;
    String[] querys = m_query.split("\\$");
    m_query = "";
    int i = 0;
    for(i=0; i<querys.length-1; i++){
        m_query = m_query + querys[i] +"\r\n";
    }
    m_query = m_query + querys[i];
    String savedTable = "";
    String tableStyle = "<table border='1' text-align='center'>";
    String cytoscapeFile = tableStyle;
    cytoscapeFile = "";
    if(type.equals("1")){
        if(TopK>5080){
            TopK = 5080;
        }
        String querygene = m_query;
        savedTable = "<tr><td>Rank</td><td>Phenotype Name</td><td>OMIM ID</td><td>Known Gene Association</td><td>Relevance<br>Score</td></tr>";
        PhenotypeFinder pf = new PhenotypeFinder(request.getParameter("speciesID"));
        ArrayList<Gene> genes = pf.ValidateGenes(querygene);
        ArrayList<MatchedPhenotypeEntry> mpes = pf.SearchByGenes(genes, AlgId);
        ArrayList<MatchedPhenotypeEntry> fmpe = new ArrayList<MatchedPhenotypeEntry>();
        if(mpes.size()<TopK)
        {
            TopK = mpes.size();
        }
        for(i=0; i<25; i++)
       {
            MatchedPhenotypeEntry mpe =(MatchedPhenotypeEntry)mpes.get(i);
            fmpe.add(mpe);
        }
        for (i = 0 ; i < TopK ; i ++)
        {
            MatchedPhenotypeEntry mpe =(MatchedPhenotypeEntry)mpes.get(i);
            savedTable = savedTable + "<tr>";
            savedTable += "<td>" + Integer.toString(i+1) + "</td>";
            savedTable += "<td>" + mpe.Name + "</td>";
            savedTable += "<td>" + mpe.OMIM_ID + "</td>";
            savedTable += "<td>" + mpe.causalgene + "</td>";
            savedTable += "<td>" + String.format("%1.2e",mpe.RelScore) + "</td>";
            savedTable += "</tr>";
        } 
        savedTable = tableStyle + savedTable + "</table>";
        //Get cytoscape file
        ArrayList<MatchedGeneEntry> matchedgenes = pf.getDisplayGenotype(m_query, TopG);
        ArrayList<Edge> GG = pf.getGeneEdges(matchedgenes);
        ArrayList<Edge> GP = pf.getNetworkEdges(matchedgenes, fmpe);
        ArrayList<Edge> PP = pf.getPhenotypeEdges(fmpe);

        //here is the new code to delete nodes which have no edge to any onther node
        // matchedgenes = pf.deleteAloneGenes(matchedgenes, GG, GP);
        //matchedgenes = pf.mdeleteAloneGenes(genes, matchedgenes, GG, GP);
        ArrayList<MatchedGeneEntry> query = pf.displayedQueryGene(querygene, matchedgenes);
        ArrayList<MatchedGeneEntry> nonquery = pf.displayedNonQueryGene(query, matchedgenes);

        // fmpe = pf.mdeleteAlonePhenotype(matchedgenes, fmpe, PP, GP);
        // fmpe = pf.deleteAlonePhenotype(fmpe, PP, GP);
        GANDP gp = pf.deleteUnReachableGenesAndPhenotypes(query, nonquery, fmpe, GG, PP, GP);
        matchedgenes = gp.mge;
        fmpe = gp.mpe;
        GG = pf.updateGG(GG, matchedgenes);
        GP = pf.updateGP(GP, matchedgenes, fmpe);
        PP = pf.updatePP(PP, fmpe);
        query = pf.displayedQueryGene(querygene, matchedgenes);
        nonquery = pf.displayedNonQueryGene(query, matchedgenes);
        for(int x = 0; x < GG.size(); x++){
            cytoscapeFile += GG.get(x).getSource()+ " " +GG.get(x).getTarget() + "\r\n";
        }
        for(int x = 0; x < GP.size(); x++){
            cytoscapeFile += GP.get(x).getSource() + " " + "OMIM_" + GP.get(x).getTarget() + "\r\n";
        }
        for(int x = 0; x < PP.size(); x++){
            cytoscapeFile += "OMIM_" + PP.get(x).getSource() + " " + "OMIM_" + PP.get(x).getTarget() + "\r\n";     
        }
    }
    else if(type.equals("2")){
        if(TopK>9673){
            TopK = 9673;
        }
        String queryphenotype = m_query;
        String Region = request.getParameter("Region");
        String ChrRegions = request.getParameter("ChrRegions");
	GenetypeFinder gf = new GenetypeFinder(request.getParameter("speciesID"));
        
	ArrayList<Phenotype> phenotype = gf.ValidatePhenotypes(queryphenotype);
	if (phenotype.size() == 0)
	{
            return;
	}
	ArrayList<MatchedGeneEntry> mge = gf.SearchByPhenotype(phenotype, AlgId);
        mge = gf.getGeneLocation(mge);
        int RegionFlag = Integer.parseInt(Region);
        String regionInfo="";
        if (RegionFlag == 2)
        {
            String[] ChrRegionss = ChrRegions.split("\\$");
            ChrRegions = "";
            for(i=0; i<ChrRegionss.length-1; i++){
                ChrRegions = ChrRegions + ChrRegionss[i] +"\r\n";
            }
            ChrRegions = ChrRegions + ChrRegionss[i];
            regionInfo = ChrRegions;
            mge = gf.getSpecifiedGenes(mge, regionInfo);
            if (mge == null)
            {
		if (true) return;
            }
            if (mge.size() == 0)
            {
		if (true) return;
            }
            
        }
        if(TopK > mge.size())
        {
            TopK = mge.size();
        }
        savedTable = "<tr><td>Rank</td><td>Gene Name</td><td>Known Disease Association</td><td>locus</td><td>Relevance<br>Score</td></tr>";
        for (i = 0 ; i < TopK ; i ++)
        {
                MatchedGeneEntry smge =(MatchedGeneEntry)mge.get(i);
                savedTable = savedTable + "<tr>";
                savedTable += "<td>" + Integer.toString(i+1) + "</td>";
                savedTable += "<td>" + smge.GeneName + "</td>";
                savedTable += "<td>" + smge.disease + "</td>";
                savedTable += "<td>" + smge.location + "</td>";
                savedTable += "<td>" + String.format("%1.2e",smge.RelScore) + "</td>";
                savedTable += "</tr>";
        }
        savedTable = tableStyle + savedTable + "</table>";
        
        ArrayList<MatchedGeneEntry> matchedgenes = gf.getDisplayGene(mge, TopG);
        ArrayList<MatchedPhenotypeEntry> matchedphenotypes = gf.getDisplayPhenotype(queryphenotype, TopP);
        ArrayList<Edge> GG = gf.getGeneEdges(matchedgenes);
        ArrayList<Edge> GP = gf.getNetworkEdges(matchedgenes, matchedphenotypes);
        ArrayList<Edge> PP = gf.getPhenotypeEdges(matchedphenotypes);
        gf.getAllQueryNetworkEdges(matchedgenes, matchedphenotypes, GP);
        ArrayList<MatchedPhenotypeEntry> query = gf.displayedQueryPhen(queryphenotype, matchedphenotypes);
        ArrayList<MatchedPhenotypeEntry> nonquery = new ArrayList<MatchedPhenotypeEntry>();
        nonquery = gf.displayedNonQueryPhen(query, matchedphenotypes);
        GANDP gp = gf.deleteUnReachableGenesAndPhenotypes(query, nonquery, matchedgenes, GG, PP, GP);
        matchedgenes = gp.mge;
        matchedphenotypes = gp.mpe;
        query = gf.displayedQueryPhen(queryphenotype, matchedphenotypes);
        nonquery = gf.displayedNonQueryPhen(query, matchedphenotypes);
        GG = gf.updateGG(GG, matchedgenes);
        GP = gf.updateGP(GP, matchedgenes, matchedphenotypes);
        PP = gf.updatePP(PP, matchedphenotypes);
        for(int x = 0; x < GG.size(); x++){
            cytoscapeFile += GG.get(x).getSource()+ " " +GG.get(x).getTarget() + "\r\n";
        }
        for(int x = 0; x < GP.size(); x++){
            cytoscapeFile += GP.get(x).getSource() + " " + "OMIM_" + GP.get(x).getTarget() + "\r\n";
        }
        for(int x = 0; x < PP.size(); x++){
            cytoscapeFile += "OMIM_" + PP.get(x).getSource() + " " + "OMIM_" + PP.get(x).getTarget() + "\r\n";     
        }
    }
    String rootPath = "C:\\rcnet_data\\";
    String folderName = ContextManager_new.getRandomName() + "\\";
    String pathName = rootPath + folderName;
    File mfile = new File(pathName); 
    mfile.mkdir();
    byte[] tableData = savedTable.getBytes();
    RandomAccessFile inOut = new RandomAccessFile(pathName + "result.html", "rw");
    inOut.write(tableData);
    inOut.close();
    byte[] cytoscapeData = cytoscapeFile.getBytes();
    inOut = new RandomAccessFile(pathName + "cytoscapeFile.txt", "rw");
    inOut.write(cytoscapeData);
    inOut.close();
    String zipPath = rootPath + ContextManager_new.getRandomName() + "\\";
    //String zipPath = rootPath  + "\\tempFile\\";
    File zipfile = new File(zipPath); 
    zipfile.mkdir();
    ContextManager_new.zip(pathName, zipPath + "result.zip");

    File dfile = new File(zipPath + "result.zip");
    String filename = dfile.getName();
    InputStream fis = new BufferedInputStream(new FileInputStream(zipPath +"result.zip"));
    byte buffer[] = new byte[fis.available()];
    fis.read(buffer);
    fis.close(); 
    response.reset();
    request.setCharacterEncoding("ISO-8859-1");
    response.setContentType("application/octet-stream");
    response.addHeader("Content-Disposition", (new StringBuilder(
                    "attachment;filename=\"")).append(
                    URLEncoder.encode(filename, "ISO-8859-1")).append("\"")
                    .toString());
    response.addHeader("Content-Length", (new StringBuilder()).append(
                    dfile.length()).toString());
    OutputStream toClient = new BufferedOutputStream(response
                    .getOutputStream());
    toClient.write(buffer);
    toClient.flush();
    toClient.close();
    ContextManager_new.deletefile(pathName);
    ContextManager_new.deletefile(zipPath);
    out.clear();
    out = pageContext.pushBody();
%>
</body>
</html>