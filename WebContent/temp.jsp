<%-- 
    Document   : temp
    Created on : 2012-12-4, 12:44:50
    Author     : zhanghy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   <%
   try{
        int type = Integer.parseInt(request.getParameter("type")); 
        if(type == 1){
            int AlgId = Integer.parseInt(request.getParameter("AlgId")); 
            int TopK = Integer.parseInt(request.getParameter("txtTopK"));
            int TopG = Integer.parseInt(request.getParameter("topg")); 
            int TopP = Integer.parseInt(request.getParameter("topp")); 
            String querygene = request.getParameter("Genes");
            String speciesID = request.getParameter("speciesID");
            response.sendRedirect("search_phenotype.jsp?txtTopK="+TopK+"&topg="+TopG+"&topp="+TopP+"&AlgId="+AlgId+"&speciesID="+speciesID+"&Genes="+querygene);
        }
        else if(type==2){
            int AlgId = Integer.parseInt(request.getParameter("AlgId")); 
            int TopK = Integer.parseInt(request.getParameter("txtTopK"));
            int TopG = Integer.parseInt(request.getParameter("topg")); 
            int TopP = Integer.parseInt(request.getParameter("topp")); 
            int Region = Integer.parseInt(request.getParameter("Region")); 
            String ChrRegions = request.getParameter("ChrRegions"); 
            String Phenotypes = request.getParameter("Phenotypes"); 
            response.sendRedirect("search_genes.jsp?txtTopK="+TopK+"&topg="+TopG+"&topp="+TopP+"&AlgId="+AlgId+"&Phenotypes="+Phenotypes+"&Region="+Region+"&ChrRegions="+ChrRegions);
        }
        else if(type==3){
            int AlgId = Integer.parseInt(request.getParameter("AlgId")); 
            int TopK = Integer.parseInt(request.getParameter("txtTopK"));
            int TopG = Integer.parseInt(request.getParameter("topg")); 
            int TopP = Integer.parseInt(request.getParameter("topp")); 
            String querygene = request.getParameter("Genes"); 
            response.sendRedirect("search_phenotype.jsp?txtTopK="+TopK+"&topg="+TopG+"&topp="+TopP+"&AlgId="+AlgId+"&Genes="+querygene);
        }
      
        else if(type == 4){
            int AlgId = Integer.parseInt(request.getParameter("AlgId")); 
            int TopK = Integer.parseInt(request.getParameter("txtTopK"));
            int TopG = Integer.parseInt(request.getParameter("topg")); 
            int TopP = Integer.parseInt(request.getParameter("topp")); 
            int Region = Integer.parseInt(request.getParameter("Region")); 
            String ChrRegions = request.getParameter("ChrRegions"); 
            String Phenotypes = request.getParameter("Phenotypes"); 
            response.sendRedirect("search_genes.jsp?txtTopK="+TopK+"&topg="+TopG+"&topp="+TopP+"&AlgId="+AlgId+"&Phenotypes="+Phenotypes+"&Region="+Region+"&ChrRegions="+ChrRegions);
        }
     }
    catch (Exception e){
        response.sendRedirect("error.jsp?code=501");
        if (true) return;
    }
    %>
</html>
