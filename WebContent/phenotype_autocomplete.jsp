<%@page import="java.util.Iterator"%>  
<%@page import="java.util.List"%>  
<%@page import="java.util.ArrayList"%>  
<%@page import="com.ppi.Phenotype"%>
<%@page import="com.ppi.ContextManager"%>

<%   
	ArrayList<Phenotype> phenotypes = ContextManager.getLsPhenotypes();
	int i = 0;
	Iterator<Phenotype> iterator = phenotypes.iterator();
	while (iterator.hasNext()) {
		String phenotype = ((Phenotype)(iterator.next())).Name;		
		String prefix = request.getParameter("q").toUpperCase();
		if (phenotype.startsWith(prefix) == true)
		{
			out.println(phenotype);
			i ++;
		}
	}
%>  