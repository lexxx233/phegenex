//加载loading图片
function setLoadingPage(visible){
    if (visible==true){
        document.all["loadingImage"].style.visibility="visible";
        document.all["pg"].style.visibility="hidden";
    }else{
        document.all["loadingImage"].style.visibility="hidden";
        document.all["pg"].style.visibility="visible";
    }
}


// 调整某个控件的尺寸

// id 
// width
// height
function adjustWindow(id, width, height) {
	var ctl = document.getElementById(id);
	if (!ctl) {
		return;
	}
	if (height != 0) {
		var top = 0;
		var bodyHeight = document.body.offsetHeight;
		var node = document.getElementById(id);
		while (node.tagName != "BODY") {
			top += node.offsetTop;
			node = node.offsetParent;
		}
		var maxheight = 0;
		for (i = 0; i < ctl.children.length; i++) {
			if (maxheight < ctl.children[i].offsetTop + ctl.children[i].offsetHeight) {
				maxheight = ctl.children[i].offsetTop + ctl.children[i].offsetHeight;
			}
		}
		if (maxheight < bodyHeight - top) {
			if (maxheight > height) {
				ctl.style.posHeight = height;
			} else {
				ctl.style.posHeight = 0;
			}
		} else {
			if (bodyHeight - top > height) {
				ctl.style.posHeight = height;
			} else {
				ctl.style.posHeight = bodyHeight - top;
			}
		}
		if (ctl.height) {
			ctl.height = ctl.style.posHeight;
		}
	}
	if (width != 0) {
		var left = 0;
		var node = document.getElementById(id);
		while (node.tagName != "BODY") {
			left += node.offsetLeft;
			node = node.offsetParent;
		}
		if (document.body.clientWidth - left > width) {
			ctl.style.posWidth = width;
		} else {
			ctl.style.posWidth = document.body.clientWidth - left;
		}
		if (ctl.width) {
			ctl.width = ctl.style.posWidth;
		}
	}
}

// 上下滚动条
function adjustVScroll(id, height) {
	var ctl = document.getElementById(id);
	if (!ctl) {
		return;
	}
	var top = 0;
	var bodyHeight = document.body.scrollHeight;
	var node = document.getElementById(id);
	while (node.tagName != "BODY") {
		top += node.offsetTop;
		node = node.offsetParent;
	}
	var maxheight = 0;
	for (i = 0; i < ctl.children.length; i++) {
		if (maxheight < ctl.children[i].offsetTop + ctl.children[i].offsetHeight) {
			maxheight = ctl.children[i].offsetTop + ctl.children[i].offsetHeight;
		}
	}
	if (maxheight < bodyHeight - top) {
		if (maxheight > height) {
			ctl.style.posHeight = height;
		} else {
			ctl.style.posHeight = 0;
		}
	} else {
		if (bodyHeight - top > height) {
			ctl.style.posHeight = height;
		} else {
			ctl.style.posHeight = bodyHeight - top;
		}
	}
	if (ctl.height) {
		ctl.height = ctl.style.posHeight;
	}
}


function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_nbGroup(event, grpName) { //v6.0
  var i,img,nbArr,args=MM_nbGroup.arguments;
  if (event == "init" && args.length > 2) {
    if ((img = MM_findObj(args[2])) != null && !img.MM_init) {
      img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;
      if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();
      nbArr[nbArr.length] = img;
      for (i=4; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
        if (!img.MM_up) img.MM_up = img.src;
        img.src = img.MM_dn = args[i+1];
        nbArr[nbArr.length] = img;
    } }
  } else if (event == "over") {
    document.MM_nbOver = nbArr = new Array();
    for (i=1; i < args.length-1; i+=3) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = (img.MM_dn && args[i+2]) ? args[i+2] : ((args[i+1])? args[i+1] : img.MM_up);
      nbArr[nbArr.length] = img;
    }
  } else if (event == "out" ) {
    for (i=0; i < document.MM_nbOver.length; i++) {
      img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up; }
  } else if (event == "down") {
    nbArr = document[grpName];
    if (nbArr)
      for (i=0; i < nbArr.length; i++) { img=nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }
    document[grpName] = nbArr = new Array();
    for (i=2; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = img.MM_dn = (args[i+1])? args[i+1] : img.MM_up;
      nbArr[nbArr.length] = img;
  } }
}
function isBlank(field, strBodyHeader) {   
    strTrimmed = trim(field.value);
    if (strTrimmed.length > 0) return false;
    alert("\"" + strBodyHeader + "\"不能为空！");
    //field.focus();
    return true;
}
function isEmail(field, strBodyHeader){
    emailStr = field.value;
    if(emailStr.length == 0) return true;
    if(!checkEmail(emailStr)) {
        alert(strBodyHeader + "地址无效!");
        //field.focus();
        return false;
    }
    return true;
}
function checkGoodName(str, desc) {
    s = trim(str);
    len = s.length; 
    for (i = 0; i < len; i++) {
        b = s.charCodeAt(i);
        if ((b >= toInt('a')) && (b <= toInt('z'))) { // not a cast, it's a function.
            // low chars
        } else if ((b >= toInt('A')) && (b <= toInt('Z'))) {
            // up chars
        } else if ((b >= toInt('0')) && (b <= toInt('9'))) {
            // numbers 
        } else if (( (b==toInt('_')) || (b==toInt('.')) || (b==toInt('@')) ) && (i != 0)) {
            // very litte special chars
        } else {
            alert(desc + " \"" + s +  "\" " + "无效. 无效字符为： \"" + s.charAt(i) + "\""); 
            return false;
            // not good name error
        }
    }
    return true;
}

function toInt(c) {
  return c.charCodeAt(0);
}

function checknum(p, n) { 
  var l = p.length; 
  var count=0; 
  for(var i=0; i<l; i++) { 
    var digit = p.charAt(i); 
    if(digit == "." ) { 
      ++count; 
      if(count>1) {
	    alert ("\"" + n + "\": 必须为正整数！"); 
        return false; 
      } 
    } else if(digit < "0" || digit > "9") {
	    alert ("\"" + n + "\": 必须为正整数！"); 
        return false; 
    } 
  } 
  return true;
}

function checkEmail(emailStr) {
       if (emailStr.length == 0) {
           return true;
       }
       var emailPat=/^(.+)@(.+)$/;
       var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
       var validChars="\[^\\s" + specialChars + "\]";
       var quotedUser="(\"[^\"]*\")";
       var ipDomainPat=/^(\d{1,3})[.](\d{1,3})[.](\d{1,3})[.](\d{1,3})$/;
       var atom=validChars + '+';
       var word="(" + atom + "|" + quotedUser + ")";
       var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
       var domainPat=new RegExp("^" + atom + "(\\." + atom + ")*$");
       var matchArray=emailStr.match(emailPat);
       if (matchArray == null) {
           return false;
       }
       var user=matchArray[1];
       var domain=matchArray[2];
       if (user.match(userPat) == null) {
           return false;
       }
       var IPArray = domain.match(ipDomainPat);
       if (IPArray != null) {
           for (var i = 1; i <= 4; i++) {
              if (IPArray[i] > 255) {
                 return false;
              }
           }
           return true;
       }
       var domainArray=domain.match(domainPat);
       if (domainArray == null) {
           return false;
       }
       var atomPat=new RegExp(atom,"g");
       var domArr=domain.match(atomPat);
       var len=domArr.length;
       if ((domArr[domArr.length-1].length < 2) ||
           (domArr[domArr.length-1].length > 3)) {
           return false;
       }
       if (len < 2) {
           return false;
       }
       return true;
    }
    
function addLikeFlag(value)
{
	return "%"+value+"%";
}

//valuelist用，在翻页时触发的事件
function goNext(node){}
function goPre(node){}
function goFirst(node){}
function goLast(node){} 

//表格的局部滚动，右半边的上下和左右滚动
function loc_scrl()
{
    div_top_right.scrollLeft = div_bottom_right.scrollLeft;
    div_bottom_left.scrollTop = div_bottom_right.scrollTop;
}
//表格带滚动时，在页面load时使表格左右两部分的每一行都对齐
function adjustRow(leftId, rightId) {
	var left = document.getElementById(leftId);
	var right = document.getElementById(rightId);
	var leftDisp = left.style.display;
	var rightDisp = right.style.display;
	left.style.display ="none";
	right.style.display ="none";
	for (j = 0; right && j < right.rows.length; j++) {
		if (left.rows[j].offsetHeight < right.rows[j].offsetHeight) {
			left.rows[j].style.height = right.rows[j].offsetHeight;
		} else if (left.rows[j].offsetHeight > right.rows[j].offsetHeight) {
			right.rows[j].style.height = left.rows[j].offsetHeight;
		}
	}
	left.style.display = leftDisp;
	right.style.display = rightDisp;
}
