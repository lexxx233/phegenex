<%-- 
    Document   : factorize
    Created on : Jun 10, 2013, 1:52:04 PM
    Author     : compbio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html style="background-color: #ededed">
    <head>
        <title>Gene Matrix Factorization</title>
        <%@ page import="java.io.*"%>
        <%@ page import="java.util.*" %>
        <%@ page import="com.ppi.*"%>
        <%@ page import="com.ppi.PhenotypeFinder"%>
        <%@ page import="com.ppi.GenetypeFinder"%>
        <%@ page import="org.apache.log4j.Logger" %>
        <%@ page import="com.ppi.web.LogHelper" %>

        <script type="text/javascript" src="./js/json2.min.js"></script>
        <script type="text/javascript" src="./js/AC_OETags.min.js"></script>
        <script type="text/javascript" src="./js/cytoscapeweb.min.js"></script>
        <script type="text/javascript" src="./js/d3.v3.min.js"/>
    </head>
    
    <body>
        <h1>Hello World!</h1>
    </body>
</html>
