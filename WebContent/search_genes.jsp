<%@page import="javax.swing.text.Document"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<html style="background-color: #ededed">
<head>
<title>Gene Finder</title>
<%@ page import="java.io.*"%>
<%@ page import="java.util.*" %>
<%@ page import="com.ppi.*"%>
<%@ page import="com.ppi.PhenotypeFinder"%>
<%@ page import="com.ppi.GenetypeFinder"%>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="com.ppi.web.LogHelper" %>

<script type="text/javascript" src="./js/json2.min.js"></script>
<script type="text/javascript" src="./js/AC_OETags.min.js"></script>
<script type="text/javascript" src="./js/cytoscapeweb.min.js"></script>

<style>
	#cytoscapeweb { width: 100%; height: 100%;}
</style>

<link href="css/style.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    function show_full_list(){
            document.getElementById("phenotype_full_list").style.display = "block";
            document.getElementById("phenotype_abstract_list").style.display = "none";
    }
    function show_full_list_others(){
            document.getElementById("gene_full_list_others").style.display = "block";
            document.getElementById("gene_abstract_list_others").style.display = "none";
    }
    function show_abstract_list(){
            document.getElementById("phenotype_abstract_list").style.display = "block";
            document.getElementById("phenotype_full_list").style.display = "none";
    }
    function show_abstract_list_others(){
            document.getElementById("gene_abstract_list_others").style.display = "block";
            document.getElementById("gene_full_list_others").style.display = "none";
    }
    
    function calcHeight(){
                //find the height of the internal page
                var the_height= parent.document.getElementById('frmContent').contentWindow.document.body.scrollHeight;

                //change the height of the iframe
                parent.document.getElementById('frmContent').height= the_height;
            }

    var ori_height = parent.document.getElementById('frmContent').contentWindow.document.body.scrollHeight;
    //handle cytoscape
    function show_cytoscape(){
        document.getElementById('showcytoscape').style.display = "block";
        document.getElementById('show1').style.visibility = 'hidden';
        calcHeight();
    }   
    function hide_cytoscape(){
        document.getElementById('showcytoscape').style.display = "none";
        document.getElementById('show1').style.visibility = 'visible';
        parent.document.getElementById('frmContent').height= ori_height;
    }
    </script>
</head>

<body>
<%
//      Logger log = Logger.getLogger(ContextManager.class);
//	log.info(LogHelper.GetLogItemFromRequest(request));     
    ArrayList<Entry> PhenotypeNetwork = ContextManager.getPhenotypeEntry();
    ArrayList<String> geneName = ContextManager.getGeneName();
    ArrayList<Entry> GeneNetwork = ContextManager.getGeneEntry();
    ArrayList<MatchedGeneEntry> fmg = new ArrayList<MatchedGeneEntry>();
    int AlgId = 0;
    int TopK = 10;
    int TopP = 25;
    int TopG = 50;
    String queryphenotype = "";
    AlgId = Integer.parseInt(request.getParameter("AlgId"));
    try{
            if(request.getParameter("txtTopK") != null){
                TopK = Integer.parseInt(request.getParameter("txtTopK"));
            }
            if(request.getParameter("topg") != null){
                TopG = Integer.parseInt(request.getParameter("topg"));
            }
            if(request.getParameter("topp") != null){
                TopP = Integer.parseInt(request.getParameter("topp")); 
            }
            if(TopK>9673){
                TopK = 9673;
            }
          if(TopP>5080){
            TopP = 5080;
            }
          if(TopG>9673){
            TopG = 9673;
            }
    }catch (Exception e){
            response.sendRedirect("error.jsp?code=501");
            if (true) return;
    }
    queryphenotype = request.getParameter("Phenotypes");
    queryphenotype = queryphenotype.replace("$", "\r\n");
    GenetypeFinder gf = new GenetypeFinder(request.getParameter("speciesID"));
        
    ArrayList<Phenotype> phenotype = gf.ValidatePhenotypes(queryphenotype);
    if (phenotype.size() == 0)
    {
            response.sendRedirect("error.jsp?code=502");
            if (true) return;
    }
    ArrayList<MatchedGeneEntry> mge = gf.SearchByPhenotype(phenotype, AlgId);
    //add by zhanghy at 20120903
    mge = gf.getGeneLocation(mge);
    int Region = Integer.parseInt(request.getParameter("Region"));
    String regionInfo="";
    if (Region == 2)
    {
        regionInfo = request.getParameter("ChrRegions");
        regionInfo = regionInfo.replace("$", "\r\n");
        mge = gf.getSpecifiedGenes(mge, regionInfo);
        if (mge == null)
        {
            response.sendRedirect("error.jsp?code=503");
            if (true) return;
        }
        if (mge.size() == 0)//added at 20120908
        {
            response.sendRedirect("error.jsp?code=506");
            if (true) return;
        }        
    }
    if(TopK > mge.size())
    {
        TopK = mge.size();
    }
%>

<table width="90%" border="0" align="center">
    <!-- The 2nd row for listing a 3-columns information-->
    <tr>
    	<td  align="left" width="80%">
            <font size="4">Ranking Result:</font>
            <div align="right">
            <form id="tableForm" name="tableForm" method="post">
                Number of genes to display:
                <input name="txtTopK" id="txtTopK" type="text" size="10" value="<%=TopK%>"  />
                <input id="updateTable" type="button" onclick="update_table()" value="refresh" />
            </form></div>
            <table class="valueListTableStyle" id="resulttable">
                <tr>
                    <td width = "40">Rank</td><td width = "100">Gene Name</td><td width="200">Known Disease Association</td><td width = "200">locus</td><td width = 70>Relevance<br>Score</td>
                </tr>
                               
                <% 	 
                    int i = 0;
                    for (i = 0 ; i < TopK ; i ++){
                        MatchedGeneEntry smge =(MatchedGeneEntry)mge.get(i);
                        fmg.add(smge);

                %>
		<tr id="row<%=i+1%>">
                <td>
                        <%=(i + 1) %>
                </td>

                <td><a href = "genefo.jsp?genename=<%= smge.GeneName%>" target="_blank"><%= smge.GeneName %></a></td>

                <td>
                    <% if(smge.ism == true){%>
                    <%= smge.disease %>
                    <a href="http://www.omim.org/entry/<%=smge.OMIM_ID%>" target="_blank"> &nbsp(more)</a>
                    <% }else{%>
                    ---
                    <%}%>

                </td>
                <td>
                        <%= smge.location%>
                </td>
                <td>
                        <%=String.format("%1.2e",smge.RelScore) %> <!---"%1.2g" -->
                </td>
                <td style="display:none"><%= smge.GeneName%></td>
            </tr>
                <%
                }
                %>
           
                <% // Generate genes node
                int displayNum = 50;
                if(displayNum > mge.size())
                {
                    displayNum = mge.size();
                }
                ArrayList<MatchedGeneEntry> matchedgenes = gf.getDisplayGene(mge, TopG);
                ArrayList<MatchedPhenotypeEntry> matchedphenotypes = gf.getDisplayPhenotype(queryphenotype, TopP);

                ArrayList<Edge> GG = gf.getGeneEdges(matchedgenes);
                ArrayList<Edge> GP = gf.getNetworkEdges(matchedgenes, matchedphenotypes);
                ArrayList<Edge> PP = gf.getPhenotypeEdges(matchedphenotypes);

                gf.getAllQueryNetworkEdges(matchedgenes, matchedphenotypes, GP);          
                //matchedgenes = gf.deleteAloneGenes(matchedgenes, GG, GP);
                //matchedphenotypes = gf.mdeleteAlonePhenotype(phenotype, matchedphenotypes, PP, GP);
                //matchedgenes = gf.mdeleteAloneGenes(matchedphenotypes, matchedgenes, GG, GP);

                ArrayList<MatchedPhenotypeEntry> query = gf.displayedQueryPhen(queryphenotype, matchedphenotypes);
                ArrayList<MatchedPhenotypeEntry> nonquery = new ArrayList<MatchedPhenotypeEntry>();
                nonquery = gf.displayedNonQueryPhen(query, matchedphenotypes);
                GANDP gp = gf.deleteUnReachableGenesAndPhenotypes(query, nonquery, matchedgenes, GG, PP, GP);
                matchedgenes = gp.mge;
                matchedphenotypes = gp.mpe;
                query = gf.displayedQueryPhen(queryphenotype, matchedphenotypes);
                nonquery = gf.displayedNonQueryPhen(query, matchedphenotypes);
                GG = gf.updateGG(GG, matchedgenes);
                GP = gf.updateGP(GP, matchedgenes, matchedphenotypes);
                PP = gf.updatePP(PP, matchedphenotypes); 
            %>
            </table>
            <table width="100%" border="0" align="center">
                <tr><td>
                    <hr>
                    <div id="down" style="display: block; vertical-align: central"> <font size="4">Query Details&nbsp;</font><img src="images/down.png" width="18" height="18" onclick="getDetails()" align="center" style="border: none"/></div>
                    <div id="up" style="display: none; vertical-align: central"><font size="4">Query Details&nbsp;</font><img src="images/up.png" width="18" height="18" onclick="hideDetails()" align="center" style="border:none" /></div>
                </td></tr>
                <tr><td><div id="detailTable" style="display:none"><table>
                <!--The 1st row for listing header of the page -->
                    <tr>
                        <td>
                        <div id="phenotype_full_list" style="display:none" >
                            <font size="3">&nbsp;&nbsp;&nbsp;&nbsp;Query Phenotypes:</font>
                            <%
                                    
                                for ( i = 0 ; i < phenotype.size() - 1 ; i ++)
                                {
                                        Phenotype p =phenotype.get(i);
                            %>
                                &nbsp;&nbsp;<a href="http://omim.org/entry/<%=p.OMIM_ID %>" target="_blank"><%=p.OMIM_ID %></a>&nbsp;&nbsp;|
                            <%
                                }
                            %>
                            &nbsp;&nbsp;<a href="http://omim.org/entry/<%=phenotype.get(i).OMIM_ID %>" target="_blank"><%=phenotype.get(i).OMIM_ID %></a>
                            &nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript:show_abstract_list()" style="color:red">LESS</a>
                        </div>
                        <div id="phenotype_abstract_list" style="display:block" >
                            <font size="3">&nbsp;&nbsp;&nbsp;&nbsp;Query Phenotypes:</font>
                             <%
                                int size = (phenotype.size() > 5) ? 5 : phenotype.size();
                                for ( i = 0 ; i < size - 1 ; i ++)
                                {
                                    Phenotype p =phenotype.get(i);
                            %>
                                &nbsp;&nbsp;<a href="http://omim.org/entry/<%=p.OMIM_ID %>" target="_blank"><%=p.OMIM_ID %></a>&nbsp;&nbsp;|
                            <%
                                }
                            %>
                    &nbsp;&nbsp;<a href="http://omim.org/entry/<%=phenotype.get(i).OMIM_ID %>" target="_blank"><%=phenotype.get(i).OMIM_ID %></a>
                    <%  if (phenotype.size() > 5 ) { %>
                            &nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript:show_full_list()" style="color:red">MORE</a>
                    <%} %>
            </div>
            <%  if (Region == 2){ %>
            <br />
                <div id="gene_full_list_others" style="display:none" >
                <font size="3">&nbsp;&nbsp;&nbsp;&nbsp;Unused Genes:</font>
                <%
                    ArrayList<Gene> othergenes = gf.getOtherGenes(mge, regionInfo);
                    if (othergenes == null)
                    {
                        response.sendRedirect("error.jsp?code=503");
                        if (true) return;
                    }
                    int msize = 0;
                    int totalsize = 0;
                    if(othergenes == null){
                        msize = 0;
                        totalsize = 0;
                    }
                    else{
                        msize = (othergenes.size() > 5) ? 5 : othergenes.size();
                        totalsize = othergenes.size();
                    }
                    for ( i = 0 ; i <= totalsize - 1 ; i ++)
                    {
                            Gene g =othergenes.get(i);
                            %>
                            &nbsp;&nbsp;<a href="genefo.jsp?genename=<%=g.Name %>" target="_blank"><%=g.Name %></a>&nbsp;&nbsp;|
                            <%
                    }
                %>
		&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript:show_abstract_list_others()" style="color:red">LESS</a>
                    </div>
                <div id="gene_abstract_list_others" style="display:block" >
                   <font size="3">&nbsp;&nbsp;&nbsp;&nbsp;Unused Genes in corrsponding locus:</font>
			<%
                                //ArrayList<Gene> othergenes = gf.getOtherGenes(mge, regionInfo);
                                //int msize = 0;
                                //int totalsize = 0;
                                if(othergenes == null){
                                    msize = 0;
                                    totalsize = 0;
                                }
                                else{
                                    msize = (othergenes.size() > 5) ? 5 : othergenes.size();
                                    totalsize = othergenes.size();
                                }
                                    
				
				for ( i = 0 ; i <= msize - 1 ; i ++)
				{
					Gene g =othergenes.get(i);
			%>
					&nbsp;&nbsp;<a href="genefo.jsp?genename=<%=g.Name %>" target="_blank"><%=g.Name %></a>&nbsp;&nbsp;|
			<%
				}
			%>
			<%  if (totalsize > 5 ) { %>
				&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript:show_full_list_others()" style="color:red">MORE</a>
			<%} %>
		</div>
                <% } %>
		</td></tr></table></div>
                <hr>
                <font size="4">Subnetwork Visualization:</font>
        </td></tr>
           <%
            String[] queryphenotypes = queryphenotype.split("\r\n");
            queryphenotype = "";
            for(i=0; i<queryphenotypes.length; i++){
                queryphenotype = queryphenotype + queryphenotypes[i] + "$";
            }
            String[] regionInfos = regionInfo.split("\r\n");
            regionInfo = "";
            for(i=0; i<regionInfos.length; i++){
                regionInfo = regionInfo + regionInfos[i] + "$";
            }
            %>
            <tr><td><span id="assoc"></span></td></tr>
            <tr><td height="530px" valign="top"><div id="cytoscapeweb">Visualizing... Please wait...</div></td></tr>
            <tr><td><span id="legend"></span></td></tr>
   <tr><td><hr></td></tr>
   <tr><td style="word-break:normal;text-align:justify;text-justify:inter-ideograph">The dense phenotype-gene association subnetwork is generated by intersecting HPRD protein-protein interaction network and the phenotype similarity network with the top-ranked phenotypes and genes in the query. To customize the subnetwork, you can specify the number of the top-ranked phenotypes and genes to visualize and then click the "refresh" button.</td></tr> 
   <tr><td>
           <form id="topkForm" name="topkForm" method="post" action="index.html">
                    Number of top-ranked genes:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="topg" name="topg" type="text" value="<%=TopG%>" size="4" /><br />
                    <span style="line-height:9px">&nbsp;</span><br />
                    Number of top-ranked phenotypes:<input id="topp" name="topp" type="text" value="<%=TopP%>" size="4" /><br />
                    <input id="updategraph" type="button" onclick="update_graph()" value="refresh" style="width:130px;" />
           </form>       
       </td>
   </tr>
   <tr><td><hr></td></tr>
            <tr><td>
                    <table width="100%"><tr>
                        <td valign="top">
                            <div id="showcytoscape"  style="display:none;width: 50%" >
                            <input id="hide1" type="button" onclick="hide_cytoscape()" value ="Hide Cytoscape file" />
                            <br>
                            <br>
                            <% for(int x = 0; x < GG.size(); x++){ %>
                            <%= GG.get(x).getSource() %> &nbsp; <%= GG.get(x).getTarget() %><br>
                            <% } %>
                            <% for(int x = 0; x < GP.size(); x++){ %>
                            <%= GP.get(x).getSource() %> &nbsp; OMIM_<%= GP.get(x).getTarget() %><br>
                            <% } %>
                            <% for(int x = 0; x < PP.size(); x++){ %>
                            OMIM_<%= PP.get(x).getSource() %> &nbsp; OMIM_<%= PP.get(x).getTarget() %><br>
                            <% } %>
                            <br>
                            <input id="hide2" type="button" onclick="hide_cytoscape()" value ="Hide Cytoscape file" />
                            </div>
                            <input id="show1" type="button" onclick="show_cytoscape()" value="Show Cytoscape file" />
                    </td>
                    <td valign="top">
                        <div style ="float:right" >
                        <form id="downloadfile" name="downloadfile" enctype="multipart/form-data" action="export.jsp" method="post" target="frmContent">
                                <input name="btn_download" type="button" value="download the result"  onclick="download_file()"/>
                        </form>
                        </div>
                    </td>
                </tr></table>
              
                
            </td>
        </tr>
 
</table>

<script type="text/javascript">
        var vis;
	window.onload = function() {
	// id of Cytoscape Web container div
	var div_id = "cytoscapeweb";
//added by zhanghy at 20120904 
<% if(PP.size() ==0 && GP.size() != 0 && GG.size() != 0 && nonquery.size()!=0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
        <graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).OMIM_ID%>"><data key="label"><%= query.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int h = 0; h < nonquery.size(); h ++){ %><node id="<%= nonquery.get(h).OMIM_ID%>"><data key="label"><%= nonquery.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
        </graph>\
	</graphml>\
	';
           document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
        <% } %>
 <%   if(PP.size() ==0 && GP.size() != 0 && GG.size() != 0 && nonquery.size()==0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
        <graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).OMIM_ID%>"><data key="label"><%= query.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
        </graph>\
	</graphml>\
	';
          document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
        <% } %>
<% if(PP.size() ==0 && GP.size() != 0 && GG.size() == 0 && nonquery.size() != 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
        <graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).OMIM_ID%>"><data key="label"><%= query.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int h = 0; h < nonquery.size(); h ++){ %><node id="<%= nonquery.get(h).OMIM_ID%>"><data key="label"><%= nonquery.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
        </graph>\
	</graphml>\
	';
          document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
        <% } %>
    
<% if(PP.size() ==0 && GP.size() != 0 && GG.size() == 0 && nonquery.size() == 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
        <graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).OMIM_ID%>"><data key="label"><%= query.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
        </graph>\
	</graphml>\
	';
            document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
 <% } %>
    
<% if(PP.size() !=0 && GP.size() != 0 && GG.size() == 0 && nonquery.size() != 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
        <graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).OMIM_ID%>"><data key="label"><%= query.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int h = 0; h < nonquery.size(); h ++){ %><node id="<%= nonquery.get(h).OMIM_ID%>"><data key="label"><%= nonquery.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
        </graph>\
	</graphml>\
	';
          document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
<% } %>
    <% if(PP.size() !=0 && GP.size() != 0 && GG.size() == 0 && nonquery.size() == 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
        <graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).OMIM_ID%>"><data key="label"><%= query.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
        </graph>\
	</graphml>\
	';
           document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
<% } %>
//end by zhanghy
<% if(PP.size() !=0 && GP.size() != 0 && GG.size() != 0 && nonquery.size() != 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
        <graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
                <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).OMIM_ID%>"><data key="label"><%= query.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int h = 0; h < nonquery.size(); h ++){ %><node id="<%= nonquery.get(h).OMIM_ID%>"><data key="label"><%= nonquery.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               </graph>\
	</graphml>\
	';
           document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
<% } %>
    <% if(PP.size() !=0 && GP.size() != 0 && GG.size() != 0 && nonquery.size() == 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
        <graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
                <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).OMIM_ID%>"><data key="label"><%= query.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               </graph>\
	</graphml>\
	';
        document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
<% } %>
<% if(PP.size() !=0 && GP.size() == 0 && GG.size() != 0 && nonquery.size()!=0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).OMIM_ID%>"><data key="label"><%= query.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int h = 0; h < nonquery.size(); h ++){ %><node id="<%= nonquery.get(h).OMIM_ID%>"><data key="label"><%= nonquery.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               </graph>\
	</graphml>\
	';
        document.getElementById('assoc').innerHTML = "INFO: There is no Genes - Phenotypes association to visualize";
     document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
<% } %>
    <% if(PP.size() !=0 && GP.size() == 0 && GG.size() != 0 && nonquery.size()==0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).OMIM_ID%>"><data key="label"><%= query.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
              <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               </graph>\
	</graphml>\
	';
        document.getElementById('assoc').innerHTML = "INFO: There is no Genes - Phenotypes association to visualize";
         document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
<% } %>
<% if(GG.size() ==0 && GP.size() == 0 && PP.size() != 0 && nonquery.size() != 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
                <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).OMIM_ID%>"><data key="label"><%= query.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int h = 0; h < nonquery.size(); h ++){ %><node id="<%= nonquery.get(h).OMIM_ID%>"><data key="label"><%= nonquery.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
              <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               </graph>\
	</graphml>\
	';
        document.getElementById('assoc').innerHTML = "INFO: There is no Genes-Genes/ Genes - Phenotypes association to visualize";
          document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
<% } %>
    <% if(GG.size() ==0 && GP.size() == 0 && PP.size() != 0 && nonquery.size() == 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
                <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).OMIM_ID%>"><data key="label"><%= query.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
              <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               </graph>\
	</graphml>\
	';
        document.getElementById('assoc').innerHTML = "INFO: There is no Genes-Genes/ Genes - Phenotypes association to visualize";
           document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
<% } %>
<% if(PP.size() ==0 && GP.size() == 0 && GG.size() != 0 && nonquery.size()!=0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
                <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).OMIM_ID%>"><data key="label"><%= query.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int h = 0; h < nonquery.size(); h ++){ %><node id="<%= nonquery.get(h).OMIM_ID%>"><data key="label"><%= nonquery.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
              <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               </graph>\
	</graphml>\
	';
        document.getElementById('assoc').innerHTML = "INFO: There is no Phenotypes-Phenotypes / Genes - Phenotypes association to visualize";
           document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
<% } %>
    <% if(GG.size() ==0 && GP.size() == 0 && PP.size() == 0){ %>
	// NOTE the attributes on nodes and edges
        document.getElementById('assoc').innerHTML = "INFO: There is no Genes-Genes/ Genes - Phenotypes/ Phenotypes - Phenotypes association to visualize";
          document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
<% } %>
    <% if(PP.size() ==0 && GP.size() == 0 && GG.size() != 0 && nonquery.size()==0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
                <% for(int h = 0; h < query.size(); h ++){ %><node id="<%= query.get(h).OMIM_ID%>"><data key="label"><%= query.get(h).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               </graph>\
	</graphml>\
	';
        document.getElementById('assoc').innerHTML = "INFO: There is no Phenotypes-Phenotypes / Genes - Phenotypes association to visualize";
           document.getElementById('legend').innerHTML = "<font color='orange'>Orange Rectangles: Phenotypes</font> <br /><font color='blue'>Blue Circles: Genes</font>";
<% } %>
	var visual_style =
	{
		global: { backgroundColor: "#EDEDED" },
		nodes:
		{
			labelFontSize: 16.5,
			shape: {
				passthroughMapper: { attrName: "s"}
			},
			color: {
				passthroughMapper: { attrName: "c"}
			},
			borderWidth: 1,
			borderColor: "#EDEDED",
			size:
			{
				defaultValue: 66,
				continuousMapper: { attrName: "weight", minValue: 66, maxValue: 120}
			},
			labelHorizontalAnchor: "center"

		},
		edges: { width: 2, color: "#0B94B1" }
	};

	// initialization options
	var options =
	{
		swfPath: "./swf/CytoscapeWeb",
		flashInstallerPath: "./swf/playerProductInstall"
	};

	vis = new org.cytoscapeweb.Visualization(div_id, options);

	// callback when Cytoscape Web has finished drawing
	vis.ready(function()
	{
		// add a listener for when nodes and edges are clicked
		vis.addListener("click", "nodes", function(event)
		{
			handle_click(event);
		});
                
                vis.addListener("mouseover", "nodes", function(event)
		{
			handle_mouseover(event);
		});

                vis.addListener("mouseout", "nodes", function(event)
		{
			handle_mouseout(event);
		});

	function handle_click(event)
	{
            var temp = event.target;
            var temp2 = temp.data["s"];
            if(temp2=="ROUNDRECT"){
		var target = event.target;
                window.open("http://omim.org/entry/"+target.data["id"], "OMIM", "channelmode=yes");
            }else{
            	var target = event.target;
                window.open("genefo.jsp?genename="+target.data["id"], "GENE", "channelmode=yes");
            }
	}
        
        var mostcurrent_i;

        function handle_mouseover(event)
        {
            
            var temp = event.target;
            var temp2 = temp.data["s"];
            
            if(temp2=="CIRCLE"){
                
                table = document.getElementById('resulttable');
                
                for(var i = 1; i < table.rows.length; i++){
                    if(table.rows[i].cells[5].firstChild.nodeValue == temp.data["label"]){
                          var idx = "row_" + i;
                          
                          document.getElementById(idx).style.color = "red";
                          document.getElementById(idx).style.fontWeight = "bold";
                          mostcurrent_i = idx;
                          break;
                    }
                }
            }

        }

        function handle_mouseout(event)
        {
           table = document.getElementById('resulttable');

                for(var i = 1; i < table.rows.length; i++){
                          var idx = "row_" + i;

                          document.getElementById(idx).style.color = "black";
                          document.getElementById(idx).style.fontWeight = "normal";

                }
        }


	});

	// draw options
	var draw_options =
	{
		// your data goes here
		network: xml,

		// hide pan zoom
		panZoomControlVisible: true,

		// show edge labels too
		edgeLabelsVisible: false,

 		edgesMerged: true,

		// set the style at initialisation
		visualStyle: visual_style,

		// let's try another layout
		layout: "Tree"
	};


	vis.draw(draw_options);
        ori_height = parent.document.getElementById('frmContent').contentWindow.document.body.scrollHeight;

        //find the height of the internal page
        var the_height= document.body.clientHeight;

        //change the height of the iframe
        parent.document.getElementById('frmContent').height= the_height;

};

function download_file()
{
    var topg = document.topkForm.topg.value;
    var topp = document.topkForm.topp.value;
    window.location.href = 'export.jsp?topg='+topg+'&topp='+topp+'&AlgId=<%=AlgId%>&TopK=<%=TopK%>&query=<%=queryphenotype%>&Region=<%=Region%>&ChrRegions=<%=regionInfo%>&type=2';
}

function update_graph(){
    // window.location.reload(true);
    var topg = document.topkForm.topg.value;
    var topp = document.topkForm.topp.value;
    window.location.href = 'temp.jsp?type=2&topg='+topg+'&topp='+topp+'&type=1&AlgId=<%=AlgId%>&txtTopK=<%=TopK%>&Phenotypes=<%=queryphenotype%>&Region=<%=Region%>&ChrRegions=<%=regionInfo%>';
}
function update_table(){
    var topk = document.tableForm.txtTopK.value;
    var topg = document.topkForm.topg.value;
    var topp = document.topkForm.topp.value;
    window.location.href = 'temp.jsp?topg='+topg+'&topp='+topp+'&type=4&AlgId=<%=AlgId%>&txtTopK='+topk+'&Phenotypes=<%=queryphenotype%>&Region=<%=Region%>&ChrRegions=<%=regionInfo%>';
}
function getDetails(){ 
    document.getElementById("detailTable").style.display = "block";
    document.getElementById("up").style.display = "block";
    document.getElementById("down").style.display = "none";
}

function hideDetails(){ 
    document.getElementById("detailTable").style.display = "none";
    document.getElementById("up").style.display = "none";
    document.getElementById("down").style.display = "block"; 
}
</script>

</body>
</html>