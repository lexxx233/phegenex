<%@page import="sun.misc.FpUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page isThreadSafe="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>Insert title here</title>
<%@ page import="java.io.*"%>
<%@ page import="java.util.*" %>
<%@ page import="com.ppi.*"%>
<%@ page import="com.ppi.PhenotypeFinder"%>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="com.ppi.web.LogHelper" %>
<%@ page import="com.ppi.ContextManager_new" %>

<script type="text/javascript" src="./js/json2.min.js"></script>
<script type="text/javascript" src="./js/AC_OETags.min.js"></script>
<script type="text/javascript" src="./js/cytoscapeweb.min.js"></script>
<script type="text/javascript" src="./js/jquery-latest.js"></script>
<script type="text/javascript" src="./js/jquery.dataTables.min.js"></script>

<link href="css/site_jui.css" rel="stylesheet" type="text/css"/>
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css"/>
<link href="css/theme/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>
<link href="css/style.css" rel="stylesheet" type="text/css"/>

<style media="screen">
	#cytoscapeweb { width: 100%; height: 100%;}
        .dataTables_info { padding-top: 0; }
	.dataTables_paginate { padding-top: 0; }
	.css_right { float: right; }
	#example_wrapper .fg-toolbar { font-size: 0.8em }
	#theme_links span { float: left; padding: 2px 10px; }
</style>


<script type="text/javascript">
	function show_full_list(){
		document.getElementById("gene_full_list").style.display = "block";
		document.getElementById("gene_abstract_list").style.display = "none";
	}
	function show_abstract_list(){
		document.getElementById("gene_abstract_list").style.display = "block";
		document.getElementById("gene_full_list").style.display = "none";
	}
        function show_full_list_others(){
		document.getElementById("gene_full_list_others").style.display = "block";
		document.getElementById("gene_abstract_list_others").style.display = "none";
	}
	function show_abstract_list_others(){
		document.getElementById("gene_abstract_list_others").style.display = "block";
		document.getElementById("gene_full_list_others").style.display = "none";
	}
        
        
        function calcHeight(){
                    //find the height of the internal page
                    var the_height= parent.document.getElementById('frmContent').contentWindow.document.body.scrollHeight;

                    //change the height of the iframe
                    parent.document.getElementById('frmContent').height= the_height;
                }
              
        var ori_height = parent.document.getElementById('frmContent').contentWindow.document.body.scrollHeight;
        
        function show_cytoscape(){
            document.getElementById('showcytoscape').style.display = "block";
            document.getElementById('show1').style.visibility = 'hidden';
            calcHeight();
        }
        
        function hide_cytoscape(){
            document.getElementById('showcytoscape').style.display = "none";
            document.getElementById('show1').style.visibility = 'visible';
            parent.document.getElementById('frmContent').height= ori_height;
        }
     
</script>

<script>
    $(document).ready( function() {
        $('#resulttable').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers"
        });
    } );
</script>

</head>


<body>
    
<%

    Logger log = Logger.getLogger(ContextManager.class);
    //log.info(LogHelper.GetLogItemFromRequest(request));
    ArrayList<Entry> PhenotypeNetwork = ContextManager_new.getPhenotypeEntry(request.getParameter("speciesID"));
    ArrayList<String> geneName = ContextManager_new.getGeneName(request.getParameter("speciesID"));
    ArrayList<Entry> GeneNetwork = ContextManager_new.getGeneEntry(request.getParameter("speciesID"));
    ArrayList<MatchedPhenotypeEntry> fmpe = new ArrayList<MatchedPhenotypeEntry>();
    
    int AlgId = 0;
    int TopK = 10;
    int TopG = 50;
    int TopP = 25;
    String querygene = "";
    AlgId = Integer.parseInt(request.getParameter("AlgId"));
    try{ 
        if(request.getParameter("topg") != null){
            TopG = Integer.parseInt(request.getParameter("topg"));
        }
        if(request.getParameter("topp") != null){
            TopP = Integer.parseInt(request.getParameter("topp")); 
        }
        if(request.getParameter("txtTopK") != null){
            TopK = Integer.parseInt(request.getParameter("txtTopK")); 
        }
                  
        if(TopK>5080){
            TopK = 5080;
        }
          if(TopP>5080){
            TopP = 5080;
        }
          if(TopG>9673){
            TopG = 9673;
        }

    }catch (Exception e){
        response.sendRedirect("error.jsp?code=501");
        if (true) return;
    }
    querygene = request.getParameter("Genes");
    querygene = querygene.replace("$", "\r\n");
    PhenotypeFinder pf = new PhenotypeFinder(request.getParameter("speciesID"));

    ArrayList<Gene> genes = pf.ValidateGenes(querygene);
    if (genes == null){
        response.sendRedirect("error.jsp?code=503");
        if (true) return;
    }
    if (genes.size() == 0)
    {
            response.sendRedirect("error.jsp?code=502");
            if (true) return;
    }

    //Because Local variables are used : AlgId, TopK, and genes, then passed to this is threadsafe
    //<%! are JSP declaration tag
    ArrayList<MatchedPhenotypeEntry> mpes = pf.SearchByGenes(genes, AlgId);

%>
<table width="90%" border="0" align="center">
<!--The 1st row for listing header of the page -->



<!-- 2. Ranking results -->

<!-- The 2nd row for listing a 3-columns information-->
    <tr>
    	<td  align="left" width="80%">
            <font size="4">Ranking Result:</font>
            <div align="right">
                </div>
                <BR>
                <table class="valueListTableStyle"  id="resulttable" border="1" style="text-align:center" width="100%">
                            <thead>
                                <tr style="background-color: #0096FF; color:white">
                                    <th><b><center>Rank</center></b></th>
                                        <th><b><center>Phenotype Name</center></b></th>
                                        <th><b><center>OMIM ID</center></b></th>
                                        <th><b><center>Known Gene Association</center></b></th>
                                        <th><b><center>Relevance Score</center></b></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% 	
                                int i = 0;
                                if(TopK>5080){
                                    TopK = 5080;
                                }
                                //int m_num = TopK;
                                int m_num = TopP;
                                //if(m_num>30){m_num=30;}
                                for (i = 0 ; i < m_num ; i ++)
                                {
                                    MatchedPhenotypeEntry mpe =(MatchedPhenotypeEntry)mpes.get(i);
                                    fmpe.add(mpe);
                                }
                                for (i = 0 ; i < mpes.size() ; i ++)
                                {
                                        MatchedPhenotypeEntry mpe =(MatchedPhenotypeEntry)mpes.get(i);
                                        //fmpe.add(mpe);
                                %>
                                                <tr id="row_<%=i+1%>" class="Gene">
                                                    <td height="40">
                                                                <%=(i + 1) %>
                                                        </td>

                                                        <td>&nbsp
                                                                <a href = "http://www.ncbi.nlm.nih.gov/omim/<%=mpe.OMIM_ID  %>" target="_blank">
                                                                <%=mpe.Name %>
                                                                </a>&nbsp
                                                        </td>
                                                        <td>&nbsp<%=mpe.OMIM_ID%>&nbsp</td>
                                                        <td>&nbsp
                                                            <% if(mpe.ism == true){%>
                                                            <a href="genefo.jsp?genename=<%=mpe.causalgene %>" target="_blank"><%= mpe.causalgene %></a>
                                                            <% }else{%>
                                                            ---
                                                            <%}%>&nbsp
                                                        </td>
                                                        <td>&nbsp
                                                                <%=String.format("%1.2e",mpe.RelScore) %> <!---"%1.2g" -->&nbsp;&nbsp
                                                        </td>
                                                </tr>
                                <%
                                        }

                                %>
                            </tbody>
                            <tfoot>
                                <tr style="background-color: #0096FF; color:white">
                                    <th><b><center>Rank</center></b></th>
                                        <th><b><center>Phenotype Name</center></b></th>
                                        <th><b><center>OMIM ID</center></b></th>
                                        <th><b><center>Known Gene Association</center></b></th>
                                        <th><b><center>Relevance Score</center></b></th>
                                </tr>
                            </tfoot>

              
<!-- 3. Subnetwork visualization -->

                                <% // Generate genes node
            String justgame = "49";
            ArrayList<MatchedGeneEntry> matchedgenes = pf.getDisplayGenotype(querygene, TopG);

            ArrayList<Edge> GG = pf.getGeneEdges(matchedgenes);
            ArrayList<Edge> GP = pf.getNetworkEdges(matchedgenes, fmpe);
            ArrayList<Edge> PP = pf.getPhenotypeEdges(fmpe);

            //here is the new code to delete nodes which have no edge to any onther node
           // matchedgenes = pf.deleteAloneGenes(matchedgenes, GG, GP);
            //matchedgenes = pf.mdeleteAloneGenes(genes, matchedgenes, GG, GP);
            ArrayList<MatchedGeneEntry> query = pf.displayedQueryGene(querygene, matchedgenes);
            ArrayList<MatchedGeneEntry> nonquery = pf.displayedNonQueryGene(query, matchedgenes);
            
           // fmpe = pf.mdeleteAlonePhenotype(matchedgenes, fmpe, PP, GP);
           // fmpe = pf.deleteAlonePhenotype(fmpe, PP, GP);
            GANDP gp = pf.deleteUnReachableGenesAndPhenotypes(query, nonquery, fmpe, GG, PP, GP);
            matchedgenes = gp.mge;
            fmpe = gp.mpe;
            GG = pf.updateGG(GG, matchedgenes);
            GP = pf.updateGP(GP, matchedgenes, fmpe);
            PP = pf.updatePP(PP, fmpe);
            query = pf.displayedQueryGene(querygene, matchedgenes);
            nonquery = pf.displayedNonQueryGene(query, matchedgenes);
            
            
        %>
			</table>
		</td>
	</tr>
        
        <!-- 1. Query gene set list -->
 <tr><td>
         <hr>
            <BR>
        <div id="down" style="display: block; vertical-align: central"> <font size="4">Query Details&nbsp;</font><img src="images/down.png" width="18" height="18" onclick="getDetails()" align="center" style="border: none"/></div>
        <div id="up" style="display: none; vertical-align: central"><font size="4">Query Details&nbsp;</font><img src="images/up.png" width="18" height="18" onclick="hideDetails()" align="center" style="border:none" /></div>
 </td></tr>
    <tr> 
    	<td>
        <div id ="detailTable" style="display:none">
        <table><tr><td>
    	<div id="gene_full_list" style="display:none" >
            <font size="3">&nbsp;&nbsp;&nbsp;&nbsp;Query Genes:</font>
			<%
				
				for ( i = 0 ; i < genes.size() - 1 ; i ++)
				{
					Gene g =genes.get(i);
			%>
					&nbsp;&nbsp;<a href="genefo.jsp?genename=<%=g.Name %>" target="_blank"><%=g.Name %></a>&nbsp;&nbsp;|
			<%
				}
			%>
			&nbsp;&nbsp;<a href="genefo.jsp?genename=<%=genes.get(i).Name %>" target="_blank"><%=genes.get(i).Name%></a>
			&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript:show_abstract_list()" style="color:red">LESS</a>
		</div>
		<div id="gene_abstract_list" style="display:block" >
                    <font size="3">&nbsp;&nbsp;&nbsp;&nbsp;Query Genes:</font>
			<%
				int size = (genes.size() > 5) ? 5 : genes.size();
				for ( i = 0 ; i < size - 1 ; i ++)
				{
					Gene g =genes.get(i);
			%>
					&nbsp;&nbsp;<a href="genefo.jsp?genename=<%=g.Name %>" target="_blank"><%=g.Name %></a>&nbsp;&nbsp;|
			<%
				}
			%>
			&nbsp;&nbsp;<a href="genefo.jsp?genename=<%=genes.get(i).Name %>" target="_blank"><%=genes.get(i).Name%></a>
			<%  if (genes.size() > 5 ) { %>
				&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript:show_full_list()" style="color:red">MORE</a>
			<%} %>
		</div>
		</td>
  	</tr>
        <tr>
            <td>
                <% 
                 ArrayList<Gene> othergenes = pf.getOtherGenes(querygene);
                if(othergenes!=null&&othergenes.size()>0){%>
                 <div id="gene_full_list_others" style="display:none" >
                <font size="3">&nbsp;&nbsp;&nbsp;&nbsp;Unused Query Genes:</font>
                <%
                    if (othergenes == null)
                    {
                        response.sendRedirect("error.jsp?code=503");
                        if (true) return;
                    }
                    int msize = 0;
                    int totalsize = 0;
                    if(othergenes == null){
                        msize = 0;
                        totalsize = 0;
                    }
                    else{
                        msize = (othergenes.size() > 5) ? 5 : othergenes.size();
                        totalsize = othergenes.size();
                    }
                    for ( i = 0 ; i <= totalsize - 1 ; i ++)
                    {
                            Gene g =othergenes.get(i);
                            %>
                            &nbsp;&nbsp;<a href="genefo.jsp?genename=<%=g.Name %>" target="_blank"><%=g.Name %></a>&nbsp;&nbsp;|
                            <%
                    }
                %>
		&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript:show_abstract_list_others()" style="color:red">LESS</a>
                    </div>
                <div id="gene_abstract_list_others" style="display:block" >
                    <font size="3">&nbsp;&nbsp;&nbsp;&nbsp;Unused Query Genes:</font>
			<%
                                if(othergenes == null){
                                    msize = 0;
                                    totalsize = 0;
                                }
                                else{
                                    msize = (othergenes.size() > 5) ? 5 : othergenes.size();
                                    totalsize = othergenes.size();
                                }
				for ( i = 0 ; i <= msize - 1 ; i ++)
				{
					Gene g =othergenes.get(i);
			%>
					&nbsp;&nbsp;<a href="genefo.jsp?genename=<%=g.Name %>" target="_blank"><%=g.Name %></a>&nbsp;&nbsp;|
                        <%
				}
			%>
			<%  if (totalsize > 5 ) { %>
				&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript:show_full_list_others()" style="color:red">MORE</a>
			<%} %>
		</div>
           <%}%></td>
        </tr>
 
        <tr>
            <td >
                <div id="gene_list_unmatched" style="display:block" >
            <%
				ArrayList<Gene> unmatched = pf.getUnmatched(querygene);
				
                                if(unmatched.size() > 0){ %>
                                    <font size="3">&nbsp;&nbsp;&nbsp;&nbsp;Unmatched Query:</font>
                                <% }
                                
                                for (int k = 0 ; k < unmatched.size() ; k ++)
				{
					Gene g = unmatched.get(k);
			%>
					&nbsp;&nbsp;<a href="genefo.jsp?genename=<%=g.Name %>" target="_blank"><%=g.Name %></a>&nbsp <% if(k < unmatched.size()-1){ %>|<% }%>
			<%
				}
			%>
                </div>
            </td>
        </tr>
        </table></div>
                </td></tr>
    
    <%
    String[] querygenes = querygene.split("\r\n");
            
            querygene = "";
            for(i=0; i<querygenes.length; i++){
                querygene = querygene + querygenes[i] + "$";
            }
%>
    
    
        <tr><td>
               <hr> 
                <font size="4">Subnetwork Visualization:</font>
        </td></tr>
        <tr><td>
                
                <span id="assoc"></span>
         </td></tr>
        <tr><td height="530px" valign="top"><div id="cytoscapeweb">Visualizing... Please wait...</div></td></tr>
        <tr><td><span id="legend"></span></td></tr>
       
        <tr><td><hr></td></tr>      
        <tr><td style="word-break:normal;text-align:justify;text-justify:inter-ideograph">The dense phenotype-gene association subnetwork is generated by intersecting HPRD protein-protein interaction network and the phenotype similarity network with the top-ranked phenotypes and genes in the query. To customize the subnetwork, you can specify the number of the top-ranked phenotypes and genes to visualize and then click the "refresh" button.  
</td></tr> 
        <tr><td>
                 <form id="topkForm" name="topkForm" method="post">
                     Number of top-ranked genes:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="topg" name="topg" type="text" value="<%=TopG%>" size="4" /><br />
                     <span style="line-height:9px">&nbsp;</span><br />
                    Number of top-ranked phenotypes:<input id="topp" name="topp" type="text" value="<%=TopP%>" size="4" /><br />
                    <input type="hidden" id="speciesID" value="<%=request.getParameter("speciesID")%>"/>
                    <input id="updategraph" type="button" onclick="update_graph()" value="refresh"  style="width:130px;" />
                 </form>
            </td></tr>
        <tr><td><hr></td></tr>
        <tr><td>
                <table width="100%">
                    <tr>
                    <td valign="top">
                        <div id="showcytoscape" style="display:none; width: 50%" >
                        <input id="hide1" type="button" onclick="hide_cytoscape()" value ="Hide Cytoscape file" />
                        <br>
                        <br>
                        <% for(int x = 0; x < GG.size(); x++){ %>
                        <%= GG.get(x).getSource() %> &nbsp; <%= GG.get(x).getTarget() %><br>      
                        <% } %>
                        <% for(int x = 0; x < GP.size(); x++){ %>
                        <%= GP.get(x).getSource() %> &nbsp; <%= GP.get(x).getTarget() %><br>      
                        <% } %>
                        <% for(int x = 0; x < PP.size(); x++){ %>
                        <%= PP.get(x).getSource() %> &nbsp; <%= PP.get(x).getTarget() %><br>      
                        <% } %>
                        <br>
                        <input id="hide2" type="button" onclick="hide_cytoscape()" value ="Hide Cytoscape file" />
                        </div>
                        <input id="show1" type="button" onclick="show_cytoscape()" value="Show Cytoscape file" />
                    </td>
                    <td valign="top">
                        <div style ="float:right">
                        <form id="downloadfile" name="downloadfile" enctype="multipart/form-data" action="export.jsp" method="post"  target="_blank">
                            <input name="btn_download" type="button" value="download the result"  onclick="download_file()"/>
                        </form>
                        </div>
                    </td>
                </tr></table>
            </td>
        </tr>
  
</table>
<br>

<script type="text/javascript">
    var vis;
    window.onload = function() {
    // id of Cytoscape Web container div
    var div_id = "cytoscapeweb";
//add by zhanghy at 20120904
<% if(GG.size() == 0 && GP.size() == 0 && PP.size() ==0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               </graph>\
	</graphml>\
	';
        document.getElementById('assoc').innerHTML = "INFO: There is no Genes-Genes association and Genes-Phenotypes association and Phenotypes-Phenotypes association to visualize";
        document.getElementById('legend').innerHTML = "<font color='blue'>Blue Circle: Query Genes</font> <br><font color='orange'>Orange Rectangular: Disease Phenotypes</font>";
<% } %>
<% if(GG.size() == 0 && GP.size() == 0 && PP.size() !=0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int x = 0; x < fmpe.size(); x ++){ %><node id="<%= fmpe.get(x).OMIM_ID%>"><data key="label"><%= fmpe.get(x).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               </graph>\
	</graphml>\
	';
        document.getElementById('assoc').innerHTML = "INFO: There is no Genes-Genes association and Genes-Phenotypes association to visualize";
        
        document.getElementById('legend').innerHTML = "<font color='blue'>Blue Circle: Query Genes</font> <br><font color='orange'>Orange Rectangular: Disease Phenotypes</font>";
<% } %>
<% if(GG.size() == 0 && GP.size() != 0 && PP.size() ==0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int x = 0; x < fmpe.size(); x ++){ %><node id="<%= fmpe.get(x).OMIM_ID%>"><data key="label"><%= fmpe.get(x).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';
         document.getElementById('assoc').innerHTML = "INFO: There is no Genes-Genes association and Phenotypes-Phenotypes association to visualize";
         document.getElementById('legend').innerHTML = "<font color='blue'>Blue Circle: Query Genes</font> <br><font color='orange'>Orange Rectangular: Disease Phenotypes</font>";
<% } %>
<% if(GG.size() == 0 && GP.size() != 0 && PP.size() !=0 ){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int x = 0; x < fmpe.size(); x ++){ %><node id="<%= fmpe.get(x).OMIM_ID%>"><data key="label"><%= fmpe.get(x).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';
        
        document.getElementById('assoc').innerHTML = "INFO: There is no Genes - Genes association to visualize";
         document.getElementById('legend').innerHTML = "<font color='blue'>Blue Circle: Query Genes</font> <br><font color='orange'>Orange Rectangular: Disease Phenotypes</font>";
<% } %>
<% if(GG.size() != 0 && GP.size() == 0 && PP.size() ==0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';
        
       document.getElementById('assoc').innerHTML = "INFO: There is no Genes-Phenotypes association and Phenotypes-Phenotypes association to visualize";
         document.getElementById('legend').innerHTML = "<font color='blue'>Blue Circle: Query Genes</font> <br><font color='orange'>Orange Rectangular: Disease Phenotypes</font>";
<% } %>
<% if(GG.size() != 0 && GP.size() == 0 && PP.size() != 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';

        document.getElementById('assoc').innerHTML = "INFO: There is no Genes-Phenotypes association to visualize";
         document.getElementById('legend').innerHTML = "<font color='blue'>Blue Circle: Query Genes</font> <br><font color='orange'>Orange Rectangular: Disease Phenotypes</font>";
<% } %>
<% if(GG.size() != 0 && GP.size() != 0 && PP.size() == 0){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int x = 0; x < fmpe.size(); x ++){ %><node id="<%= fmpe.get(x).OMIM_ID%>"><data key="label"><%= fmpe.get(x).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';
       document.getElementById('assoc').innerHTML = "INFO: There is no Phenotypes-Phenotypes association to visualize";
         document.getElementById('legend').innerHTML = "<font color='blue'>Blue Circle: Query Genes</font> <br><font color='orange'>Orange Rectangular: Disease Phenotypes</font>";
<% } %>    
<% if(GG.size() != 0 && GP.size() != 0 && PP.size() !=0 ){ %>
	// NOTE the attributes on nodes and edges
	var xml = '\
	<graphml>\
	<key id="label" for="all" attr.name="label" attr.type="string"/>\
	<key id="weight" for="node" attr.name="weight" attr.type="double"/>\
	<key id="s" for="node" attr.name="s" attr.type="string"/>\
	<key id="c" for="node" attr.name="c" attr.type="string"/>\
	<graph edgedefault="circle">\\n\\n\
               <% for(int h = 0; h < matchedgenes.size(); h ++){ %><node id="<%= matchedgenes.get(h).GeneName%>"><data key="label"><%= matchedgenes.get(h).GeneName%></data><data key="weight">1.0</data><data key="s">CIRCLE</data><data key="c">#0066CC</data></node>\<% } %>
               <% for(int x = 0; x < fmpe.size(); x ++){ %><node id="<%= fmpe.get(x).OMIM_ID%>"><data key="label"><%= fmpe.get(x).OMIM_ID%></data><data key="weight">1.0</data><data key="s">ROUNDRECT</data><data key="c">#FF6600</data></node>\<% } %>
               <% for(int m = 0; m < PP.size(); m++){ String temp1 = PP.get(m).getSource(); String temp2 = PP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GG.size(); m++){ String temp1 = GG.get(m).getSource(); String temp2 = GG.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
               <% for(int m = 0; m < GP.size(); m++){ String temp1 = GP.get(m).getSource(); String temp2 = GP.get(m).getTarget();%><edge source="<%= temp1%>" target="<%= temp2%>"></edge>\<% } %>
                </graph>\
	</graphml>\
	';
         document.getElementById('legend').innerHTML = "<font color='blue'>Blue Circle: Query Genes</font> <br><font color='orange'>Orange Rectangular: Disease Phenotypes</font>";
<% } %>

	var visual_style =
	{
		global: { backgroundColor: "#EDEDED" },
		nodes:
		{
			labelFontSize: 16.5,
			shape: {
				passthroughMapper: { attrName: "s"}
			},
			color: {
				passthroughMapper: { attrName: "c"}
			},
			borderWidth: 1,
			borderColor: "#EDEDED",
			size:
			{
				defaultValue: 66,
				continuousMapper: { attrName: "weight", minValue: 66, maxValue: 120}
			},
			labelHorizontalAnchor: "center"

		},
		edges: { width: 2, color: "#0B94B1" }
	};

	// initialization options
	var options =
	{
		swfPath: "./swf/CytoscapeWeb",
		flashInstallerPath: "./swf/playerProductInstall"
	};

	//var vis = new org.cytoscapeweb.Visualization(div_id, options);
 vis = new org.cytoscapeweb.Visualization(div_id, options);
	// callback when Cytoscape Web has finished drawing
	vis.ready(function()
	{
		// add a listener for when nodes and edges are clicked
		vis.addListener("click", "nodes", function(event)
		{
			handle_click(event);
		});

                vis.addListener("mouseover", "nodes", function(event)
		{
			handle_mouseover(event);
		});

                vis.addListener("mouseout", "nodes", function(event)
		{
			handle_mouseout(event);
		});

	function handle_click(event)
	{
            var temp = event.target;
            var temp2 = temp.data["s"];
            if(temp2=="ROUNDRECT"){
		var target = event.target;
                window.open("http://omim.org/entry/"+target.data["id"], "OMIM", "channelmode=yes");
            }else{
            	var target = event.target;
                window.open("genefo.jsp?genename="+target.data["id"], "GENE", "channelmode=yes");
            }
	}

        var mostcurrent_i;

        function handle_mouseover(event)
        {
            var temp = event.target;
            var temp2 = temp.data["s"];
            if(temp2=="ROUNDRECT"){
                
                table = document.getElementById('resulttable');
                
                for(var i = 1; i < table.rows.length; i++){
                    if(table.rows[i].cells[2].firstChild.nodeValue == temp.data["label"]){
                          var idx = "row_" + i;
                          
                          document.getElementById(idx).style.color = "red";
                          document.getElementById(idx).style.fontWeight = "bold";
                          mostcurrent_i = idx;
                          break;
                    }
                }
            }

        }

        function handle_mouseout(event)
        {
           table = document.getElementById('resulttable');
                for(var i = 1; i < table.rows.length; i++){
                          var idx = "row_" + i;
                          document.getElementById(idx).style.color = "black";
                          document.getElementById(idx).style.fontWeight = "normal";
                }
        }
	});

	// draw options
	var draw_options =
	{
		// your data goes here
		network: xml,

		// hide pan zoom
		panZoomControlVisible: true,

		// show edge labels too
		edgeLabelsVisible: false,

 		edgesMerged: true,

		// set the style at initialisation
		visualStyle: visual_style,

		// let's try another layout
		layout: "Tree"
	};
       vis.draw(draw_options);
       ori_height = parent.document.getElementById('frmContent').contentWindow.document.body.scrollHeight;
       var the_height= document.body.clientHeight;

       //change the height of the iframe
       parent.document.getElementById('frmContent').height= the_height;
      
};
function download_file()
{
    var topg = document.topkForm.topg.value;
    var topp = document.topkForm.topp.value;
    window.location.href = 'export.jsp?topg='+topg+'&topp='+topp+'&AlgId=<%=AlgId%>&TopK=<%=TopK%>&speciesID=<%=request.getParameter("speciesID")%>&query=<%=querygene%>&type=1';
}
function update_graph(){
    // window.location.reload(true);
    var topg = document.topkForm.topg.value;
    var topp = document.topkForm.topp.value; 
    window.location.href = 'temp.jsp?topg='+topg+'&topp='+topp+'&type=1&AlgId=<%=AlgId%>&txtTopK=<%=TopK%>&speciesID=<%=request.getParameter("speciesID")%>&Genes=<%=querygene%>';

}
function update_table(){
    var topk = document.tableForm.txtTopK.value;
    var topg = document.topkForm.topg.value;
    var topp = document.topkForm.topp.value;
    window.location.href = 'temp.jsp?topg='+topg+'&topp='+topp+'&type=3&AlgId=<%=AlgId%>&txtTopK='+topk+'&Genes=<%=querygene%>';
}

function getDetails(){ 
    document.getElementById("detailTable").style.display = "block";
    document.getElementById("up").style.display = "block";
    document.getElementById("down").style.display = "none";
}

function hideDetails(){ 
    document.getElementById("detailTable").style.display = "none";
    document.getElementById("up").style.display = "none";
    document.getElementById("down").style.display = "block"; 
}
</script>

</body>
</html>