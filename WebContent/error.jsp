<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page import="com.ppi.util.ErrorMng"%>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Error/Warnning Report</title>
<style type="text/css">
<!--
.STYLE1 {font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; color: #fff; font-weight: bold;}
.STYLE2 {font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; color: #000}
-->
</style>
</head>
<body>
<p align="center" class="STYLE1">
<br />
<br />
<% String ErrorCode = request.getParameter("code"); 
    if(ErrorCode.equals("502")){ %>
<table width="80%" border="0"  align="center" cellpadding="0" cellspacing="0" >
	<tr>
		<td  style="background-color:#09F;  -moz-border-radius: 10px; -webkit-border-radius: 10px; -khtml-border-radius:8px; border-radius: 10px; height: 30px" class="STYLE1" style="height:31px">
			<div id="title" style="padding-left:10px">Warnning</div>
		</td>
	</tr>
        <tr>
            <td>&nbsp;</td>
        </tr>

	<tr>
            <td class="STYLE2" style="height:50" >
			<div id="content" style="border-top:1px solid #cccccc;border-left:1px solid #cccccc;
			border-bottom: 1px solid #cccccc;border-right: 1px solid #cccccc;padding:30px;line-height:1px; -moz-border-radius: 10px; -webkit-border-radius: 10px;
                        -khtml-border-radius:10px; border-radius: 10px;">No gene or phenotype to query!<br />
			</div>
		</td>
	</tr>
        </table>
 <%} else{%>
 
<table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" style="height:auto" >
	<tr>
		<td  style="background-color:#09F;  -moz-border-radius: 10px; -webkit-border-radius: 10px;
                        -khtml-border-radius:8px; border-radius: 10px; height: 30px" class="STYLE1">
			<div id="title" style="padding-left:10px">Error</div>
		</td>
	</tr>
        <tr>
            <td>&nbsp;</td>
        </tr>

	<tr>
            <td class="STYLE2">
                <div id="content" style="border-top:1px solid #cccccc;border-left:1px solid #cccccc;
                border-bottom: 1px solid #cccccc;border-right: 1px solid #cccccc;padding:30px;line-height:1px; -moz-border-radius: 10px; -webkit-border-radius: 10px;
                -khtml-border-radius:10px; border-radius: 10px;">Error Code:<%=ErrorCode %><br /><br />
                    Description:<%=ErrorMng.GetErrorDescription(ErrorCode) %><br />
                </div>
            </td>
	</tr>
        
</table>
<% } %>
</p>
</body>
</html>