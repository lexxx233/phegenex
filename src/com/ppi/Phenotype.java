package com.ppi;

public class Phenotype {
	public String Name;
	public int	Id; 
	public String OMIM_ID;


        public Phenotype(String name, int id)
	{
		Name = name;
		Id = id;
	}
	
        public Phenotype(String name, int id, String omim){
            Name = name;
            Id = id;
            OMIM_ID = omim;
        }
        
	public Phenotype(int Index, String NameAndOMIMId)
	{
		Id = Index;
		String[] rslt = NameAndOMIMId.split("\t");
		OMIM_ID = rslt[0];
		Name = rslt[1];
	}
}
