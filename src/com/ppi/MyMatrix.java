/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ppi;

/**
 *
 * @author zhanghy
 */
public class MyMatrix {
    public MyMatrix(int mrow, int mcol){
        row = mrow;
        col = mcol;
        m_matrix = new float[row][col];
        for(int i=0; i<row; i++){
            for(int j=0; j<col; j++){
                m_matrix[i][j] = 0;
            }
        }
    }
    public float[][] m_matrix;
    public int row;
    public int col;
    public void addMatrix(MyMatrix matrix1){
        int i,j;
        for(i=0; i<row; i++){
            for(j=0; j<col; j++){
                m_matrix[i][j] += matrix1.m_matrix[i][j];
            }
        }
    }
    public void mulMatrix(MyMatrix matrix1){
        int i,j,k;
        MyMatrix temp = new MyMatrix(row,matrix1.col);
        for(i=0; i<row; i++){
            for(j=0; j<matrix1.col; j++){
                for(k=0; k<col; k++){
                    temp.m_matrix[i][j] += m_matrix[i][k] * matrix1.m_matrix[k][j];
                }
            }
        }
        col = matrix1.col;
        m_matrix = temp.m_matrix;
    }
    
    public void normalizeRow(){
        for(int i=0; i<row; i++){
            float isum = sumRow(i);
            for(int j=0; j<col; j++){
                this.m_matrix[i][j] /= isum;
            }
        }
    }
    
    public float sumRow(int idx){
        float sum = 0;
        for(int j=0; j<col; j++){
            sum += this.m_matrix[idx][j];
        }
        return sum;
    }
}
