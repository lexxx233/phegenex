package com.ppi;

import java.io.Serializable;
import java.util.ArrayList;

public class MatchedGeneEntry implements Serializable{
    /**
    * 
    */
    private static final long serialVersionUID = 1L;
    //In omim list - This is static
    ArrayList<Integer> inomim;
    ArrayList<Integer> ismorbid;
    ArrayList<String> morbidPhenotype;
    ArrayList<String> lsomim;
    
    public String GeneName;
    public int GeneId;
    public float RelScore;
    public boolean isInOMIM;
    public boolean ism;
    public String disease;
    public String OMIM_ID;
    //added by zhanghy at 20120904
    public String location;
    //end
        //Check if in OMIM
    
    public MatchedGeneEntry(String geneName, int geneId, float relScore, String _id)
    {
        
                
        inomim = ContextManager_new.getInOMIM(_id);
        ismorbid = ContextManager_new.getIsDisease(_id);
        morbidPhenotype = ContextManager_new.getMorbidPhenotypes(_id);
        lsomim = ContextManager_new.getOmimId(_id);
        
        GeneName = geneName;
        GeneId = geneId;
        RelScore = relScore;
        OMIM_ID = lsomim.get(geneId);

        isInOMIM = isOMIM(GeneId+1);
        ism = isMorbid(GeneId+1);
        /*
        if(RelScore == Float.NaN){
            RelScore = 9999.99;
        }*/
    }
        
    public MatchedGeneEntry(Gene g, float relScore, String _id)
    {
        
        inomim = ContextManager_new.getInOMIM(_id);
        ismorbid = ContextManager_new.getIsDisease(_id);
        morbidPhenotype = ContextManager_new.getMorbidPhenotypes(_id);
        lsomim = ContextManager_new.getOmimId(_id);
        
        GeneName = g.Name;
        GeneId = g.Id;
        RelScore = relScore;
        isInOMIM = isOMIM(GeneId+1);
        ism = isMorbid(GeneId+1);
        
        /*
        if(RelScore == Float.NaN){
            RelScore = 9999.99;
        }*/
    }
    
    public void setLocation(String input){
        location = input;
    }     
    private boolean isOMIM(int input){
        if(inomim.contains(input)){
            return true;
        }else{
            return false;
        }
    }

    public void setScore(float input){
        RelScore = input;
    } 


    private boolean isMorbid(int input){
        if(ismorbid.contains(input)){
            int idx = ismorbid.indexOf(input);
            disease = morbidPhenotype.get(idx);
            return true;
        }else{
            return false;
        }
    }

    public void setIsMorbid(){
        ism = isMorbid(GeneId+1);
    }
}
