package com.ppi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class GenetypeFinder {

        //Species id
        String id = "1";
         //var to store info in these file should not be mutated
        String line = null;
        
        int GENE_CNT, PHENOTYPE_CNT, GENE_IN_ASSOCIATION_CNT, PHENOTYPE_IN_ASSOCIATION_CNT;
        
        ArrayList<String> geneName;
        ArrayList<Phenotype> phenotypes;
        float [] wMatrix;
        int [][]wIndex;
        float[][] geneMatrix, RidgeRegressionMatrix, PhenotypeMatrix;
        //Network
        ArrayList<Entry> geneEntry, phenotypeEntry, networkEntry;
        ArrayList<Phenotype> phenotype_file; 
        //chromosomal location
        ArrayList<String> chromosome, startpos, stoppos, locgene, lociflag;
        
        //added by zhanghy at 20120904, used for adding chromosome loci to the matched genes
        
        //added by Thanh 20130621 refactoring to handle multiple species
        public GenetypeFinder(String _id){//if not specified species id, the species default to human
            id = _id;
            
            GENE_CNT = ContextManager_new.GENE_CNT.get(id);
            PHENOTYPE_CNT = ContextManager_new.PHENOTYPE_CNT.get(id);
            GENE_IN_ASSOCIATION_CNT = ContextManager_new.GENE_IN_ASSOCIATION_CNT.get(id);
            PHENOTYPE_IN_ASSOCIATION_CNT = ContextManager_new.PHENOTYPE_IN_ASSOCIATION_CNT.get(id);
            
            geneName = ContextManager_new.getGeneName(id);
            phenotypes = ContextManager_new.getPhenotypes(id);
            wMatrix = ContextManager_new.getwMatrix(id);
            wIndex = ContextManager_new.getwIndex(id);
            geneEntry = ContextManager_new.getGeneEntry(id);
            phenotypeEntry = ContextManager_new.getPhenotypeEntry(id);
            networkEntry = ContextManager_new.getNetworkEntry(id);
            phenotype_file = ContextManager_new.getLsPhenotypes(id);
            chromosome = ContextManager_new.getChromosome(id);
                                                                                                                                                                                                   startpos = ContextManager_new.getStartpos(id);
            stoppos = ContextManager_new.getStoppos(id);
            locgene = ContextManager_new.getLocgene(id);
            lociflag = ContextManager_new.getLociflag(id);
            geneMatrix = ContextManager_new.getGeneMatrix(id);
            RidgeRegressionMatrix = ContextManager_new.getRidgeRegressionMatrix_g(id);
            PhenotypeMatrix = ContextManager_new.getPhenotypeMatrix(id);
        }
     
        public ArrayList<MatchedGeneEntry> getGeneLocation(ArrayList<MatchedGeneEntry> mge)
        { 
            for(int i = 0 ; i < mge.size(); i++)
            {   
                int j = 0;
                String tempLoci="";
                MatchedGeneEntry tempGene = mge.get(i);
                String tempName = tempGene.GeneName;
                for(int op = 0; op < chromosome.size(); op++){
                    if(locgene.get(op).equals(tempName))
                    {
                        String t_chromosome = chromosome.get(op);
                        String t_startpos = startpos.get(op);
                        String t_stoppos = stoppos.get(op);
                        String t_flag = lociflag.get(op);
                        if(j == 0)
                        {
                            tempLoci = t_chromosome + ":" + t_startpos + "-" + t_stoppos;
                            j = 1;
                            mge.get(i).setLocation(tempLoci);
                            continue;
                        }
                        tempLoci = tempLoci + "<br>" + t_chromosome + ":" + t_startpos + "-" + t_stoppos;
                        mge.get(i).setLocation(tempLoci);
                    }
                }
                if(tempLoci.equals("")|| tempLoci == null || tempLoci.isEmpty()){
                    tempLoci = "unknown";
                    mge.get(i).setLocation(tempLoci);
                }
                
            }
/*          int i = 0;
            int j = 0;
            String tempLoci="";
            for(int op = 0; op < chromosome.size(); op++){
                String t_chromosome = chromosome.get(op);
                if(t_chromosome.equals("0"))
                {
                    continue;
                }
                String t_startpos = startpos.get(op);
                String t_stoppos = stoppos.get(op);
                String t_flag = lociflag.get(op);
                String genename = locgene.get(op);
                i = mge.indexOf(genename);
                if(i<0){continue;}
                if(j == 0)
                {
                    tempLoci = t_chromosome + ":" + t_startpos + "-" + t_stoppos;
                    j = 1;
                    mge.get(i).setLocation(tempLoci);
                    continue;
                }
                tempLoci = tempLoci + "<br>" + t_chromosome + ":" + t_startpos + "-" + t_stoppos;
                mge.get(i).setLocation(tempLoci);
            }*/
            return mge;
        }
        //added by zhanghy at 20120903, used for get genes on specified loci
        public ArrayList<MatchedGeneEntry> getSpecifiedGenes(ArrayList<MatchedGeneEntry> mge, String regionInfo)
        {
            ArrayList<Gene> genes = new ArrayList<Gene>();
            regionInfo = regionInfo.toUpperCase();
            regionInfo = regionInfo.trim();
            if(regionInfo.isEmpty()){
                return mge;
            }
            String[] strRegions = regionInfo.split("\r\n");
            
            for(int i = 0 ; i < strRegions.length; i++)
            {
                String tempstr = strRegions[i].trim();
                if(tempstr.isEmpty()){
                    continue;
                }
                String[] st = tempstr.split(":");
                st[0] = st[0].trim();
                if(st.length == 3){
                    if(st[0].equals("CHROMOSOME")){}
                    else{return null;}
                    String tempchromosome = st[1].trim();
                    try{
                        if(!tempchromosome.equals("X")&&!tempchromosome.equals("Y")){
                            int chroIndex = Integer.parseInt(tempchromosome);
                            if(chroIndex<1||chroIndex>22){
                                return null;
                            }
                        }
                    }
                    catch(Exception e){
                        return null;
                    }
                    String[] sts = st[2].split("-");
                    if(sts.length == 2)
                    {
                        String tempstart = sts[0].trim();
                        String tempstop = sts[1].trim();
                        try{
                            for(int op = 0; op < chromosome.size(); op++){
                                if(tempchromosome.equals(chromosome.get(op))){
                                    if(Integer.parseInt(startpos.get(op)) >= Integer.parseInt(tempstart) && Integer.parseInt(stoppos.get(op)) <= Integer.parseInt(tempstop)){
                                        int index = geneName.indexOf(locgene.get(op));
                                        genes.add(new Gene(locgene.get(op), index));
                                    }
                                }
                            }
                        }
                        catch(Exception e)
                        {
                            return null;
                        }
                    }
                    else{return null;}
                    continue;
                } //upstream:k:GENE
                else{return null;}
            }
            ArrayList<MatchedGeneEntry> tmpgenes = new ArrayList<MatchedGeneEntry>();
	    for (int i = 0; i < mge.size(); i ++)
            {
                MatchedGeneEntry tmpGene = mge.get(i);
                for (int j = 0; j < genes.size(); j ++)
                {
                    if (genes.get(j).Id == tmpGene.GeneId)
                    {
                        tmpgenes.add(tmpGene);
                        break;
                    }
                }
            }
            return tmpgenes;
        }
        
        
        public ArrayList<Gene> getOtherGenes(ArrayList<MatchedGeneEntry> mge, String regionInfo)
        {
            ArrayList<Gene> genes = new ArrayList<Gene>();
            regionInfo = regionInfo.toUpperCase();
            regionInfo = regionInfo.trim();
            if(regionInfo.isEmpty()){
                return null;
            }
            String[] strRegions = regionInfo.split("\r\n");
            for(int i = 0 ; i < strRegions.length; i++)
            {
                String tempstr = strRegions[i];
                tempstr = tempstr.trim();
                if(tempstr.isEmpty()){
                    continue;
                }
                String[] st = tempstr.split(":");
                if(st.length == 3){
                    if(st[0].trim().equals("CHROMOSOME")){}
                    else{return null;}
                    String tempchromosome = st[1].trim();
                    String[] sts = st[2].trim().split("-");
                    if(sts.length == 2)
                    {
                        String tempstart = sts[0].trim();
                        String tempstop = sts[1].trim();
                        try{
                            for(int op = 0; op < chromosome.size(); op++){
                                if(tempchromosome.equals(chromosome.get(op))){
                                    if(Integer.parseInt(startpos.get(op)) >= Integer.parseInt(tempstart) && Integer.parseInt(stoppos.get(op)) <= Integer.parseInt(tempstop)){
                                        int index = geneName.indexOf(locgene.get(op));
                                        genes.add(new Gene(locgene.get(op), index));
                                    }
                                }
                            }
                        }
                        catch(Exception e)
                        {
                            return null;
                        }
                    }
                    else{return null;}
                    continue;
                } //upstream:k:GENE
                else{
                    return null;
                }
            }
            ArrayList<Gene> tmpgenes = new ArrayList<Gene>();
	    int flag = 0;
            for (int i = 0; i < genes.size(); i ++)
            {
                Gene tmpGene = genes.get(i);
                for (int j = 0; j < mge.size(); j ++)
                {
                    if (mge.get(j).GeneId == tmpGene.Id)
                    {
                        flag = 1;
                        break;
                    }
                }
                if(flag == 0){
                     tmpgenes.add(tmpGene);
                }
                flag = 0;
            }
            return tmpgenes;
        }
        //This method is to validate weather the input symbols are known
	public ArrayList<Phenotype> ValidatePhenotypes(String strReqPhenotypes) throws IOException
	{

		strReqPhenotypes = strReqPhenotypes.toUpperCase();
                strReqPhenotypes = strReqPhenotypes.trim();
		ArrayList<Phenotype> phenotypes = new ArrayList<Phenotype>();
		// 1. Split string into string array, in which phenotype names are stored.
                //prepare the input of SearchByPhenotypes
                if(strReqPhenotypes.isEmpty()){
                    return phenotypes;
                }
                String[] strPhenotypes = strReqPhenotypes.split("\r\n");
		for(int i = 0 ; i < strPhenotypes.length; i++)
		{
                    String tempstr = strPhenotypes[i];
                    tempstr = tempstr.trim();
                   
                    for(int m=0;m<phenotype_file.size();m++){
                        if(tempstr.equals(phenotype_file.get(m).Name) && (HasRepeatPhenotype(phenotypes, phenotype_file.get(m).Id) == false)){
                            phenotypes.add(new Phenotype(phenotype_file.get(m).Name,phenotype_file.get(m).Id,phenotype_file.get(m).OMIM_ID));
                        }else if(tempstr.equals(phenotype_file.get(m).OMIM_ID) && (HasRepeatPhenotype(phenotypes, phenotype_file.get(m).Id) == false)){
                            phenotypes.add(new Phenotype(phenotype_file.get(m).Name,phenotype_file.get(m).Id,phenotype_file.get(m).OMIM_ID));
                        }
                    }
                   
                }
                return phenotypes;
        }
        //add by zhanghy at 2012/11/22
         public ArrayList<MatchedPhenotypeEntry> displayedQueryPhen(String strReqPhentypes, ArrayList<MatchedPhenotypeEntry> input) throws IOException{
		// 1. Split string into string array, in which gene names are stored.
                //prepare the input of SearchByGenes
                ArrayList<MatchedPhenotypeEntry> query = new ArrayList<MatchedPhenotypeEntry>();
    		ArrayList<Phenotype> queryPhenotype = ValidatePhenotypes(strReqPhentypes);
                for(int i = 0; i < queryPhenotype.size(); i++){
                    for(int j = 0; j < input.size(); j++){
                        if(input.get(j).Name.equalsIgnoreCase(queryPhenotype.get(i).Name)){
                            MatchedPhenotypeEntry temp = new MatchedPhenotypeEntry(queryPhenotype.get(i).Name,queryPhenotype.get(i).Id+1,queryPhenotype.get(i).OMIM_ID,0, id);
                            query.add(temp);
                        }
                    }
                }      
            return query;
        }
        
        public ArrayList<MatchedPhenotypeEntry> displayedNonQueryPhen(ArrayList<MatchedPhenotypeEntry> query, ArrayList<MatchedPhenotypeEntry> input){
            ArrayList<MatchedPhenotypeEntry> result = new ArrayList<MatchedPhenotypeEntry>();
            
            for(int i = 0; i < input.size(); i ++){
                for(int j = 0; j < query.size(); j++){
                    if(input.get(i).Name.equalsIgnoreCase(query.get(j).Name)){
                        break;
                    }
                    if(j == (query.size() - 1) && input.get(i).Name.equalsIgnoreCase(query.get(j).Name) == false){
                        result.add(input.get(i));
                    }
                }
            }
            return result;
        }
        
        //Check to see if there are duplicates in input
        private boolean HasRepeatPhenotype(ArrayList<Phenotype> phenotypes, int index)
	{
		if (phenotypes.isEmpty())
		{
			return false;
		}

		for (int j = 0 ; j < phenotypes.size() ; j ++ )
		{
			if ( phenotypes.get(j).Id == index)
			{
				return true;
			}
		}
		return false;
	}
    
    
        public void SetIsMorbid(ArrayList<MatchedGeneEntry> input, int num){
            for(int i=0;i<num;i++){
                input.get(i).setIsMorbid();
            }
        }
        
        
    public ArrayList<MatchedGeneEntry> SearchByPhenotype(
          	ArrayList<Phenotype> phenotypes, int AlgID) throws IOException {
                
                // 0. Sorting the input phenotypes.
		ContentComparatorPhenotypeAscend comp1 = new ContentComparatorPhenotypeAscend();
		Collections.sort(phenotypes, comp1);

		// 1. Preparing the result array.
                ArrayList<MatchedGeneEntry> matched_gene = ContextManager_new.iniGeneEntry(id);
		//ArrayList<MatchedGeneEntry> results = new ArrayList<MatchedGeneEntry>();

		/* 2. gene names has been loaded when initializing.
                ArrayList<String> geneName = ContextManager_new.getGeneName(id);

                */
                // 3. phenotypeScores has been loaded
		float[] phenotypeScoreList = GetPhenotypeProfile(phenotypes);
                
		// if Laplacian or Correlation Coefficients
		if (AlgID == 1 || AlgID == 2) {

			// 4.1 load W matrix
                        /*Already loaded up top
			float[] wMatrix = ContextManager_new.getwMatrix(id);
			
			int[][] wIndex = ContextManager_new.getwIndex(id);
                        
                        float[][] GeneMatrix = ContextManager_new.getGeneMatrix(id);
                        * 
                        */

			// 5 load geneScore
			

                        float [] pScore;
                        
                        
                        pScore = getPhenotypeScoreList(phenotypeScoreList, GENE_IN_ASSOCIATION_CNT, wMatrix, wIndex);
			// 6. put the gene scores into a two dimension array and call
			// the Laplacian Score or CC

                        for (int i = 0; i < GENE_CNT; i++) {
                 
				if (AlgID == 1) {
                                    matched_gene.get(i).setScore(getLaplacianScore(phenotypeScoreList, geneMatrix[i], wMatrix, wIndex));
				} else if (AlgID == 2) {
                                    matched_gene.get(i).setScore(getCorrCoeff(pScore, geneMatrix[i]));
				}
			}
                        
			if (AlgID == 1) {
				ContentComparatorAscend comp = new ContentComparatorAscend();
				Collections.sort(matched_gene, comp);
			} else if (AlgID == 2) {
				ContentComparatorDescend comp = new ContentComparatorDescend();
				Collections.sort(matched_gene, comp);
			}
                        
			return matched_gene;
                // if Ridge Regression 
		} else if (AlgID == 3) {
                    
			for (int i = 0; i < GENE_CNT; i++) {
                            matched_gene.get(i).setScore(getRidgeRegression(RidgeRegressionMatrix[i],phenotypeScoreList));
                            //results.add(new MatchedGeneEntry(geneName.get(i), i, getRidgeRegression(RidgeRegressionMatrix[i],phenotypeScoreList)));
			}
			ContentComparatorDescend comp = new ContentComparatorDescend();
			Collections.sort(matched_gene, comp);
                        
			return matched_gene;
		}
		return null;
	}
    
    public float[] GetPhenotypeProfile(ArrayList<Phenotype> phenotypes)
	{
		// 3.2 put the phenotype score into a one dimension array
		int QueryPhenotypeSize = phenotypes.size();
		float[] phenotypeScoreList = new float[PHENOTYPE_IN_ASSOCIATION_CNT];
		float temp_score;
		 
		for(int i = 0 ; i < PHENOTYPE_IN_ASSOCIATION_CNT ; i ++ ){
		    temp_score = 0;
		    for(int j = 0 ; j < QueryPhenotypeSize ; j ++ ){
		       temp_score += PhenotypeMatrix[phenotypes.get(j).Id][i];
		    }
		    phenotypeScoreList[i]=temp_score/QueryPhenotypeSize;   
		}
                
		return phenotypeScoreList;
	}
        
        public float getLaplacianScore(float [] phenotypeScoreList,float [] geneScoreList, float []wMatrix, int [][] wIndex){
		float LaplacianScore = 0;
		float meanPhenotype = getMean(phenotypeScoreList);
		float meanGene = getMean(geneScoreList);
		float sdPhenotype = getStandardDeviation (phenotypeScoreList);
		float sdGene = getStandardDeviation (geneScoreList);
		int n = phenotypeScoreList.length;
		int m = geneScoreList.length;

		float [] phenotypeScoreList_z = new float [n];
		float [] geneScoreList_z = new float [m];

                
		for(int i=0; i<n; i++){
			phenotypeScoreList_z[i] = (phenotypeScoreList[i] - meanPhenotype)/sdPhenotype;
		}

		for(int i=0;i<m;i++){
			geneScoreList_z[i] = (geneScoreList[i] - meanGene) / sdGene;
		}

                for ( int i = 0 ; i < wMatrix.length ; i ++ ){
                    // in wIndex matrix, 0 : phenotype, 1: gene.
                    int phen_idx = wIndex[i][0] - 1;
                    int gene_idx = wIndex[i][1] - 1;
                    LaplacianScore += wMatrix[i] * (geneScoreList_z[gene_idx] - phenotypeScoreList_z[phen_idx])*(geneScoreList_z[gene_idx] - phenotypeScoreList_z[phen_idx]);
                }
                if (Float.isNaN(LaplacianScore))
                {
                    LaplacianScore = 100000;
                }
                return LaplacianScore;
                
                
/* NO DELETION
                double sum_p=0;
		for ( int i=0; i<n; i++ ) {
		       double v = phenotypeScoreList_z[i];
		       sum_p += v * v;
		}

		double sum_g=0;
		for ( int i=0; i<m; i++ ) {
		       double v = geneScoreList_z[i];
		       sum_g += v * v;
		}

		double [] tempMatrix=new double [n];
		for(int i=0; i<wMatrix.length; i++){
			tempMatrix[wIndex[i][0]-1] += geneScoreList_z[wIndex[i][1]-1] * wMatrix[i];
		}
		for (int i=0;i<n;i++){
			LaplacianScore -= tempMatrix[i]*phenotypeScoreList_z[i];
		}
		LaplacianScore *= 2;
		LaplacianScore = LaplacianScore + sum_p + sum_g;
		return LaplacianScore;
  */
                
	}

            
    public float getStandardDeviation ( float[] data ){
        int n = data.length;
        float mean = getMean(data);
        float sum = 0;
        for ( int i=0; i<n; i++ ) {
            float v = data[i] - mean;
            sum += v * v;
        }
        return (float)Math.sqrt( sum / n );
    }

    public float getMean ( float[] data){
        float mean = 0;
        int n = data.length;
        for ( int i=0; i<n; i++ ){
            mean += data[i];
        }
        return mean/n;
    }
        
    
    
    public float [] getPhenotypeScoreList(float [] phenotypeScoreList, int length, float []wMatrix, int [][] wIndex){
	float [] pScore = new float [length];
	for(int i=0; i<wMatrix.length; i++){
            pScore[wIndex[i][1]-1] += phenotypeScoreList[wIndex[i][0]-1] * wMatrix[i];
        }  
        return pScore;
    }
    
        
        public float getCorrCoeff(float [] phenotypeScoreList,float [] geneScoreList){

                float CC = 0;
                float meanGene = getMean(geneScoreList);
		float meanPhenotype = getMean(phenotypeScoreList);
                int m = geneScoreList.length;
                
		if(meanPhenotype==0 || meanGene==0){
                	return CC;
		}else{
                    for(int i = 0 ; i < m ; i ++){
                        CC += (geneScoreList[i] - meanGene)*(phenotypeScoreList[i] - meanPhenotype);
                    }
                    CC /= (getStandardDeviation(geneScoreList)*getStandardDeviation(phenotypeScoreList)*m);
                    return CC;
		}
	}
             
        // ** not changed just copied from PhenotypeFinder **
        public float getRidgeRegression ( float[] inverseScoreList,float [] geneScoreList){
		  float RR = 0;
		  int n = inverseScoreList.length;
		  for (int i=0; i<n; i++){
			  RR += inverseScoreList[i]*geneScoreList[i];
		  }
		  return RR; 
	}
        
        //Get matched phenotype
        public ArrayList<MatchedGeneEntry> getDisplayGene(ArrayList<MatchedGeneEntry> input, int queryNum){
		ArrayList<MatchedGeneEntry> result = new ArrayList<MatchedGeneEntry>();
                int j = 0;
                //array of the IDs of the nodes 
                ArrayList<Integer> ids = new ArrayList<Integer>();
                
		for(int i=0;i<queryNum;i++){
                        if(i == input.size()){
                            break;
                        }
			result.add(input.get(i));
                        ids.add(input.get(i).GeneId);
                        j = i;
		}
                
                int addNum = j;
                int maxAddNum = j+20;
                while(addNum<maxAddNum){
                    
                    if(j==input.size()){
                        break;
                    }
                    
                    if((input.get(j).GeneId!=-1)&&(ids.contains(input.get(j).GeneId)==false) && searchGPNetwork2(input.get(j).GeneId)==true){
                        result.add(input.get(j));
                        ids.add(input.get(j).GeneId);
                        addNum++;
                        j++;
                    }else{
                        j++;
                    }
                }
                
		return result;
	}

        
        public boolean searchGPNetwork2(int geneID){
            boolean result = false;
            for(int i=0;i<networkEntry.size();i++){
                if(networkEntry.get(i).neighbourList.contains(geneID) == true){
                    result = true;
                }
            }
            return result;
        }
        
        
        public int searchGPNetwork(int geneID){            
            for(int i=0;i<networkEntry.size();i++){
                if(networkEntry.get(i).neighbourList.contains(geneID) == true){
                    return i;
                }
            }
            return -1;            
        }
        
        public ArrayList<MatchedPhenotypeEntry> getDisplayPhenotype(String strReqPhenotypes, int queryNum)  throws IOException{

            ArrayList<MatchedPhenotypeEntry> result = new ArrayList<MatchedPhenotypeEntry>();
		ArrayList<Phenotype> queryPhenotype = new ArrayList<Phenotype>();
 
		queryPhenotype = ValidatePhenotypes(strReqPhenotypes);
                
                // To check if 20 more top phenotypes are added besides query phenotypes
                ArrayList<Integer> addedIDs = new ArrayList<Integer>();
                int addNum = queryNum;
		// add query genes into the result Arraylist
		for(int i=0;i<queryPhenotype.size();i++){
			MatchedPhenotypeEntry temp = new MatchedPhenotypeEntry(queryPhenotype.get(i).Name,queryPhenotype.get(i).Id+1,queryPhenotype.get(i).OMIM_ID,0, id);
			result.add(temp);
                        addedIDs.add(queryPhenotype.get(i).Id+1);
		}

                
                float[] phenotypeScoreList = GetPhenotypeProfile(queryPhenotype);
                
                 // find the minimum number in the list
                 float min = phenotypeScoreList[0];
                 
                 for(int i=0;i<phenotypeScoreList.length;i++){
                     if(phenotypeScoreList[i]<min){
                        min = phenotypeScoreList[i];
                     }
                 }
                 
		while(addNum>0){
                        float maxScore = phenotypeScoreList[0];
			 int maxIndex=0;
			 for(int j=0;j<phenotypeScoreList.length;j++){
				 if(phenotypeScoreList[j]>maxScore){
					 maxScore = phenotypeScoreList[j];
					 maxIndex = j;
				 }
			 }
                         if(addedIDs.contains(maxIndex+1) == false){
                            MatchedPhenotypeEntry temp = new MatchedPhenotypeEntry(phenotype_file.get(maxIndex).Name,maxIndex,phenotype_file.get(maxIndex).OMIM_ID,maxScore, id);
                            result.add(temp);
                            addNum--;
                         }
                         phenotypeScoreList[maxIndex] = min;
		 }

		return result;
        }

        public ArrayList<Edge> getGeneEdges(ArrayList<MatchedGeneEntry> input) throws ArrayIndexOutOfBoundsException{
		ArrayList<Edge> result = new ArrayList<Edge>();
                
                 for(int i=0;i<input.size();i++){
                    
                     ArrayList<Integer> temp = geneEntry.get((input.get(i).GeneId)).getNeighborList();
                     for(int j=0;j<input.size();j++){
                            if(temp.contains(input.get(j).GeneId + 1)==true){
                               Edge tempEdge = new Edge(2,input.get(j).GeneName,input.get(i).GeneName);
                               result.add(tempEdge);
                           }

                        }
                    }
		return result;
	}

        public ArrayList<MatchedPhenotypeEntry> displayedQueryPhenotype(String strReqPhenotypes, ArrayList<MatchedPhenotypeEntry> input) throws IOException{
		
            ArrayList<MatchedPhenotypeEntry> query = new ArrayList<MatchedPhenotypeEntry>();
            
            ArrayList<Phenotype> queryPhenotype = ValidatePhenotypes(strReqPhenotypes);
                
                for(int i = 0; i < queryPhenotype.size(); i++){
                    for(int j = 0; j < input.size(); j++){
                        if(input.get(j).Name.equalsIgnoreCase(queryPhenotype.get(i).Name)){
                            MatchedPhenotypeEntry temp = new MatchedPhenotypeEntry(queryPhenotype.get(i).Name,queryPhenotype.get(i).Id+1,queryPhenotype.get(i).OMIM_ID,0, id);
                            query.add(temp);
                        }
                    }
                }
                
            return query;
        }
        
        public ArrayList<MatchedPhenotypeEntry> displayedNonQueryPhenotype(ArrayList<MatchedPhenotypeEntry> query, ArrayList<MatchedPhenotypeEntry> input){
            ArrayList<MatchedPhenotypeEntry> result = new ArrayList<MatchedPhenotypeEntry>();
            
            for(int i = 0; i < input.size(); i ++){
                for(int j = 0; j < query.size(); j++){
                    if(input.get(i).Name.equalsIgnoreCase(query.get(j).Name)){
                        break;
                    }
                    if(j == (query.size() - 1) && input.get(i).Name.equalsIgnoreCase(query.get(j).Name) == false){
                        result.add(input.get(i));
                    }
                }
            }
            return result;
        }
        //add by zhanghy at 20121129
        public ArrayList<MatchedGeneEntry> mdeleteAloneGenes( ArrayList<MatchedPhenotypeEntry> mpe, ArrayList<MatchedGeneEntry> input, ArrayList<Edge> geneEdge, ArrayList<Edge> networkEdge){
           ArrayList<MatchedGeneEntry> result = new ArrayList<MatchedGeneEntry>();
            int i,j,k;
            int flag = 0;
            ArrayList<MatchedGeneEntry> tempGene = new ArrayList<MatchedGeneEntry>();
            for(j=0; j<input.size(); j++){
                for(i=0; i<mpe.size(); i++){
                    for(k=0; k<networkEdge.size(); k++){
                        if(mpe.get(i).OMIM_ID.equals(networkEdge.get(k).target)&&input.get(j).GeneName.equals(networkEdge.get(k).source) ){
                            tempGene.add(input.get(j));
                            flag = 1;
                            break;
                        }
                    }
                    if(flag == 1)
                    {
                        flag = 0;
                        break;
                    }
                }
            }
            int dimGene = input.size();
            MyMatrix geneMatrix0 = new MyMatrix(dimGene, dimGene);
            MyMatrix geneMatrix1 = new MyMatrix(dimGene, dimGene);
            MyMatrix geneMatrix_sum = new MyMatrix(dimGene, dimGene);
            for(i=0;i<dimGene;i++){
                for(j=0; j<dimGene; j++){
                    for(k=0;k<geneEdge.size();k++){
                        if((input.get(i).GeneName.equals(geneEdge.get(k).source)
                            && input.get(j).GeneName.equals(geneEdge.get(k).target))){
                            geneMatrix0.m_matrix[i][j] = 1;
                            geneMatrix0.m_matrix[j][i] = 1;
                            geneMatrix1.m_matrix[i][j] = 1;
                            geneMatrix1.m_matrix[j][i] = 1;
                            geneMatrix_sum.m_matrix[i][j] = 1;
                            geneMatrix_sum.m_matrix[j][i] = 1;
                            break;
                        }
                    }
                }
            }
            int count = 0;
            while(count<3){
                geneMatrix0.mulMatrix(geneMatrix1);
                geneMatrix_sum.addMatrix(geneMatrix0);
                count ++;
            }
            int[] m_phen = new int[dimGene];
            for(i=0; i<dimGene; i++){
                for(j=0; j<tempGene.size(); j++){
                    m_phen[i] += geneMatrix_sum.m_matrix[j][i];
                }
            }
            for(i=0; i<tempGene.size(); i++){
                result.add(input.get(i));
            }
            for(i=tempGene.size(); i<dimGene; i++){
                if(m_phen[i] > 0)
                {
                    result.add(input.get(i));
                }
            }
            return result;
        }
         public ArrayList<MatchedPhenotypeEntry> mdeleteAlonePhenotype(ArrayList<Phenotype> phenotype, ArrayList<MatchedPhenotypeEntry> input, ArrayList<Edge> phenotypeEdge, ArrayList<Edge> networkEdge){
            ArrayList<MatchedPhenotypeEntry> result = new ArrayList<MatchedPhenotypeEntry>();
            int dimPhen = input.size();
            int querysize = phenotype.size();
            MyMatrix phenMatrix = new MyMatrix(dimPhen, dimPhen);
            MyMatrix phenMatrix1 = new MyMatrix(dimPhen, dimPhen);
            //MyMatrix tempgeneMatrix = new MyMatrix(dimGene, dimGene);
            MyMatrix phenMatrix_sum = new MyMatrix(dimPhen, dimPhen);  
            for(int i=0;i<dimPhen;i++){
                for(int j=0; j<dimPhen; j++){
                    for(int k=0;k<phenotypeEdge.size();k++){
                        if((input.get(i).OMIM_ID.equals(phenotypeEdge.get(k).source)
                            && input.get(j).OMIM_ID.equals(phenotypeEdge.get(k).target))){
                            phenMatrix.m_matrix[i][j] = 1;
                            phenMatrix.m_matrix[j][i] = 1;
                            phenMatrix1.m_matrix[i][j] = 1;
                            phenMatrix1.m_matrix[j][i] = 1;
                            phenMatrix_sum.m_matrix[i][j] = 1;
                            phenMatrix_sum.m_matrix[j][i] = 1;
                            break;
                        }
                    }
                }
            }
            int count = 0;
            while(count<3){
                phenMatrix.mulMatrix(phenMatrix1);
                phenMatrix_sum.addMatrix(phenMatrix);
                count ++;
            }
            int[] m_phen = new int[dimPhen];
            for(int i=0; i<dimPhen; i++){
                for(int j=0; j<querysize; j++){
                    m_phen[i] += phenMatrix_sum.m_matrix[j][i];
                }
            }
            for(int i=0; i< querysize; i++){
                result.add(input.get(i));
            }
            for(int i=querysize; i<dimPhen; i++){
                if(m_phen[i] > 0)
                {
                    result.add(input.get(i));
                }
            } 
            return result;
        }
        
         public ArrayList<Edge> updateGG(ArrayList<Edge> GG, ArrayList<MatchedGeneEntry> matchedgenes){
            int i,j,k;
            ArrayList<Edge> resultGG = new ArrayList<Edge>();
            int flag = 0;
            for(i=0; i<GG.size(); i++){
                for(j=0; j<matchedgenes.size(); j++){
                    if(GG.get(i).source.equals(matchedgenes.get(j).GeneName)){
                        flag = 1;
                        for(k=0; k<matchedgenes.size(); k++){
                            if(GG.get(i).target.equals(matchedgenes.get(k).GeneName)){
                                resultGG.add(GG.get(i));
                                break;
                            }
                        }
                        if(flag == 1){
                            flag = 0;
                            break;
                        }
                    }
                }
            }
            return resultGG;
        }
        public ArrayList<Edge> updateGP(ArrayList<Edge> GP, ArrayList<MatchedGeneEntry> matchedgenes, ArrayList<MatchedPhenotypeEntry> mpe){
            ArrayList<Edge> resultGP = new ArrayList<Edge>();
            int flag = 0;
            for(int i=0; i<GP.size(); i++){
                for(int j=0; j<matchedgenes.size(); j++){
                    if(GP.get(i).source.equals(matchedgenes.get(j).GeneName)){
                        flag = 1;
                        for(int k=0; k<mpe.size(); k++){
                            if(GP.get(i).target.equals(mpe.get(k).OMIM_ID)){
                                resultGP.add(GP.get(i));
                                break;
                            }
                        }
                        if(flag == 1){
                            flag = 0;
                            break;
                        }
                    }
                }
            }
            GP = resultGP;
            return GP;
        }
        public ArrayList<Edge> updatePP(ArrayList<Edge> PP, ArrayList<MatchedPhenotypeEntry> mpe){
            ArrayList<Edge> resultPP = new ArrayList<Edge>();
            int flag = 0;
            for(int i=0; i<PP.size(); i++){
                for(int j=0; j<mpe.size(); j++){
                    if(PP.get(i).source.equals(mpe.get(j).OMIM_ID)){
                        flag = 1;
                        for(int k=0; k<mpe.size(); k++){
                            if(PP.get(i).target.equals(mpe.get(k).OMIM_ID)){
                                resultPP.add(PP.get(i));
                                break;
                            }
                        }
                        if(flag == 1){
                            flag = 0;
                            break;
                        }
                    }
                }
            }
            PP = resultPP;
            return PP;
        }
        
           //add by hy at 20121128
        public GANDP deleteUnReachableGenesAndPhenotypes(ArrayList<MatchedPhenotypeEntry> query, ArrayList<MatchedPhenotypeEntry> nonquery, ArrayList<MatchedGeneEntry> mge, ArrayList<Edge> geneEdge, ArrayList<Edge> phenotypeEdge, ArrayList<Edge> networkEdge)
        {
            //store the final gene and phenotype set
            ArrayList<MatchedGeneEntry> result_gene = new ArrayList<MatchedGeneEntry>();
            ArrayList<MatchedPhenotypeEntry> result_phen = new ArrayList<MatchedPhenotypeEntry>();

            //
            ArrayList<MatchedGeneEntry> g_temp = new ArrayList<MatchedGeneEntry>();
            ArrayList<MatchedPhenotypeEntry> p_temp = new ArrayList<MatchedPhenotypeEntry>();

            //store the query used for each loop
            ArrayList<MatchedGeneEntry> m_gene = new ArrayList<MatchedGeneEntry>();
            ArrayList<MatchedPhenotypeEntry> m_phenotype = new ArrayList<MatchedPhenotypeEntry>();

            //store the rest genes and phenotypes
            ArrayList<MatchedGeneEntry> restGene = mge;
            ArrayList<MatchedPhenotypeEntry> restPhenotype = nonquery; 
            
            int i,j,k;
            int mflag = 0;
            for(i=0; i<query.size(); i++){
                for(j=0; j<phenotypeEdge.size(); j++)
                {
                    if((query.get(i).OMIM_ID.equals(phenotypeEdge.get(j).source))||(query.get(i).OMIM_ID.equals(phenotypeEdge.get(j).target))){
                        result_phen.add(query.get(i));
                        p_temp.add(query.get(i));
                        m_phenotype.add(query.get(i));
                        mflag = 1;
                        break;
                    }
                }
                if(mflag == 0){
                    for(k=0; k<networkEdge.size(); k++)
                    {
                        if(query.get(i).OMIM_ID.equals(networkEdge.get(k).target))
                        {
                            result_phen.add(query.get(i));
                            p_temp.add(query.get(i));
                            m_phenotype.add(query.get(i));
                            break;
                        }
                    }
                }
                mflag = 0;   
            }
            
            /*for(i=0; i<query.size(); i++){
                result_phen.add(query.get(i));
                p_temp.add(query.get(i));
                m_phenotype.add(query.get(i));
            }*/
            //int a =0;
            while(g_temp.size()!=0 || p_temp.size()!=0)
            {
                    m_gene = g_temp;
                    m_phenotype = p_temp;
                    g_temp = new ArrayList<MatchedGeneEntry>();
                    p_temp = new ArrayList<MatchedPhenotypeEntry>();;
                    boolean found = false;
                    for(i=0; i<m_phenotype.size(); i++)
                    {
                            for(j=0; j<restPhenotype.size(); j++)
                            {
                                for(k=0; k<phenotypeEdge.size(); k++)
                                {
                                    if((m_phenotype.get(i).OMIM_ID.equals(phenotypeEdge.get(k).source)
                                    && restPhenotype.get(j).OMIM_ID.equals(phenotypeEdge.get(k).target))
                                    ||(m_phenotype.get(i).OMIM_ID.equals(phenotypeEdge.get(k).target)
                                    && restPhenotype.get(j).OMIM_ID.equals(phenotypeEdge.get(k).source))){
                                            found = true;
                                            break;
                                    }
                                }
                                if(found == true){
                                    for(k=0; k<result_phen.size(); k++){
                                        if(result_phen.get(k).OMIM_ID.equals(restPhenotype.get(j).OMIM_ID)){
                                            found = false;
                                            break;
                                        }
                                    }
                                    if(found == true){
                                        p_temp.add(restPhenotype.get(j));
                                        result_phen.add(restPhenotype.get(j));
                                        found = false;
                                    }

                                }
                            }
                            found = false;
                            for(j=0; j<restGene.size(); j++)
                            {
                                for(k=0; k<networkEdge.size(); k++)
                                {
                                    if(m_phenotype.get(i).OMIM_ID.equals(networkEdge.get(k).target)
                                    && restGene.get(j).GeneName.equals(networkEdge.get(k).source))
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if(found == true){
                                    for(k=0; k<result_gene.size(); k++){
                                        if(result_gene.get(k).GeneName.equals(restGene.get(j).GeneName)){
                                            found = false;
                                            break;
                                        }
                                    }
                                    if(found == true){
                                        g_temp.add(restGene.get(j));
                                        result_gene.add(restGene.get(j));
                                        found = false;
                                    }
                                }
                            }
                    }


                    for(i=0; i<m_gene.size(); i++)
                    {
                            for(j=0; j<restGene.size(); j++)
                            {
                                    for(k=0; k<geneEdge.size(); k++)
                                    {
                                            if((m_gene.get(i).GeneName.equals(geneEdge.get(k).source)
                                            && restGene.get(j).GeneName.equals(geneEdge.get(k).target))
                                            ||(m_gene.get(i).GeneName.equals(geneEdge.get(k).target)
                                            && restGene.get(j).GeneName.equals(geneEdge.get(k).source))){
                                                    found = true;
                                            }
                                    }
                                    if(found == true){
                                            for(k=0; k<result_gene.size(); k++){
                                                if(result_gene.get(k).GeneName.equals(restGene.get(j).GeneName)){
                                                    found = false;
                                                    break;
                                                }
                                            }
                                            if(found == true){
                                                g_temp.add(restGene.get(j));
                                                result_gene.add(restGene.get(j));
                                                found = false;
                                            }  
                                    }
                            }
                            found = false;
                            for(j=0; j<restPhenotype.size(); j++)
                            {
                                    for(k=0; k<networkEdge.size(); k++)
                                    {
                                            if(m_gene.get(i).GeneName.equals(networkEdge.get(k).source)
                                            && restPhenotype.get(j).OMIM_ID.equals(networkEdge.get(k).target)){
                                                    found = true;
                                            }
                                    }
                                    if(found == true){
                                            for(k=0; k<result_phen.size(); k++){
                                                if(result_phen.get(k).OMIM_ID.equals(restPhenotype.get(j).OMIM_ID)){
                                                    found = false;
                                                    break;
                                                }
                                            }
                                            if(found == true){
                                                 p_temp.add(restPhenotype.get(j));
                                                result_phen.add(restPhenotype.get(j));
                                                found = false;
                                            }
                                           
                                    }
                            }
                    }
                    ArrayList<Edge> temp = new ArrayList<Edge>();
                    found = false;
                    for(i=0; i<geneEdge.size(); i++){
                        for(j=0; j<m_gene.size(); j++){
                            if(geneEdge.get(i).source.equals(m_gene.get(j).GeneName)||geneEdge.get(i).target.equals(m_gene.get(j).GeneName)){
                                found = true;
                                break;
                            }
                        }
                        if(found == false){
                            temp.add(geneEdge.get(i));
                        }
                        else{
                            found = false;
                        }
                    }
                    geneEdge = temp;
                    temp = new ArrayList<Edge>();
                    for(i=0; i<phenotypeEdge.size(); i++){
                        for(j=0; j<m_phenotype.size(); j++){
                            if(phenotypeEdge.get(i).source.equals(m_phenotype.get(j).OMIM_ID)||phenotypeEdge.get(i).target.equals(m_phenotype.get(j).OMIM_ID)){
                                found = true;
                                break;
                            }
                        }
                        if(found == false){
                            temp.add(phenotypeEdge.get(i));
                        }
                        else{
                            found = false;
                        }
                    }
                    phenotypeEdge = temp;
                    temp = new ArrayList<Edge>();
                    for(i=0; i<networkEdge.size(); i++){
                        for(j=0; j<m_phenotype.size(); j++){
                            if(networkEdge.get(i).target.equals(m_phenotype.get(j).OMIM_ID)){
                               found = true;
                               break;
                            }
                        }
                        for(j=0; j<m_gene.size(); j++){
                            if(networkEdge.get(i).source.equals(m_gene.get(j).GeneName)){
                                found = true;
                                break;
                            } 
                        }
                        if(found == false){
                            temp.add(networkEdge.get(i));
                        }
                        else{
                            found = false;
                        }
                    }
                    networkEdge = temp;
                    temp = new ArrayList<Edge>();
            }//end while
            GANDP gp = new GANDP();
            gp.mge = result_gene;
            gp.mpe = result_phen;
           // return result_gene;
            return gp;
        }
        //end
        public ArrayList<MatchedGeneEntry> deleteAloneGenes(ArrayList<MatchedGeneEntry> input, ArrayList<Edge> geneEdge, ArrayList<Edge> networkEdge){
            ArrayList<MatchedGeneEntry> result = new ArrayList<MatchedGeneEntry>();
            
            for(int i=0;i<input.size();i++){
                
                boolean found = false;
                
                for(int j=0;j<geneEdge.size();j++){
                    if(input.get(i).GeneName.equals(geneEdge.get(j).source)
                         || input.get(i).GeneName.equals(geneEdge.get(j).target)){
                        found = true;
                    }
                }
                
                for(int k=0;k<networkEdge.size();k++){
                    if(input.get(i).GeneName.equals(networkEdge.get(k).source) 
                            || input.get(i).GeneName.equals(networkEdge.get(k).target)){
                        found = true;
                    }
                }
                
                if(found == true){
                    result.add(input.get(i));
                }
            }
            
            return result;
        }
        
         public ArrayList<MatchedPhenotypeEntry> deleteAlonePhenotype(ArrayList<MatchedPhenotypeEntry> input, ArrayList<Edge> phenotypeEdge, ArrayList<Edge> networkEdge){
             ArrayList<MatchedPhenotypeEntry> result = new ArrayList<MatchedPhenotypeEntry>();
             
            for(int i=0;i<input.size();i++){
                
                boolean found = false;
                
                for(int j=0;j<phenotypeEdge.size();j++){
                    if(input.get(i).OMIM_ID.equals(phenotypeEdge.get(j).source)
                         || input.get(i).OMIM_ID.equals(phenotypeEdge.get(j).target)){
                        found = true;
                    }
                }
                
                for(int k=0;k<networkEdge.size();k++){
                    if(input.get(i).OMIM_ID.equals(networkEdge.get(k).source) 
                            || input.get(i).OMIM_ID.equals(networkEdge.get(k).target)){
                        found = true;
                    }
                }
                
                if(found == true){
                    result.add(input.get(i));
                }
            }
            return result;
        }
        
        public ArrayList<Edge> getNetworkEdges(ArrayList<MatchedGeneEntry> input_g, ArrayList<MatchedPhenotypeEntry> input_p) throws ArrayIndexOutOfBoundsException{
		ArrayList<Edge> result = new ArrayList<Edge>();               
                for(int i=0;i<input_p.size();i++){   
                   if(input_p.get(i).Id>0){
                        ArrayList<Integer> temp = networkEntry.get((input_p.get(i).Id)-1).getNeighborList();
                        for(int j=0;j<input_g.size();j++){
                            if(temp.contains(input_g.get(j).GeneId + 1)==true){
                                Edge tempEdge = new Edge(2,input_g.get(j).GeneName,input_p.get(i).OMIM_ID);
                                result.add(tempEdge);
                            }
                        }
                   }
                }
		return result;
	}
        
        public ArrayList<Edge> getAllQueryNetworkEdges(ArrayList<MatchedGeneEntry> input_g,ArrayList<MatchedPhenotypeEntry> input_p, ArrayList<Edge> ed){    
            int phenotypeID;
            for(int i=0;i<input_g.size();i++){
                phenotypeID = searchGPNetwork(input_g.get(i).GeneId+1);
                if(phenotypeID!=-1){
                    String omimID = phenotype_file.get(phenotypeID).OMIM_ID;
                    String pName = phenotype_file.get(phenotypeID).Name;
                    Edge tempEdge = new Edge(2, input_g.get(i).GeneName, omimID);
                    
                    MatchedPhenotypeEntry tempPhenotype = new MatchedPhenotypeEntry(pName, phenotypeID, omimID, 0, id);
                    ed.add(tempEdge);
                    if(nodeExist(input_p,tempPhenotype)==false){
                        input_p.add(tempPhenotype);
                    }
                }           
            }     
            return ed;
        }
        
        public boolean nodeExist(ArrayList<MatchedPhenotypeEntry> input_p, MatchedPhenotypeEntry temp){
            boolean result = false;
            for(int i=0;i<input_p.size();i++){
                if(input_p.get(i).OMIM_ID.equals(temp.OMIM_ID)){
                    result = true;
                    return result;
                }
            }
            return result;
        }
        
	public boolean isEntryExist(MatchedGeneEntry input_g, MatchedPhenotypeEntry input_p){
		boolean result = false;

		for(int i=0;i<networkEntry.size();i++){
			if((input_p.Id == networkEntry.get(i).EntryID) && ((networkEntry.get(i).findNeighbor(input_g.GeneId))==true)){
				result = true;
			}
		}

		return result;
	}

        public ArrayList<Edge> getPhenotypeEdges(ArrayList<MatchedPhenotypeEntry> input) throws ArrayIndexOutOfBoundsException{
		ArrayList<Edge> result = new ArrayList<Edge>();

                for(int i=0;i<input.size();i++){
                    ArrayList<Integer> temp = phenotypeEntry.get((input.get(i).Id)).getNeighborList();
                    
                   for(int j=0;j<input.size();j++){
                       if(temp.contains(input.get(j).Id+1)==true){
                           Edge tempEdge = new Edge(2,input.get(j).OMIM_ID,input.get(i).OMIM_ID);
                           result.add(tempEdge);
                       }
                       
                    }
                }
		return result;
	}
        
        
}