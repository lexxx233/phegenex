package com.ppi;

import java.util.Comparator;

public class ContentComparatorGeneAscend implements Comparator {

 public int compare(Object o1, Object o2) {

	 Gene c1 = (Gene) o1;
	 Gene c2 = (Gene) o2;
  if (c1.Id > c2.Id) {
   return 1;
  } else {
     if (c1.Id == c2.Id) {
         return 0;
     } else {
         return -1;
   }
  }
 }
}