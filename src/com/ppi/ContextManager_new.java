/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ppi;

import com.ppi.util.FileUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import org.apache.log4j.Logger;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.*;
import java.util.zip.*;
//Apache2.0 license for jsonsimple
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Context Manager overhaul
 *
 * @author Thanh Le @email thanhxle@gmail.com
 * 
 */
public class ContextManager_new {
    //0. Constants

    public static HashMap<String, Integer> GENE_CNT = new HashMap();
    public static HashMap<String, Integer> PHENOTYPE_CNT = new HashMap();
    public static HashMap<String, Integer> GENE_IN_ASSOCIATION_CNT = new HashMap();
    public static HashMap<String, Integer> PHENOTYPE_IN_ASSOCIATION_CNT = new HashMap();
    //root
    private static String DATA_ROOT = "C:\\rcnet_data\\";
    //101 102
    private static HashMap<String, String> MORBID_FILE = new HashMap(); //For human it's OMIM morbid map file
    private static HashMap<String, String> GENE_FILE = new HashMap();
    private static HashMap<String, String> GENE_LOCUS_FILE = new HashMap();
    private static HashMap<String, String> PHENOTYPE_FILE = new HashMap();
    //201 202 203 204
    private static HashMap<String, String> G_P_NETOWRK = new HashMap();
    private static HashMap<String, String> PHENOTYPE_SCORE_MATRIX = new HashMap();
    private static HashMap<String, String> GENE_MATRIX_FILE = new HashMap();
    private static HashMap<String, String> RIDGE_REGRESSION_MATRIX_PHENOTYPESEARCH = new HashMap();
    private static HashMap<String, String> RIDGE_REGRESSION_MATRIX_GENESEARCH = new HashMap();
    //301 302 303 
    private static HashMap<String, String> GENE_NETWORK = new HashMap();
    private static HashMap<String, String> PHENOTYPE_NETWORK = new HashMap();
    private static HashMap<String, String> ASSOCIATION_NETWORK = new HashMap();
    private static HashMap<String, String> ASSOCIATION_NETWORK_GENE = new HashMap();
    /*
     * -------------------------------------------------------------------------
     */
    // 2. Cache handlers for precomputed results.
    private static HashMap<String, ArrayList<Entry>> geneEntry = new HashMap();
    private static HashMap<String, ArrayList<Entry>> networkEntry = new HashMap();
    private static HashMap<String, ArrayList<Entry>> phenotypeEntry = new HashMap();
    //added by zhanghy
    private static HashMap<String, ArrayList<Entry>> networkEntry_gene = new HashMap();
//    private static ArrayList<Gene> lsGenes = new ArrayList<Gene>();
    private static HashMap<String, ArrayList<Gene>> lsAlias = new HashMap();
    private static HashMap<String, ArrayList<String>> geneName = new HashMap();
    private static HashMap<String, ArrayList<String>> chromosome = new HashMap();
    private static HashMap<String, ArrayList<String>> startpos = new HashMap();
    private static HashMap<String, ArrayList<String>> stoppos = new HashMap();
    private static HashMap<String, ArrayList<String>> locgene = new HashMap();
    private static HashMap<String, ArrayList<Integer>> inomim = new HashMap();
    private static HashMap<String, ArrayList<Integer>> ismorbid = new HashMap();
    private static HashMap<String, ArrayList<Integer>> ismorbidgene = new HashMap();
    private static HashMap<String, ArrayList<String>> morbidgene = new HashMap();
    private static HashMap<String, ArrayList<String>> morbidphenotype = new HashMap();
    private static HashMap<String, ArrayList<String>> aliasname = new HashMap();
    private static HashMap<String, ArrayList<String>> genevalue = new HashMap();
    //added by zhanghy at 20120905
    private static HashMap<String, ArrayList<String>> lociflag = new HashMap();
    //end
    private static HashMap<String, ArrayList<Phenotype>> phenotypes = new HashMap();
    private static HashMap<String, ArrayList<Phenotype>> lsPhenotypes = new HashMap();
    private static HashMap<String, ArrayList<String>> lsomim = new HashMap();
    
    private static HashMap<String, int[][]> g_p_network_matrix_index = new HashMap();
    private static HashMap<String, float[]> g_p_network_matrix_value = new HashMap();
    private static HashMap<String, float[][]> GeneScores = new HashMap();
    private static HashMap<String, float[][]> PhenotypeScores = new HashMap();
    private static HashMap<String, float[][]> RidgeRegressionScores_p = new HashMap();
    private static HashMap<String, float[][]> RidgeRegressionScores_g = new HashMap();
    private static HashMap<String, ArrayList<String>> inverseScore = new HashMap();
    private static HashMap<String, Integer> row_p2 = new HashMap();
    private static HashMap<String, Integer> col_p2 = new HashMap();
    private static Logger log = Logger.getLogger(ContextManager.class);

    public static void init() throws FileNotFoundException, IOException {
        //Parse JSON configuration file to get location for data of each species
        JSONParser parser = new JSONParser();
        try {
            JSONObject obj = (JSONObject) parser.parse(new FileReader("C:/Users/compbio/Desktop/PlanB_Project/phegenex74/src/com/ppi/species.json"));
            
            DATA_ROOT = (String) obj.get("root");

            //Loop Json Section Array
            JSONArray sections = (JSONArray) obj.get("section");
            for (int i = 0; i < sections.size(); i++) {
                JSONObject section = (JSONObject) sections.get(i);
                JSONObject inp = (JSONObject) section.get("input");

                //Meta info
                String spname = (String) section.get("species");
                String id = (String) section.get("id");
                Boolean forcedfb = Boolean.parseBoolean((String) section.get("forcedfallback"));
                String copyx = (String) section.get("copy");

                //Specific input file
                String species_root = DATA_ROOT + spname + "/";
                String loc = "default";
                if (forcedfb == true) {
                    loc = "fallback";
                }

                String geneAlias_loc = species_root + (String) ((JSONObject) inp.get(new StringBuilder().append(spname).append("_geneAlias").toString())).get(loc);
                String geneLocation_loc = species_root + (String) ((JSONObject) inp.get(new StringBuilder().append(spname).append("_geneLocation").toString())).get(loc);
                String phenotypeOMIM_loc = species_root + (String) ((JSONObject) inp.get(new StringBuilder().append(spname).append("_phenotypeOMIM").toString())).get(loc);
                String morbidMap_loc = species_root + (String) ((JSONObject) inp.get(new StringBuilder().append(spname).append("_morbidMap").toString())).get(loc);
                String genePhenAssociation_loc = species_root + (String) ((JSONObject) inp.get(new StringBuilder().append(spname).append("_genePhenAssociation").toString())).get(loc);
                String phenotypeScoreMatrix_loc = species_root + (String) ((JSONObject) inp.get(new StringBuilder().append(spname).append("_phenotypeScoreMatrix").toString())).get(loc);
                String geneScoreMatrix_loc = species_root + (String) ((JSONObject) inp.get(new StringBuilder().append(spname).append("_geneScoreMatrix").toString())).get(loc);
                String geneNetwork_loc = species_root + (String) ((JSONObject) inp.get(new StringBuilder().append(spname).append("_geneNetwork").toString())).get(loc);
                String ridgeRegressionPhen_loc = species_root + (String) ((JSONObject) inp.get(new StringBuilder().append(spname).append("_ridgeRegressionPhen").toString())).get(loc);
                String ridgeRegressionGen_loc = species_root + (String) ((JSONObject) inp.get(new StringBuilder().append(spname).append("_ridgeRegressionGen").toString())).get(loc);
                String phenotypeNetwork_loc = species_root + (String) ((JSONObject) inp.get(new StringBuilder().append(spname).append("_phenotypeNetwork").toString())).get(loc);
                String associationMatrix_loc = species_root + (String) ((JSONObject) inp.get(new StringBuilder().append(spname).append("_associationMatrix").toString())).get(loc);
                String associationMatrixGene_loc = species_root + (String) ((JSONObject) inp.get(new StringBuilder().append(spname).append("_associationMatrixGene").toString())).get(loc);

                log.info(associationMatrix_loc);
                
                GENE_FILE.put(id, geneAlias_loc);
                GENE_LOCUS_FILE.put(id, geneLocation_loc);
                PHENOTYPE_FILE.put(id, phenotypeOMIM_loc);
                MORBID_FILE.put(id, morbidMap_loc);
                G_P_NETOWRK.put(id, genePhenAssociation_loc);
                PHENOTYPE_SCORE_MATRIX.put(id, phenotypeScoreMatrix_loc);
                GENE_MATRIX_FILE.put(id, geneScoreMatrix_loc);
                RIDGE_REGRESSION_MATRIX_PHENOTYPESEARCH.put(id, ridgeRegressionPhen_loc);
                RIDGE_REGRESSION_MATRIX_GENESEARCH.put(id, ridgeRegressionGen_loc);
                GENE_NETWORK.put(id, geneNetwork_loc);
                PHENOTYPE_NETWORK.put(id, phenotypeNetwork_loc);
                ASSOCIATION_NETWORK.put(id, associationMatrix_loc);
                ASSOCIATION_NETWORK_GENE.put(id, associationMatrixGene_loc);
                
                //Put (Initialize) the hash structures
                for(int ix = 0; ix < GENE_FILE.size(); ix++){
                    id = String.valueOf(ix+1);
                    geneEntry.put(id, new ArrayList<Entry>());
                    networkEntry.put(id, new ArrayList<Entry>());
                    phenotypeEntry.put(id, new ArrayList<Entry>());
                    networkEntry_gene.put(id, new ArrayList<Entry>());
                    lsAlias.put(id, new ArrayList<Gene>());
                    geneName.put(id, new ArrayList<String>());
                    chromosome.put(id, new ArrayList<String>());
                    
                    startpos.put(id, new ArrayList<String>());
                    stoppos.put(id, new ArrayList<String>());
                    locgene.put(id, new ArrayList<String>());
                    morbidgene.put(id, new ArrayList<String>());
                    morbidphenotype.put(id, new ArrayList<String>());
                    aliasname.put(id, new ArrayList<String>());
                    genevalue.put(id, new ArrayList<String>());
                    lociflag.put(id, new ArrayList<String>());
                    lsomim.put(id, new ArrayList<String>());
                    inverseScore.put(id, new ArrayList<String>());
                    inomim.put(id, new ArrayList<Integer>());
                    ismorbid.put(id, new ArrayList<Integer>());
                    ismorbidgene.put(id, new ArrayList<Integer>());
                    phenotypes.put(id, new ArrayList<Phenotype>());
                    lsPhenotypes.put(id, new ArrayList<Phenotype>());
                    row_p2.put(id, 0);
                    col_p2.put(id, 0);
                }
                
            }
        } catch (Exception e) {
            log.error("Exception Occur in reading json configuration file");
        } finally {
            //Filling in the data
            for(int i = 1; i <= GENE_FILE.size(); i++){
                log.info("Species ID " + String.valueOf(i));
                loadCount(String.valueOf(i));
                loadGeneFile(String.valueOf(i));
                loadPhenotype(String.valueOf(i));
                loadGenePhenAssociation(String.valueOf(i));
                loadGeneLocus(String.valueOf(i));
                loadNetwork(String.valueOf(i));
                loadPhenotypeMatrix(String.valueOf(i));
                //log.info("Ridge");
                //loadRidgeRegressionMatrixForGeneQuery(String.valueOf(i));
                loadGeneMatrix(String.valueOf(i));
            }
        }
        
    }
    

    //Getters
    public static int getCol_g(String id) {return PHENOTYPE_IN_ASSOCIATION_CNT.get(id);}
    public static ArrayList<String> getAliasName(String id) {return aliasname.get(id);}
    public static ArrayList<String> getGeneValue(String id) {return genevalue.get(id);}
    public static ArrayList<String> getLocgene(String id) {return locgene.get(id);}
    public static ArrayList<String> getStartpos(String id) {return startpos.get(id);}
    public static ArrayList<String> getStoppos(String id) {return stoppos.get(id);}
    public static ArrayList<String> getChromosome(String id) {return chromosome.get(id);}
    public static ArrayList<String> getOmimId(String id) {return lsomim.get(id);}
    public static ArrayList<String> getMorbidPhenotypes(String id) {return morbidphenotype.get(id);}
    public static ArrayList<String> getMorbidGenes(String id) {return morbidgene.get(id);}
    public static ArrayList<String> getLociflag(String id) {return lociflag.get(id);}
    public static ArrayList<String> getInverseScore(String id) {return inverseScore.get(id);}
    public static ArrayList<String> getGeneName(String id) {return geneName.get(id);}
    
    public static float[][] getRidgeRegressionMatrix_p(String id) {return RidgeRegressionScores_p.get(id);}
    public static float[][] getRidgeRegressionMatrix_g(String id) {return RidgeRegressionScores_g.get(id);}
    public static float[][] getGeneMatrix(String id) {return GeneScores.get(id);}
    public static float[][] getPhenotypeMatrix(String id) {return PhenotypeScores.get(id);}
    public static float[] getwMatrix(String id) {return g_p_network_matrix_value.get(id);}
    
    public static ArrayList<Entry> getGeneEntry(String id) {return geneEntry.get(id);}
    public static ArrayList<Entry> getPhenotypeEntry(String id) {return phenotypeEntry.get(id);}
    public static ArrayList<Entry> getNetworkEntry(String id) {return networkEntry.get(id);}
    
    public static ArrayList<Integer> getIsDisease(String id) {return ismorbid.get(id);}
    public static ArrayList<Integer> getIsMorbidGene(String id) {return ismorbidgene.get(id);}
    public static ArrayList<Integer> getInOMIM(String id) {return inomim.get(id);}
    
    public static ArrayList<Phenotype> getPhenotypes(String id) {return phenotypes.get(id);}
    public static ArrayList<Phenotype> getLsPhenotypes(String id) {return lsPhenotypes.get(id);}
    
    public static int[][] getwIndex(String id) {return (int[][]) g_p_network_matrix_index.get(id);}
    public static int getrowp2(String id) {return row_p2.get(id);}
    public static int getcolp2(String id) {return col_p2.get(id);}
    
    //public static ArrayList<Gene> getLsGenes() {return lsGenes;}
    public static ArrayList<Gene> getLsAlias(String id) {return lsAlias.get(id);}
    
    private static void loadCount(String _id) throws FileNotFoundException, IOException {
        GENE_CNT.put(_id, FileUtil.fileRowCol(GENE_FILE.get(_id), "\t")[0]);
        PHENOTYPE_CNT.put(_id, FileUtil.fileRowCol(PHENOTYPE_FILE.get(_id), "\t")[0]);
        //GENE_IN_ASSOCIATION_CNT.put(_id, FileUtil.fileRowCol(ASSOCIATION_NETWORK.get(_id), ",")[1]);
        //PHENOTYPE_IN_ASSOCIATION_CNT is calculated in loadNetwork
        
    }
    
    private static void loadNetwork(String _id) throws FileNotFoundException, IOException {
        // 1. load phenotype file with 5 nearest neighbors
        File networkFile = new File(ASSOCIATION_NETWORK.get(_id));
        networkFile.setReadOnly();
        BufferedReader input = new BufferedReader(new FileReader(networkFile));
        String line = null;
        // Temp variables for creating entry
        int id;
        int neighbourTemp;
        Entry lineEntry;
        ArrayList<Integer> totale = new ArrayList<Integer>();

        while ((line = input.readLine()) != null) {
            StringTokenizer temp = new StringTokenizer(line, ",");
            id = Integer.parseInt(temp.nextToken());
            neighbourTemp = Integer.parseInt(temp.nextToken());
            lineEntry = new Entry(id, neighbourTemp);
            // add the neighbors to the array list inside the entry
            while (temp.hasMoreTokens()) {
                int t = Integer.parseInt(temp.nextToken());
                lineEntry.neighbourList.add(t);
                if(!totale.contains(t)){
                    totale.add(t);
                }
            }
            networkEntry.get(_id).add(lineEntry);
        }
        
        log.info(totale.size());
        
        PHENOTYPE_IN_ASSOCIATION_CNT.put(_id, totale.size());
        GENE_IN_ASSOCIATION_CNT.put(_id, totale.size());
        
        input.close();

        //TODO: HAVE TO WORK ON FROM HERE DOWN
        
        //Get genes in OMIM
        ArrayList<Integer> neighbortemp;
        File OMIM_file = new File(MORBID_FILE.get(_id));
        OMIM_file.setReadOnly();
        BufferedReader inputx = new BufferedReader(new FileReader(OMIM_file));
        line = null;

        while ((line = inputx.readLine()) != null) {
            StringTokenizer thistok = new StringTokenizer(line, "\t");
            while (thistok.hasMoreTokens()) {
                String a = thistok.nextToken();
                if (geneName.get(_id).contains(a)) {
                    inomim.get(_id).add(geneName.get(_id).indexOf(a) + 1);
                    
                }
            }
        }
        inputx.close();

        for (int idx = 0; idx < networkEntry.get(_id).size(); idx++) {
            if (networkEntry.get(_id).get(idx).getNumNeighbor() != 0) {
                neighbortemp = networkEntry.get(_id).get(idx).getNeighborList();
                for (int idy = 0; idy < neighbortemp.size(); idy++) {
                    ismorbid.get(_id).add(neighbortemp.get(idy));
                    morbidphenotype.get(_id).add(lsPhenotypes.get(_id).get(idx).Name);
                }
            }
        }

        //second load phnotype
        // 1. load phenotype file with 5 nearest neighbors
        File phenotypeFile = new File(PHENOTYPE_NETWORK.get(_id));
        phenotypeFile.setReadOnly();
        BufferedReader input5 = new BufferedReader(new FileReader(phenotypeFile));
        String line2 = null;

        // Temp variables for creating entry
        int id2;
        int neighbourTemp2;
        Entry lineEntry2;

        while ((line2 = input5.readLine()) != null) {
            StringTokenizer temp = new StringTokenizer(line2, ",");
            id2 = Integer.parseInt(temp.nextToken());
            neighbourTemp2 = Integer.parseInt(temp.nextToken());
            lineEntry2 = new Entry(id2, neighbourTemp2);
            // add the neighbors to the array list inside the entry
            while (temp.hasMoreTokens()) {
                lineEntry2.neighbourList.add(Integer.parseInt(temp.nextToken()));
            }
            phenotypeEntry.get(_id).add(lineEntry2);
        }
        input5.close();

        //third load gene entries
        // 1. load genes which connected with the phenotype:8919 genes with variables of neighbours
        File geneFile = new File(GENE_NETWORK.get(_id));
        geneFile.setReadOnly();

        BufferedReader input3 = new BufferedReader(new FileReader(geneFile));

        String line3 = null;

        // Temp variables for creating entry
        int id3;
        int neighbourTemp3;
        Entry lineEntry3;

        while ((line3 = input3.readLine()) != null) {
            StringTokenizer temp = new StringTokenizer(line3, ",");
            id3 = Integer.parseInt(temp.nextToken());
            neighbourTemp3 = Integer.parseInt(temp.nextToken());
            lineEntry3 = new Entry(id3, neighbourTemp3);
            // add the neighbors to the array list inside the entry
            while (temp.hasMoreTokens()) {
                lineEntry3.neighbourList.add(Integer.parseInt(temp.nextToken()));
            }
            geneEntry.get(_id).add(lineEntry3);
        }
        input3.close();
        log.info("load Network Gene");
        loadNetwork_Gene(_id);
    }
    
    //added by zhanghy
    private static void loadNetwork_Gene(String _id) throws FileNotFoundException, IOException{
        // 1. load gene file with 5 nearest neighbors
        File networkFile = new File(ASSOCIATION_NETWORK_GENE.get(_id));
        networkFile.setReadOnly();
        BufferedReader input = new BufferedReader(new FileReader(networkFile));
        String line = null;
        // Temp variables for creating entry
        int id;
        int neighbourTemp;
        Entry lineEntry;

        while(( line = input.readLine()) != null) {
            StringTokenizer temp = new StringTokenizer(line,",");
            id = Integer.parseInt(temp.nextToken());
            neighbourTemp = Integer.parseInt(temp.nextToken());
            lineEntry = new Entry(id,neighbourTemp);
            // add the neighbors to the array list inside the entry
            while(temp.hasMoreTokens()){
                lineEntry.neighbourList.add(Integer.parseInt(temp.nextToken()));
            }
            networkEntry_gene.get(_id).add(lineEntry);
        }
        input.close();
        ArrayList<Integer> neighbortemp;
        for(int idx = 0; idx < networkEntry_gene.get(_id).size(); idx++){
            if(networkEntry_gene.get(_id).get(idx).getNumNeighbor() != 0){
                neighbortemp = networkEntry_gene.get(_id).get(idx).getNeighborList();
                for(int idy = 0; idy < neighbortemp.size()-1; idy++){
                    ismorbidgene.get(_id).add(neighbortemp.get(idy));
                    morbidgene.get(_id).add(geneName.get(_id).get(idx));
                    
                }
            }
            
        }
    }
    //end
    private static void loadGeneLocus(String _id) throws FileNotFoundException, IOException{
        File geneNameFile = new File(GENE_LOCUS_FILE.get(_id));
        String line = null;
        geneNameFile.setReadOnly();

        BufferedReader input = new BufferedReader(new FileReader(geneNameFile));

        while(( line = input.readLine()) != null) {
            StringTokenizer st = new StringTokenizer(line, "\t");
            chromosome.get(_id).add(st.nextToken());
            startpos.get(_id).add(st.nextToken());
            stoppos.get(_id).add(st.nextToken());
            lociflag.get(_id).add(st.nextToken());
            //st.nextToken();
            locgene.get(_id).add(st.nextToken());
        }
        log.info("GeneManager init location File: total item is: " + locgene.get(_id).size() + " " + chromosome.get(_id).get(0) + " start is " + startpos.get(_id).get(0) );
    }

    //Load Matrix moved from PhenotypeFInder
    //Thanh Le
    private static void loadGenePhenAssociation(String _id) throws FileNotFoundException, IOException{

        StringTokenizer st;
        String line = null;
        
        // 2. Load phenotypes name

        // 3.1 load W matrix
        File wMatrixFile = new File(G_P_NETOWRK.get(_id));
        wMatrixFile.setReadOnly();
		BufferedReader in2 = new BufferedReader(new FileReader(wMatrixFile));
		ArrayList<String> wScore = new ArrayList<String>();
		line = null;
		int row_w = 0;
		while(( line = in2.readLine()) != null ) {
            ArrayList<String> tempLine = new ArrayList<String>();
            st = new StringTokenizer(line,",");
            while (st.hasMoreTokens())
                {
                    tempLine.add(st.nextToken());
                }
            wScore.addAll(tempLine);
            row_w++;
        }
        in2.close();

        //3.2 put the W matrix and its Index information into array
        int col_w = wScore.size() / row_w;
		g_p_network_matrix_value.put(_id, new float[col_w]);
		g_p_network_matrix_index.put(_id, new int [col_w][row_w-1]);

        for(int i=0; i<col_w; i++){
            g_p_network_matrix_value.get(_id)[i]= Float.parseFloat(wScore.get(i));
        }
        for(int i=0; i<row_w-1; i++){
            for(int j=0; j<col_w; j++){
                g_p_network_matrix_index.get(_id)[j][i]=Integer.parseInt(wScore.get((i+1)*col_w+j));
            }
		}
                
    }

    private static void loadGeneFile(String _id){
        File file = new File(GENE_FILE.get(_id));
        if(!file.exists()||file.isDirectory()){
            log.warn("WARN in GeneManager loadGeneFile: there is no gene file.");
        }

        StringTokenizer st;
        StringTokenizer aliasToken;
        try{
            BufferedReader br = new BufferedReader(new FileReader(GENE_FILE.get(_id)));			

            log.info(GENE_FILE.get(_id));
            String strGene = br.readLine();
            String geneSymbolName;
            while (strGene != null) {
                st = new StringTokenizer(strGene, "\t");
                
                geneSymbolName = st.nextToken();
                geneName.get(_id).add(geneSymbolName);

                String tempAliasArray = st.nextToken();
                aliasToken= new StringTokenizer(tempAliasArray, "|");
                while(aliasToken.hasMoreTokens()){
                    String tempalias = aliasToken.nextToken();
                    lsAlias.get(_id).add(new Gene(tempalias, 1));
                    aliasname.get(_id).add(tempalias);
                    genevalue.get(_id).add(geneSymbolName);
                }
                lsomim.get(_id).add(st.nextToken());
                strGene = br.readLine();
            }
            br.close();
        } catch (Exception e) {
            log.warn("WARN in GeneManager loadGeneFile: " + e.getMessage());
        } 

        log.info("GeneManager init Gene File: total item is: " + geneName   .size());
    }
    private static void loadPhenotype(String _id){
		File file=new File(PHENOTYPE_FILE.get(_id));
        file.setReadOnly();
		if(!file.exists()||file.isDirectory()){
			log.warn("WARN in GeneManager loadPhenoType: there is no phenotype file.");
		}
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(PHENOTYPE_FILE.get(_id)));
			
			int Index = 0;
			String strPhenotype = br.readLine();
			while (strPhenotype != null) {
				lsPhenotypes.get(_id).add(new Phenotype(Index, strPhenotype) );
                phenotypes.get(_id).add(new Phenotype(Index, strPhenotype) );
				Index ++;
				strPhenotype = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			log.warn("WARN in GeneManager loadPhenoType: " + e.getMessage());
		}
		log.info("GeneManager init Phenotype File: total item is: " + lsPhenotypes.get(_id).size());
	}

        
        
    private static void loadGeneMatrix(String _id) throws IOException
    {
            File geneScoreFile = new File(GENE_MATRIX_FILE.get(_id));
            BufferedReader input2 = new BufferedReader(new FileReader(geneScoreFile));
            String line = null;
            
            GeneScores.put(_id, new float[GENE_CNT.get(_id)][FileUtil.fileRowCol(GENE_MATRIX_FILE.get(_id), ",")[1]]);
            
            log.info(GENE_CNT.get(_id));
            log.info(FileUtil.fileRowCol(GENE_MATRIX_FILE.get(_id), ",")[1]);
            
            for(int i = 0 ; i < GENE_CNT.get(_id) ; i ++){
                line = input2.readLine();
                int j = 0;
                StringTokenizer st = new StringTokenizer(line, ",");
                while (st.hasMoreTokens())
                {
                    GeneScores.get(_id)[i][j++] = Float.parseFloat(st.nextToken());
                }
                
            }
            input2.close();
	}	 
      
        
        private static void loadPhenotypeMatrix(String _id) throws IOException {
	    File phenotypeScoreFile = new File(PHENOTYPE_SCORE_MATRIX.get(_id));
            phenotypeScoreFile.setReadOnly();
	    BufferedReader input = new BufferedReader(new FileReader(phenotypeScoreFile));
	    ArrayList<String> phenotypeScore = new ArrayList<String>();
	    String line = null;
	    int row_p = 0;
	
	    while(( line = input.readLine()) != null ) {
		    ArrayList<String> tempLine= new ArrayList<String>();
		    StringTokenizer st = new StringTokenizer(line,",");

		    while (st.hasMoreTokens())
		    {
			    tempLine.add(st.nextToken());
		    }
		    phenotypeScore.addAll(tempLine);
		    row_p++;
	    }
	    input.close();
	    int col_p = phenotypeScore.size() / row_p;
            
            PhenotypeScores.put(_id, new float[row_p][col_p]);
            
            for(int i=0; i<row_p; i++){
	        for (int j=0; j<col_p; j++){
	        	PhenotypeScores.get(_id)[i][j] = Float.parseFloat(phenotypeScore.get(i*col_p+j));
	        }	
            }
	}
        
        
    private static void loadRidgeRegressionMatrixForGeneQuery(String _id) throws IOException {

        //4.1 load Inverse Matrix
        File phenotypeScoreFile = new File(RIDGE_REGRESSION_MATRIX_PHENOTYPESEARCH.get(_id));
        phenotypeScoreFile.setReadOnly();
        BufferedReader input = new BufferedReader(new FileReader(phenotypeScoreFile));
        ArrayList<String> inverseScore = new ArrayList<String>();
        String line = null;
        int row_p = 0;

        while(( line = input.readLine()) != null ) {
            ArrayList<String> tempLine= new ArrayList<String>();
            StringTokenizer st = new StringTokenizer(line,",");
            while (st.hasMoreTokens()){
                tempLine.add(st.nextToken());
            }
            inverseScore.addAll(tempLine);
            row_p++;
        }
        input.close();
		 
		 //4.2 put the inverseMatrix scores into a two dimension array and call Ridge Regression
        int col_p = inverseScore.size() / row_p;
        
	    for(int i=0; i<row_p; i++){
            for (int j=0; j<col_p; j++){
                //RidgeRegressionScores_p[i][j] = Float.parseFloat(inverseScore.get(i*col_p+j));
            }	
	    }
	}
    
        
    public static void loadRidgeRegressionMatrixForPhenotypeQuery(String _id) throws IOException
	{
		File ridgeregression = new File(RIDGE_REGRESSION_MATRIX_GENESEARCH.get(_id));
                ridgeregression.setReadOnly();
		BufferedReader input = new BufferedReader(new FileReader(ridgeregression));
		String line = null;               
		for(int i = 0 ; i < GENE_CNT.get(_id) ; i ++){
            line = input.readLine();
            int j=0;
            StringTokenizer st = new StringTokenizer(line, ",");
            while(st.hasMoreElements()){
                RidgeRegressionScores_g.get(_id)[i][j++] = Float.parseFloat(st.nextToken());
            }
        }
        input.close();   
    }
    
    public static ArrayList<MatchedGeneEntry> iniGeneEntry(String _id){
        ArrayList<MatchedGeneEntry> result = new ArrayList<MatchedGeneEntry>();
        for (int i = 0; i < GENE_CNT.get(_id); i++) {
            result.add(new MatchedGeneEntry(geneName.get(_id).get(i), i, 0, _id));
        }
        return result;
    }
    public static void zip(String inputFileName, String zipName) throws Exception 
        { 
                //String zipFileName="d:\\test.zip";//name after zip
                 String zipFileName = zipName;
                 System.out.println(zipFileName);
                 zip(zipFileName, new File(inputFileName)); 
        }
         
        private static void zip(String zipFileName, File inputFile) throws Exception {
                 ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
                 zip(out, inputFile, "");
                 System.out.println("zip done");
                 out.close();
         }
         
        private static void zip(ZipOutputStream out, File f, String base) throws Exception 
        {
                 if (f.isDirectory()) {
                         File[] fl = f.listFiles();
                         out.putNextEntry(new ZipEntry(base + "/"));
                         base = base.length() == 0 ? "" : base + "/";
                         for (int i = 0; i < fl.length; i++) 
                        {
                                 zip(out, fl[i], base + fl[i].getName());
                     }
                 }
                 else {
                         out.putNextEntry(new ZipEntry(base));
                         FileInputStream in = new FileInputStream(f);
                         int b;
                         System.out.println(base);
                         while ( (b = in.read()) != -1) 
                        {
                                 out.write(b);
                         }
                         in.close();
                 }
         }
         public static String getRandomName(){
             UUID uuid = UUID.randomUUID();
             String m_name = uuid.toString();
             return m_name;
         }
         public static boolean deletefile(String delpath) throws FileNotFoundException, IOException {
            try {
                File file = new File(delpath);
                if (!file.isDirectory()) {
                    file.delete();
                }
                else if (file.isDirectory()) {
                    String[] filelist = file.list();
                    for (int i = 0; i < filelist.length; i++) {
                        File delfile = new File(delpath + "\\" + filelist[i]);
                    if (!delfile.isDirectory())
                        delfile.delete();
                    else if (delfile.isDirectory())
                        deletefile(delpath + "\\" + filelist[i]);
                    }
                    file.delete();
                }
            }
            catch (FileNotFoundException e) { }
                return true;
         }
}
