/*
 * Matched Phenotype Class
 * Relevant score of phenotype
 */
package com.ppi;

import java.util.ArrayList;
import org.apache.log4j.Logger;


public class MatchedPhenotypeEntry {
    public String Name;
    public int Id;
    public String OMIM_ID;
    public float RelScore;
    //added by zhanghy
    public String causalgene;
    ArrayList<Integer> ismorbidgene;
    ArrayList<String> morbidGene;
    public boolean ism;
    private static Logger log = Logger.getLogger(MatchedPhenotypeEntry.class);
    //end
    public MatchedPhenotypeEntry(String phenotypeName, int phenotypeId, String OmimId, float relScore, String _id)
    {
            ismorbidgene = ContextManager_new.getIsMorbidGene(_id);
            morbidGene = ContextManager_new.getMorbidGenes(_id);
            
            Name = phenotypeName;
            Id = phenotypeId;
            OMIM_ID = OmimId;
            RelScore = relScore;
            ism = isMorbid(Id+1);
            
    }

    public MatchedPhenotypeEntry(Phenotype p, float relScore, String _id)
    {
            
        ismorbidgene = ContextManager_new.getIsMorbidGene(_id);
        morbidGene = ContextManager_new.getMorbidGenes(_id);
            
        Name = p.Name;
        Id = p.Id;
        OMIM_ID = p.OMIM_ID;
        RelScore = relScore;
        ism = isMorbid(Id+1);
            
    }

    //added by zhanghy
    private boolean isMorbid(int input){
        if(ismorbidgene.contains(input)){
            causalgene = morbidGene.get(ismorbidgene.indexOf(input));
            return true;
        }else{
            return false;
        }
    }

    public void setIsMorbid(){
        ism = isMorbid(Id+1);
    }
}
