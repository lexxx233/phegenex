package com.ppi.web;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.ppi.ContextManager;
import com.ppi.ContextManager_new;

public class WibContext implements ServletContextListener{

	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
            try {
                //ContextManager.init();
                ContextManager_new.init();
                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(WibContext.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(WibContext.class.getName()).log(Level.SEVERE, null, ex);
            }
	}
}
