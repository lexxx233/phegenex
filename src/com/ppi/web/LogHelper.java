package com.ppi.web;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat; 

public class LogHelper {
	public static String GetLogItemFromRequest(HttpServletRequest req)
	{
		SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");     
		String date = sDateFormat.format(new java.util.Date());
		String out = date + "," + getRemoteIP(req) + "," + req.getParameter("Genes").replace("\r\n", "|") + "," + req.getParameter("AlgId");
		return out;
	}
	
	public static String getRemoteIP(HttpServletRequest request) 
	{  
	    if (request.getHeader("x-forwarded-for") == null) 
	    {  
	        return request.getRemoteAddr();  
	    }  
	    return request.getHeader("x-forwarded-for");
	}   
}
