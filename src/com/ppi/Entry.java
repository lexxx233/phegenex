package com.ppi;

import java.util.ArrayList;

public class Entry {
	public int NeighbourNum;
	public int EntryID;
	public ArrayList<Integer> neighbourList = new ArrayList<Integer>();


	public Entry(int id, int neighbour){
		NeighbourNum = neighbour;
		EntryID = id;
	}


	public int getID(){
		return EntryID;
	}

	public int getNumNeighbor(){
		return NeighbourNum;
	}

	public boolean findNeighbor(int target){
		boolean result = neighbourList.contains(target);
		return result;
	}

	public ArrayList<Integer> getNeighborList(){
		return neighbourList;
	}


}
