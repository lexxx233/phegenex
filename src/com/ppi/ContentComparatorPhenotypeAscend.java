package com.ppi;

import java.util.Comparator;

public class ContentComparatorPhenotypeAscend implements Comparator {

 public int compare(Object o1, Object o2) {

	 Phenotype c1 = (Phenotype) o1;
	 Phenotype c2 = (Phenotype) o2;
  if (c1.Id > c2.Id) {
   return 1;
  } else {
     if (c1.Id == c2.Id) {
         return 0;
     } else {
         return -1;
   }
  }
 }
}