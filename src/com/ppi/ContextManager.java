package com.ppi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.*;
import java.util.UUID;


/**
 * Load some init information
 * @author xie
 *
 */

public class ContextManager {
    /*
    // 0. Constants
    public static int GENE_CNT = 9673;
    public static int PHENOTYPE_CNT = 5080;
    public static int GENE_IN_ASSOCIATION_CNT = 1327;
    public static int PHENOTYPE_IN_ASSOCIATION_CNT = 1564;

    // 1. Name list of rcnet datasets.
    private static String DATA_ROOT = "C:\\rcnet_data\\";

    //101 102
    private static String GENE_FILE = DATA_ROOT + "gene_name_alias.txt";
    private static String GENE_LOCUS_FILE = DATA_ROOT + "gene_location.txt";
    private static String PHENOTYPE_FILE = DATA_ROOT + "phenotype_omim.txt";

    //201 202 203 204
    private static String G_P_NETOWRK = DATA_ROOT + "g_p_network.csv";
    private static String PHENOTYPE_SCORE_MATRIX = DATA_ROOT + "phenotype_Matrix.csv";
    private static String GENE_MATRIX_FILE = DATA_ROOT + "gene_Matrix.csv";
    private static String RIDGE_REGRESSION_MATRIX_PHENOTYPESEARCH = DATA_ROOT + "gene_query_ridgeRegression.csv";
    private static String RIDGE_REGRESSION_MATRIX_GENESEARCH = DATA_ROOT + "phenotype_query_ridgeRegression.csv";

    //301 302 303 
    private static String GENE_NETWORK = DATA_ROOT + "ppi_connections.csv";
    private static String PHENOTYPE_NETWORK = DATA_ROOT + "phenotype_connections.csv";
    private static String ASSOCIATION_NETWORK = DATA_ROOT + "g_p_network_connections.csv";
    private static String ASSOCIATION_NETWORK_GENE = DATA_ROOT + "g_p_network_connections_genes.csv";

    // 2. Cache handlers for precomputed results.
    private static ArrayList<Entry> geneEntry = new ArrayList<Entry>();
    private static ArrayList<Entry> networkEntry = new ArrayList<Entry>();
    private static ArrayList<Entry> phenotypeEntry = new ArrayList<Entry>();
    //added by zhanghy
    private static ArrayList<Entry> networkEntry_gene = new ArrayList<Entry>();

//    private static ArrayList<Gene> lsGenes = new ArrayList<Gene>();
    private static ArrayList<Gene> lsAlias = new ArrayList<Gene>();
    
    private static ArrayList<String> geneName = new ArrayList<String>();
    private static ArrayList<String> chromosome = new ArrayList<String>();
    private static ArrayList<String> startpos = new ArrayList<String>();
    private static ArrayList<String> stoppos = new ArrayList<String>();
    private static ArrayList<String> locgene = new ArrayList<String>();
    private static ArrayList<Integer> inomim = new ArrayList<Integer>();
    private static ArrayList<Integer> ismorbid = new ArrayList<Integer>();
    private static ArrayList<Integer> ismorbidgene = new ArrayList<Integer>();
    private static ArrayList<String> morbidgene = new ArrayList<String>();
    private static ArrayList<String> morbidphenotype = new ArrayList<String>();
    private static ArrayList<String> aliasname = new ArrayList<String>();
    private static ArrayList<String> genevalue = new ArrayList<String>();
    //added by zhanghy at 20120905
    private static ArrayList<String> lociflag = new ArrayList<String>();
    //end
    
    private static ArrayList<Phenotype> phenotypes = new ArrayList<Phenotype>();
    private static ArrayList<Phenotype>  lsPhenotypes = new ArrayList<Phenotype>();
    private static ArrayList<String> lsomim = new ArrayList<String>();

    private static int [][] g_p_network_matrix_index;
    private static float [] g_p_network_matrix_value;
    private static float GeneScores[][] = new float[GENE_CNT][GENE_IN_ASSOCIATION_CNT];
    private static float[][] PhenotypeScores = new float[PHENOTYPE_CNT][PHENOTYPE_IN_ASSOCIATION_CNT];
    private static float[][] RidgeRegressionScores_p = new float[PHENOTYPE_CNT][GENE_IN_ASSOCIATION_CNT];
    private static float[][] RidgeRegressionScores_g = new float[GENE_CNT][PHENOTYPE_IN_ASSOCIATION_CNT];

    private static ArrayList<String> inverseScore = new ArrayList<String>();
    private static int row_p2 = 0;
    private static int col_p2 = 0;

		
		 
    private static Logger log = Logger.getLogger(ContextManager.class);
	
    public static void init() throws FileNotFoundException, IOException{
        loadGeneMatrix();
        loadPhenotypeMatrix();
        loadRidgeRegressionMatrixForGeneQuery();
//        loadRidgeRegressionMatrixForPhenotypeQuery();

        loadGeneFile();
        loadPhenotype();
        loadGenePhenAssociation();
        loadGeneLocus();
        loadNetwork();
    }
        
        
        
        
        

        //Added Get Methods
    public static ArrayList<String> getLociflag(){
        return lociflag;
    }
    public static float[][] getRidgeRegressionMatrix_p(){
        return RidgeRegressionScores_p;
    }

    public static float[][] getRidgeRegressionMatrix_g(){
        return RidgeRegressionScores_g;
    }

    public static float[][] getGeneMatrix(){
        return GeneScores;
    }

    public static float[][] getPhenotypeMatrix(){
        return PhenotypeScores;
    }

    public static ArrayList<Entry> getGeneEntry(){
        return geneEntry;
    }


    public static ArrayList<String> getOmimId(){
        return lsomim;
    }

    public static ArrayList<String> getMorbidPhenotypes(){
        return morbidphenotype;
    }
    public static ArrayList<String> getMorbidGenes(){
        return morbidgene;
    }
    public static ArrayList<Integer> getIsDisease(){
        return ismorbid;
    }
    public static ArrayList<Integer> getIsMorbidGene(){
        return ismorbidgene;
    }
    public static ArrayList<Entry> getPhenotypeEntry(){
        return phenotypeEntry;
    }

    public static ArrayList<Entry> getNetworkEntry(){
        return networkEntry;
    }

    public static ArrayList<Integer> getInOMIM(){
        return inomim;
    }

    public static ArrayList<Phenotype> getPhenotypes(){
        return phenotypes;
    }

    public static ArrayList<String> getGeneName(){
        return geneName;
    }

    public static int getrowp2(){
        return row_p2;
    }

    public static int getcolp2(){
        return col_p2;
    }

    public static int[][] getwIndex(){
        return g_p_network_matrix_index;
    }

    public static float[] getwMatrix(){
        return g_p_network_matrix_value;
    }

    public static ArrayList<String> getInverseScore(){
        return inverseScore;
    }
        
//	public static ArrayList<Gene> getLsGenes() {
//            return lsGenes;
//	}

    public static ArrayList<Gene> getLsAlias(){
        return lsAlias;
    }
	
	public static ArrayList<Phenotype> getLsPhenotypes() {
		return lsPhenotypes;
	}

    public static ArrayList<String> getAliasName() {
        return aliasname;
    }

    public static ArrayList<String> getGeneValue(){
        return genevalue;
    }

    public static ArrayList<String> getLocgene(){
        return locgene;
    }

    public static ArrayList<String> getStartpos(){
        return startpos;
    }

    public static ArrayList<String> getStoppos(){
        return stoppos;
    }

    public static ArrayList<String> getChromosome(){
        return chromosome;
    }


    private static void loadNetwork() throws FileNotFoundException, IOException{
        // 1. load phenotype file with 5 nearest neighbors
        File networkFile = new File(ASSOCIATION_NETWORK);
        networkFile.setReadOnly();
        BufferedReader input = new BufferedReader(new FileReader(networkFile));
        String line = null;
        // Temp variables for creating entry
        int id;
        int neighbourTemp;
        Entry lineEntry;

        while(( line = input.readLine()) != null) {
            StringTokenizer temp = new StringTokenizer(line,",");
            id = Integer.parseInt(temp.nextToken());
            neighbourTemp = Integer.parseInt(temp.nextToken());
            lineEntry = new Entry(id,neighbourTemp);
            // add the neighbors to the array list inside the entry
            while(temp.hasMoreTokens()){
                lineEntry.neighbourList.add(Integer.parseInt(temp.nextToken()));
            }
            networkEntry.add(lineEntry);
        }
        input.close();


        //Get genes in OMIM
        ArrayList<Integer> neighbortemp;
        File OMIM_file = new File(DATA_ROOT + "morbidgene.txt");
        OMIM_file.setReadOnly();
        BufferedReader inputx = new BufferedReader(new FileReader(OMIM_file));
        line = null;

        while(( line = inputx.readLine()) != null){
            StringTokenizer thistok = new StringTokenizer(line, ", ");
            while(thistok.hasMoreTokens()){
                String a = thistok.nextToken();
                if(geneName.contains(a)){
                    inomim.add(geneName.indexOf(a) + 1);
                }
            }
        }
        inputx.close();

        for(int idx = 0; idx < networkEntry.size(); idx++){
            if(networkEntry.get(idx).getNumNeighbor() != 0){
                neighbortemp = networkEntry.get(idx).getNeighborList();
                for(int idy = 0; idy < neighbortemp.size(); idy++){
                    ismorbid.add(neighbortemp.get(idy));
                    morbidphenotype.add(lsPhenotypes.get(idx).Name);
                }
            }
        }

        //second load phnotype
        // 1. load phenotype file with 5 nearest neighbors
        File phenotypeFile = new File(PHENOTYPE_NETWORK);
        phenotypeFile.setReadOnly();
        BufferedReader input5 = new BufferedReader(new FileReader(phenotypeFile));
        String line2 = null;

        // Temp variables for creating entry
        int id2;
        int neighbourTemp2;
        Entry lineEntry2;

        while(( line2 = input5.readLine()) != null) {
            StringTokenizer temp = new StringTokenizer(line2,",");
            id2 = Integer.parseInt(temp.nextToken());
            neighbourTemp2 = Integer.parseInt(temp.nextToken());
            lineEntry2 = new Entry(id2,neighbourTemp2);
            // add the neighbors to the array list inside the entry
            while(temp.hasMoreTokens()){
                lineEntry2.neighbourList.add(Integer.parseInt(temp.nextToken()));
            }
            phenotypeEntry.add(lineEntry2);
        }
        input5.close();

        //third load gene entries
        // 1. load genes which connected with the phenotype:8919 genes with variables of neighbours
        File geneFile = new File(GENE_NETWORK);
        geneFile.setReadOnly();

        BufferedReader input3 = new BufferedReader(new FileReader(geneFile));

        String line3 = null;

        // Temp variables for creating entry
        int id3;
        int neighbourTemp3;
        Entry lineEntry3;

        while(( line3 = input3.readLine()) != null) {
            StringTokenizer temp = new StringTokenizer(line3,",");
            id3 = Integer.parseInt(temp.nextToken());
            neighbourTemp3 = Integer.parseInt(temp.nextToken());
            lineEntry3 = new Entry(id3,neighbourTemp3);
            // add the neighbors to the array list inside the entry
            while(temp.hasMoreTokens()){
                lineEntry3.neighbourList.add(Integer.parseInt(temp.nextToken()));
            }
            geneEntry.add(lineEntry3);
        }
        input3.close();
        loadNetwork_Gene();
    }
    //added by zhanghy
    private static void loadNetwork_Gene() throws FileNotFoundException, IOException{
        // 1. load gene file with 5 nearest neighbors
        File networkFile = new File(ASSOCIATION_NETWORK_GENE);
        networkFile.setReadOnly();
        BufferedReader input = new BufferedReader(new FileReader(networkFile));
        String line = null;
        // Temp variables for creating entry
        int id;
        int neighbourTemp;
        Entry lineEntry;

        while(( line = input.readLine()) != null) {
            StringTokenizer temp = new StringTokenizer(line,",");
            id = Integer.parseInt(temp.nextToken());
            neighbourTemp = Integer.parseInt(temp.nextToken());
            lineEntry = new Entry(id,neighbourTemp);
            // add the neighbors to the array list inside the entry
            while(temp.hasMoreTokens()){
                lineEntry.neighbourList.add(Integer.parseInt(temp.nextToken()));
            }
            networkEntry_gene.add(lineEntry);
        }
        input.close();
        ArrayList<Integer> neighbortemp;
        for(int idx = 0; idx < networkEntry_gene.size(); idx++){
            if(networkEntry_gene.get(idx).getNumNeighbor() != 0){
                neighbortemp = networkEntry_gene.get(idx).getNeighborList();
                for(int idy = 0; idy < neighbortemp.size(); idy++){
                    ismorbidgene.add(neighbortemp.get(idy));
                    morbidgene.add(geneName.get(idx));
                }
            }
        }
    }
    //end
    private static void loadGeneLocus() throws FileNotFoundException, IOException{
        File geneNameFile = new File(GENE_LOCUS_FILE);
        String line = null;
        geneNameFile.setReadOnly();

        BufferedReader input = new BufferedReader(new FileReader(geneNameFile));

        while(( line = input.readLine()) != null) {
            StringTokenizer st = new StringTokenizer(line, " ");
            chromosome.add(st.nextToken());
            startpos.add(st.nextToken());
            stoppos.add(st.nextToken());
            lociflag.add(st.nextToken());
            //st.nextToken();
            locgene.add(st.nextToken());
        }
        log.info("GeneManager init location File: total item is: " + locgene.size() + " " + chromosome.get(0) + " start is " + startpos.get(0) );
    }

    //Load Matrix moved from PhenotypeFInder
    //Thanh Le
    private static void loadGenePhenAssociation() throws FileNotFoundException, IOException{

        StringTokenizer st;
        String line = null;
        
        // 2. Load phenotypes name

        // 3.1 load W matrix
        File wMatrixFile = new File(G_P_NETOWRK);
        wMatrixFile.setReadOnly();
		BufferedReader in2 = new BufferedReader(new FileReader(wMatrixFile));
		ArrayList<String> wScore = new ArrayList<String>();
		line = null;
		int row_w = 0;
		while(( line = in2.readLine()) != null ) {
            ArrayList<String> tempLine = new ArrayList<String>();
            st = new StringTokenizer(line,",");
            while (st.hasMoreTokens())
                {
                    tempLine.add(st.nextToken());
                }
            wScore.addAll(tempLine);
            row_w++;
        }
        in2.close();

        //3.2 put the W matrix and its Index information into array
        int col_w = wScore.size() / row_w;
		g_p_network_matrix_value= new float[col_w];
		g_p_network_matrix_index=new int [col_w][row_w-1];

        for(int i=0; i<col_w; i++){
            g_p_network_matrix_value[i]= Float.parseFloat(wScore.get(i));
        }
        for(int i=0; i<row_w-1; i++){
            for(int j=0; j<col_w; j++){
                g_p_network_matrix_index[j][i]=Integer.parseInt(wScore.get((i+1)*col_w+j));
            }
		}
                
    }

    private static void loadGeneFile(){
        File file = new File(GENE_FILE);
        if(!file.exists()||file.isDirectory()){
            log.warn("WARN in GeneManager loadGeneFile: there is no gene file.");
        }

        StringTokenizer st;
        StringTokenizer aliasToken;
        try{
            BufferedReader br = new BufferedReader(new FileReader(GENE_FILE));			

            String strGene = br.readLine();
            String geneSymbolName;
            while (strGene != null) {
                st = new StringTokenizer(strGene, " ");
                geneSymbolName = st.nextToken();
                geneName.add(geneSymbolName);

                String tempAliasArray = st.nextToken();
                aliasToken= new StringTokenizer(tempAliasArray, "|");
                while(aliasToken.hasMoreTokens()){
                    String tempalias = aliasToken.nextToken();
                    lsAlias.add(new Gene(tempalias, 1));
                    aliasname.add(tempalias);
                    genevalue.add(geneSymbolName);
                }

                lsomim.add(st.nextToken());
                strGene = br.readLine();
            }
            br.close();
        } catch (Exception e) {
            log.warn("WARN in GeneManager loadGeneFile: " + e.getMessage());
        } 

        log.info("GeneManager init Gene File: total item is: " + geneName.size());
    }
        
	private static void loadPhenotype(){
		File file=new File(PHENOTYPE_FILE);
        file.setReadOnly();
		if(!file.exists()||file.isDirectory()){
			log.warn("WARN in GeneManager loadPhenoType: there is no phenotype file.");
		}
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(PHENOTYPE_FILE));
			
			int Index = 0;
			String strPhenotype = br.readLine();
			while (strPhenotype != null) {
				lsPhenotypes.add(new Phenotype(Index, strPhenotype) );
                phenotypes.add(new Phenotype(Index, strPhenotype) );
				Index ++;
				strPhenotype = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			log.warn("WARN in GeneManager loadPhenoType: " + e.getMessage());
		}
		log.info("GeneManager init Phenotype File: total item is: " + lsPhenotypes.size());
	}

        
        
    private static void loadGeneMatrix() throws IOException
    {
            File geneScoreFile = new File(GENE_MATRIX_FILE);
            BufferedReader input2 = new BufferedReader(new FileReader(geneScoreFile));
            String line = null;

            for(int i = 0 ; i < GENE_CNT ; i ++){
                line = input2.readLine();
                int j = 0;
                StringTokenizer st = new StringTokenizer(line, ",");
                while (st.hasMoreTokens())
                {
                        GeneScores[i][j++] = Float.parseFloat(st.nextToken());
                }

            }
            input2.close();
	}	 
      
        
        
        private static void loadPhenotypeMatrix() throws IOException {
	    File phenotypeScoreFile = new File(PHENOTYPE_SCORE_MATRIX);
            phenotypeScoreFile.setReadOnly();
	    BufferedReader input = new BufferedReader(new FileReader(phenotypeScoreFile));
	    ArrayList<String> phenotypeScore = new ArrayList<String>();
	    String line = null;
	    int row_p = 0;
	
	    while(( line = input.readLine()) != null ) {
		    ArrayList<String> tempLine= new ArrayList<String>();
		    StringTokenizer st = new StringTokenizer(line,",");

		    while (st.hasMoreTokens())
		    {
			    tempLine.add(st.nextToken());
		    }
		    phenotypeScore.addAll(tempLine);
		    row_p++;
	    }
	    input.close();
	    int col_p = phenotypeScore.size() / row_p;
	    
            for(int i=0; i<row_p; i++){
	        for (int j=0; j<col_p; j++){
	        	PhenotypeScores[i][j] = Float.parseFloat(phenotypeScore.get(i*col_p+j));
	        }	
            }
	}
        
        
    private static void loadRidgeRegressionMatrixForGeneQuery() throws IOException {

        //4.1 load Inverse Matrix
        File phenotypeScoreFile = new File(RIDGE_REGRESSION_MATRIX_PHENOTYPESEARCH);
        phenotypeScoreFile.setReadOnly();
        BufferedReader input = new BufferedReader(new FileReader(phenotypeScoreFile));
        ArrayList<String> inverseScore = new ArrayList<String>();
        String line = null;
        int row_p = 0;

        while(( line = input.readLine()) != null ) {
            ArrayList<String> tempLine= new ArrayList<String>();
            StringTokenizer st = new StringTokenizer(line,",");
            while (st.hasMoreTokens()){
                tempLine.add(st.nextToken());
            }
            inverseScore.addAll(tempLine);
            row_p++;
        }
        input.close();
		 
		 //4.2 put the inverseMatrix scores into a two dimension array and call Ridge Regression
        int col_p = inverseScore.size() / row_p;
        
	    for(int i=0; i<row_p; i++){
            for (int j=0; j<col_p; j++){
                //RidgeRegressionScores_p[i][j] = Float.parseFloat(inverseScore.get(i*col_p+j));
            }	
	    }
	}
        
        
    public static void loadRidgeRegressionMatrixForPhenotypeQuery() throws IOException
	{
		File ridgeregression = new File(RIDGE_REGRESSION_MATRIX_GENESEARCH);
                ridgeregression.setReadOnly();
		BufferedReader input = new BufferedReader(new FileReader(ridgeregression));
		String line = null;               
		for(int i = 0 ; i < GENE_CNT ; i ++){
            line = input.readLine();
            int j=0;
            StringTokenizer st = new StringTokenizer(line, ",");
            while(st.hasMoreElements()){
                RidgeRegressionScores_g[i][j++] = Float.parseFloat(st.nextToken());
            }
        }
        input.close();   
    }
    
    public static ArrayList<MatchedGeneEntry> iniGeneEntry(){
        ArrayList<MatchedGeneEntry> result = new ArrayList<MatchedGeneEntry>();
        for (int i = 0; i < ContextManager.GENE_CNT; i++) {
            result.add(new MatchedGeneEntry(geneName.get(i), i, 0));
        }
        return result;
    }
    public static void zip(String inputFileName, String zipName) throws Exception 
        { 
                //String zipFileName="d:\\test.zip";//name after zip
                 String zipFileName = zipName;
                 System.out.println(zipFileName);
                 zip(zipFileName, new File(inputFileName)); 
        }
         
        private static void zip(String zipFileName, File inputFile) throws Exception {
                 ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
                 zip(out, inputFile, "");
                 System.out.println("zip done");
                 out.close();
         }
         
        private static void zip(ZipOutputStream out, File f, String base) throws Exception 
        {
                 if (f.isDirectory()) {
                         File[] fl = f.listFiles();
                         out.putNextEntry(new ZipEntry(base + "/"));
                         base = base.length() == 0 ? "" : base + "/";
                         for (int i = 0; i < fl.length; i++) 
                        {
                                 zip(out, fl[i], base + fl[i].getName());
                     }
                 }
                 else {
                         out.putNextEntry(new ZipEntry(base));
                         FileInputStream in = new FileInputStream(f);
                         int b;
                         System.out.println(base);
                         while ( (b = in.read()) != -1) 
                        {
                                 out.write(b);
                         }
                         in.close();
                 }
         }
         public static String getRandomName(){
             UUID uuid = UUID.randomUUID();
             String m_name = uuid.toString();
             return m_name;
         }
         public static boolean deletefile(String delpath) throws FileNotFoundException, IOException {
            try {
                File file = new File(delpath);
                if (!file.isDirectory()) {
                    file.delete();
                }
                else if (file.isDirectory()) {
                    String[] filelist = file.list();
                    for (int i = 0; i < filelist.length; i++) {
                        File delfile = new File(delpath + "\\" + filelist[i]);
                    if (!delfile.isDirectory())
                        delfile.delete();
                    else if (delfile.isDirectory())
                        deletefile(delpath + "\\" + filelist[i]);
                    }
                    file.delete();
                }
            }
            catch (FileNotFoundException e) { }
                return true;
         }
         */
}


//
//        private static void loadAliasFile(){
//
//            File file=new File(GENE_FILE);
//            file.setReadOnly();
//            if(!file.exists()||file.isDirectory())
//            {
//			log.warn("WARN in GeneManager loadGeneFile: there is no alias file.");
//            }
//            try {
//			FileReader fr = new FileReader(GENE_FILE);
//			BufferedReader br = new BufferedReader(fr);
//
//                        StringTokenizer st;
//			String strGene = br.readLine();
//			while (strGene != null) {
//                                st = new StringTokenizer(strGene, " ");
//                                String temp;
//
//                                //TODO: fix later. the alias table is not complete need to run processor
//                                //again due to not flusing output to file -> premature end of program before
//                                //content is written to file
//                                if (st.countTokens() == 1){
//                                    temp = st.nextToken();
//                                    lsAlias.add(new Gene(temp, 1));
//                                    //Store value for lookup purpose
//                                    aliasname.add(temp);
//                                    genevalue.add(temp);
//                                }
//
//                                //only interested in alias name not meani
//                                temp = st.nextToken();
//                                String tempalias;
//                                st = new StringTokenizer(st.nextToken(), "|");
//
//                                while(st.hasMoreTokens()){
//                                    tempalias = st.nextToken();
//                                    lsAlias.add(new Gene(tempalias, 1));
//
//                                    //Store value for lookup
//                                    aliasname.add(tempalias);
//                                    genevalue.add(temp);
//                                }
//
//				strGene = br.readLine();
//			}
//			br.close();
//			fr.close();
//		} catch (Exception e) {
//			log.warn("WARN in GeneManager loadAliasFile: " + e.getMessage());
//		}
//
//		log.info("GeneManager init Alias File: total item is: " + lsAlias.size());
//
//
//        }
