/*
 * Compare two Content
 */
package com.ppi;

import java.util.Comparator;

public class ContentComparatorAscend implements Comparator {


 public int compare(Object o1, Object o2) {

  try{
	 MatchedPhenotypeEntry c1 = (MatchedPhenotypeEntry) o1;
	 MatchedPhenotypeEntry c2 = (MatchedPhenotypeEntry) o2;
  if (c1.RelScore > c2.RelScore) {
   return 1;
  } else {
     if (c1.RelScore == c2.RelScore) {
         return 0;
     } else {
         return -1;
   }
  }
 }catch(ClassCastException e){
     MatchedGeneEntry c1 = (MatchedGeneEntry) o1;
     MatchedGeneEntry c2 = (MatchedGeneEntry) o2;
     /*
     if(c1.RelScore == Double.NaN){
         System.out.println("NAN");
         return 1;
     }
     if(c2.RelScore == Double.NaN){
         System.out.println("NAN");
         return -1;
     }
      * 
      */
     if (c1.RelScore > c2.RelScore) {
   return 1;
  } else {
     if (c1.RelScore == c2.RelScore) {
         return 0;
     } else {
         return -1;
   }
  }
 }
 }
 
}