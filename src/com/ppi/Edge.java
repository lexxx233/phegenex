package com.ppi;


public class Edge {

    public String target;
    public String source;
	
	public Edge(int type, String index1, String index2){
		String edgeType;
		target = index2;
                source = index1;
		if(type == 0){
			edgeType = "GG";
		}else if(type==1){
			edgeType = "PP";
		}else if(type==2){
			edgeType = "PG";
		}
		
		source = index1;
		target = index2;
	}

        public String getTarget(){
            return target;
        }
        public String getSource(){
            return source;
        }
	
}
