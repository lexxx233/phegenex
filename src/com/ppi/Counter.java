/*
 * This is a visitor counter
 */
package com.ppi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Thanh Xuan Le
 */
public class Counter {
    
    public int count;
    
    public Counter() throws FileNotFoundException, IOException{
        File f = new File("C:/Users/Public/counter.txt");
        if(f.exists()){
            BufferedReader bin = new BufferedReader(new FileReader(f)); 
            count = Integer.parseInt(bin.readLine());
            count++;
            f.delete();
            f.createNewFile();
            BufferedWriter bw = new BufferedWriter(new FileWriter(f));
            bw.write(count);
            bw.close();
            
        }else{ //File not exist
            count = 1;
            BufferedWriter bw = new BufferedWriter(new FileWriter(f));
            bw.write("1");
            bw.flush();
            bw.close();
        }
    
    }
    
}
