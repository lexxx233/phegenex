package com.ppi.util;

import java.util.ArrayList;

public class ErrorMng {
	public static String GetErrorDescription(String error_code)
	{
		ArrayList<String> strCode = new ArrayList<String>();
		strCode.add("501");
		strCode.add("502");
		//modified by zhanghy at 20120907
                strCode.add("503");
                strCode.add("506");
		ArrayList<String> strDescription = new ArrayList<String>();
		strDescription.add("You should provide a valid number!");
		strDescription.add("Please input valid genes or phenotypes!");
                strDescription.add("Please check the input format or the number limits!");
                strDescription.add("No corrsponding genes found on input locus!");
		int Index = strCode.indexOf(error_code);
		return (Index < 0) ? "" : strDescription.get(Index);
	}
}