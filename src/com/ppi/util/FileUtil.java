/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ppi.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
/**
 *
 * @author compbio
 */
public class FileUtil {
    
       public static int[] fileRowCol(String filename, String delim) throws FileNotFoundException, IOException{
            int[] counts = {0,0}; //counts[0] = # of rows, [1] = # of columns
            BufferedReader br = new BufferedReader(new FileReader(filename));
            
            String line = br.readLine();
            StringTokenizer st = new StringTokenizer(line, delim);
            counts[1] = st.countTokens();
            while(line != null){
                line = br.readLine();
                counts[0] += 1;
            }
            
            return counts;
        }
       
}
