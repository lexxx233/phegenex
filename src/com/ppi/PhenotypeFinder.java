package com.ppi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;

import javax.servlet.*;
import org.apache.log4j.Logger;

public class PhenotypeFinder{
    // AlgID :
    //		1 : Laplacian Score
    //		2 : Coefficency Score
    //      3 : Ridge Regression

    String id = "1";
    
    int GENE_CNT, PHENOTYPE_CNT, GENE_IN_ASSOCIATION_CNT, PHENOTYPE_IN_ASSOCIATION_CNT;
    int[][] wIndex;
    
    float[][] phenotypeMatrix;
    float[][] GeneMatrix;
    float[] wMatrix;
    
    ArrayList<String> geneName;
    // new
    ArrayList<String> inverseScore;
    int row_p2, col_p2;
    //Lookup table for alias gene name
    ArrayList<String> aliasname, genevalue;
    //chromosomal location
    ArrayList<String> chromosome, startpos, stoppos, locgene;
    //Network information

    ArrayList<Phenotype> phenotypes;
    ArrayList<Entry> geneEntry, phenotypeEntry, networkEntry;
    
    String line = null;
    public PhenotypeFinder(String _id){
        id = _id;
        
        GENE_CNT = ContextManager_new.GENE_CNT.get(id);
        PHENOTYPE_CNT = ContextManager_new.PHENOTYPE_CNT.get(id);
        GENE_IN_ASSOCIATION_CNT = ContextManager_new.GENE_IN_ASSOCIATION_CNT.get(id);
        PHENOTYPE_IN_ASSOCIATION_CNT = ContextManager_new.PHENOTYPE_IN_ASSOCIATION_CNT.get(id);
        
        geneName = ContextManager_new.getGeneName(id);
        phenotypes = ContextManager_new.getPhenotypes(id);
        inverseScore = ContextManager_new.getInverseScore(id);
        row_p2 = ContextManager_new.getrowp2(id);
        col_p2 = ContextManager_new.getcolp2(id);
        locgene = ContextManager_new.getLocgene(id);
        aliasname = ContextManager_new.getAliasName(id);
        genevalue = ContextManager_new.getGeneValue(id);
        chromosome = ContextManager_new.getChromosome(id);
        startpos = ContextManager_new.getStartpos(id);
        stoppos = ContextManager_new.getStoppos(id);
        geneEntry = ContextManager_new.getGeneEntry(id);
        phenotypeEntry = ContextManager_new.getPhenotypeEntry(id);
        networkEntry = ContextManager_new.getNetworkEntry(id);
        phenotypes = ContextManager_new.getPhenotypes(id);
        wMatrix = ContextManager_new.getwMatrix(id);
        wIndex = ContextManager_new.getwIndex(id);
        phenotypeMatrix = ContextManager_new.getPhenotypeMatrix(id);
        GeneMatrix = ContextManager_new.getGeneMatrix(id);
                
    }
//added by zhanghy
/*public void exportBase64(HttpServletResponse response, 
           @RequestParam(value="format", defaultValue="png") String format,
           @RequestBody byte postBody) throws IOException
{



        
        try {
            response.setContentType(getContentMIMEType(format));
            response.setHeader("Content-disposition",contentDisposition);
            response.getOutputStream().write(postBody.getBytes());
            response.flushBuffer();
        }
catch(Exception e){}




}*/
/*public void exportBase64(String format, byte postBody) throws IOException
{


    string Ipath = ConfigurationManager.AppSettings["WebDirPath"] + dtoPubattach.FILEPATH.TrimStart('/').Replace("/", "\\");
    byte[] imageBytes = Convert.FromBase64String(cxmldocImg.SelectSingleNode("//IMAGECONTENT").InnerText);
    MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
    ms.Write(imageBytes, 0, imageBytes.Length);
    System.Drawing.Image imag = System.Drawing.Image.FromStream(ms, true);
    imag.Save(Ipath);
    ms.Flush();
    ms.Close();
    //将base64流转换



}*/
    //This method is to validate weather the input symbols are known
        public boolean containLocus(String strReqGenes){
            strReqGenes = strReqGenes.toUpperCase();
            strReqGenes = strReqGenes.trim();
            ArrayList<Gene> genes = new ArrayList<Gene>();
            // 1. Split string into string array, in which gene names are stored.
            if(strReqGenes.isEmpty()){
                return false;
            }
            String[] strGenes = strReqGenes.split("\r\n");
            for(int i = 0 ; i < strGenes.length; i++){
                strGenes[i] = strGenes[i].trim();
            }
            for(int i = 0 ; i < strGenes.length; i++)
            {
                //Translate query of alias gene name into recognizable gene name
                //Still store the old name of the gene for returning Genecard result   
                strGenes[i] = strGenes[i].trim();
                String tempstr = strGenes[i];
                //PARSER - special input
                StringTokenizer st = new StringTokenizer(tempstr, ":");
                if(st.countTokens() > 1){
                    //If there are more than 1 token. This is a special command
                    String tempcmd = st.nextToken().trim();
                    int index = 0;
                    //chromosome:chromosome#:start:stop
                    if(tempcmd.compareTo("CHROMOSOME") == 0 && st.countTokens() == 2){
                        String tempchromosome = st.nextToken().trim();
                        if(tempchromosome.equals("X")||tempchromosome.equals("Y")){}
                        else{continue;}
                        st = new StringTokenizer(st.nextToken(), "-");
                        String tempstart = st.nextToken().trim();
                        String tempstop = st.nextToken().trim();
                        try{
                            int m_start = Integer.parseInt(tempstart);
                            int m_stop = Integer.parseInt(tempstop);
                            return true;
                        }
                        catch(Exception e){
                            return false;
                        }
                    } //upstream:k:GENE
                    else if(tempcmd.compareTo("UPSTREAM") == 0 && st.countTokens() == 2){
                        continue;
                    } //downstream:k:GENE
                    else if(tempcmd.compareTo("DOWNSTREAM") == 0 && st.countTokens() == 2){
                        continue;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            return false;
        }
        public  ArrayList<Gene> getOtherGenes(String strReqGenes)
        {
             //ArrayList<Gene> genes = new  ArrayList<Gene>();
             //return genes;\
            strReqGenes = strReqGenes.toUpperCase();
            strReqGenes = strReqGenes.trim();
            ArrayList<Gene> genes = new ArrayList<Gene>();

            // 1. Split string into string array, in which gene names are stored.
            if(strReqGenes.isEmpty()){
                return genes;
            }
            String[] strGenes = strReqGenes.split("\r\n");
            for(int i = 0 ; i < strGenes.length; i++){
                strGenes[i] = strGenes[i].trim();
            }
            for(int i = 0 ; i < strGenes.length; i++)
            {
                //Translate query of alias gene name into recognizable gene name
                //Still store the old name of the gene for returning Genecard result   
                strGenes[i] = strGenes[i].trim();
                String tempstr = strGenes[i];
                //PARSER - special input
                StringTokenizer st = new StringTokenizer(tempstr, ":");
                if(st.countTokens() > 1){
                    //If there are more than 1 token. This is a special command
                    String tempcmd = st.nextToken().trim();
                    int index = 0;
                    //chromosome:chromosome#:start:stop
                    if(tempcmd.compareTo("CHROMOSOME") == 0 && st.countTokens() == 2){
                        String tempchromosome = st.nextToken().trim();
                        st = new StringTokenizer(st.nextToken(), "-");
                        String tempstart = st.nextToken().trim();
                        String tempstop = st.nextToken().trim();
                        try{
                            for(int op = 0; op < chromosome.size(); op++){
                                if(tempchromosome.equals(chromosome.get(op))){
                                    if(Integer.parseInt(startpos.get(op)) >= Integer.parseInt(tempstart) && Integer.parseInt(stoppos.get(op)) <= Integer.parseInt(tempstop)){
                                        index = geneName.indexOf(locgene.get(op));
                                        genes.add(new Gene(locgene.get(op), index));
                                    }
                                }
                            }
                        }
                        catch(Exception e){
                            return null;
                        }
                        continue;
                    } //upstream:k:GENE
                    else if(tempcmd.compareTo("UPSTREAM") == 0 && st.countTokens() == 2){

                        String tempk = st.nextToken().trim();
                        String tempgene = st.nextToken().trim();

                        index = geneName.indexOf(tempgene);

                        if (HasRepeatGene(genes, index) == false){
                            genes.add(new Gene(tempgene, index));
                        }

                        int tk = 0;
                        try{
                            tk = Integer.parseInt(tempk);
                        }
                        catch(Exception e){
                            return null;
                        }
                        //Get locgene at location
                        int tempi = locgene.indexOf(tempgene);
                        int m_flag = 0;
                         while(tk > 0){
                            m_flag = 0;
                            tempi--;
                            if(tempi < 0)
                            {
                                break;
                            }
                            for(int gi = 0; gi < geneName.size(); gi++){
                                if(geneName.get(gi).equals(locgene.get(tempi))){
                                    m_flag = 1;
                                    if (HasRepeatGene(genes, geneName.indexOf(locgene.get(tempi))) == false){
                                        //genes.add(new Gene(locgene.get(tempi), geneName.indexOf(locgene.get(tempi))));
                                        tk--;
                                        break;
                                    }
                                    else{
                                        break;
                                    }
                                }
                            }
                            if(m_flag == 0){
                                int fg = 0;
                                for(int gi = 0; gi < genes.size(); gi++){
                                    if(genes.get(gi).Name.equals(locgene.get(tempi))){
                                        fg = 1;
                                        break;
                                    }
                                }
                                if(fg == 0){
                                    genes.add(new Gene(locgene.get(tempi), geneName.indexOf(locgene.get(tempi))));
                                }   
                            }
                        }

                        /*for(int c = 0; c < tk; c++){
                            tempi--;
                            if(tempi < 0){break;}

                            if (HasRepeatGene(genes, geneName.indexOf(locgene.get(tempi))) == false){
                                genes.add(new Gene(locgene.get(tempi), geneName.indexOf(locgene.get(tempi))));
                            }
                        }*/

                        continue;
                    } //downstream:k:GENE
                    else if(tempcmd.compareTo("DOWNSTREAM") == 0 && st.countTokens() == 2){

                        String tempk = st.nextToken().trim();
                        String tempgene = st.nextToken().trim();

                        index = geneName.indexOf(tempgene);

                        if (HasRepeatGene(genes, index) == false){
                            genes.add(new Gene(tempgene, index));
                        }
                        int tk = 0;
                        try{
                            tk = Integer.parseInt(tempk);}
                        catch(Exception e){
                            return null;
                        }

                        //Get locgene at location
                        int tempi = locgene.indexOf(tempgene);
                        int m_flag = 0;
                        while(tk > 0){
                            tempi++;
                            if(tempi > locgene.size())
                            {
                                break;
                            }
                            m_flag = 0;
                            for(int gi = 0; gi < geneName.size(); gi++){
                                if(geneName.get(gi).equals(locgene.get(tempi))){
                                    m_flag = 1;
                                    if (HasRepeatGene(genes, geneName.indexOf(locgene.get(tempi))) == false){
                                        //genes.add(new Gene(locgene.get(tempi), geneName.indexOf(locgene.get(tempi))));
                                        tk--;
                                        break;
                                    }
                                    else{
                                        break;
                                    }
                                }
                            }
                            if(m_flag == 0){
                                int fg = 0;
                                for(int gi = 0; gi < genes.size(); gi++){
                                    if(genes.get(gi).Name.equals(locgene.get(tempi))){
                                        fg = 1;
                                        break;
                                    }
                                }
                                if(fg == 0){
                                    genes.add(new Gene(locgene.get(tempi), geneName.indexOf(locgene.get(tempi))));
                                }   
                            }
                        }
                        /*for(int c = 0; c < tk; c++){
                            tempi++;
                            if(tempi > locgene.size())
                            {
                                break;
                            }

                            if (HasRepeatGene(genes, geneName.indexOf(locgene.get(tempi))) == false){
                                genes.add(new Gene(locgene.get(tempi), geneName.indexOf(locgene.get(tempi))));
                            }
                        }*/

                        continue;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            ArrayList<Gene> tempgenes = new ArrayList<Gene>();
            int flag = 0;
            for (int i = 0; i < genes.size(); i ++)
            {
                Gene tempgene = genes.get(i);
                flag = 0;
                for (int j = 0; j <geneName.size(); j ++)
                {
                    if (tempgene.Name.equals(geneName.get(j)))
                    {
                       flag = 1;
                       break;
                    }
                }
                if(flag == 0){
                    int m_flag = 0;
                    for (int j = 0; j <tempgenes.size(); j ++)
                    {
                        if (tempgene.Name.equals(tempgenes.get(j).Name)){
                            m_flag = 1;
                            break;
                        }
                    }
                    if(m_flag == 0)
                    {
                         tempgenes.add(tempgene);
                    }
                     
                }
            }    
            return tempgenes;
        }
	public ArrayList<Gene> ValidateGenes(String strReqGenes) throws IOException
	{

            strReqGenes = strReqGenes.toUpperCase();
            strReqGenes = strReqGenes.trim();
            ArrayList<Gene> genes = new ArrayList<Gene>();

            // 1. Split string into string array, in which gene names are stored.
            if(strReqGenes.isEmpty()){
                return genes;
            }
            String[] strGenes = strReqGenes.split("\r\n");
            for(int i = 0 ; i < strGenes.length; i++){
                strGenes[i] = strGenes[i].trim();
            }
            int index = 0;
            for(int i = 0 ; i < strGenes.length; i++)
            {
                //Translate query of alias gene name into recognizable gene name
                //Still store the old name of the gene for returning Genecard result   
                strGenes[i] = strGenes[i].trim();
                String tempstr = strGenes[i];
                //PARSER - special input
                StringTokenizer st = new StringTokenizer(tempstr, ":");
                if(st.countTokens() > 1){
                    //If there are more than 1 token. This is a special command
                    String tempcmd = st.nextToken().trim();

                    //chromosome:chromosome#:start:stop
                    if(tempcmd.compareTo("CHROMOSOME") == 0 && st.countTokens() == 2){
                        String tempchromosome = st.nextToken().trim();
                        st = new StringTokenizer(st.nextToken(), "-");
                        String tempstart = st.nextToken().trim();
                        String tempstop = st.nextToken().trim();
                        try{
                            for(int op = 0; op < chromosome.size(); op++){
                                if(tempchromosome.equals(chromosome.get(op))){
                                    if(Integer.parseInt(startpos.get(op)) >= Integer.parseInt(tempstart) && Integer.parseInt(stoppos.get(op)) <= Integer.parseInt(tempstop)){
                                        index = geneName.indexOf(locgene.get(op));
                                        if(!HasRepeatGene(genes, index)){
                                            genes.add(new Gene(locgene.get(op), index));
                                        }
                                        
                                    }
                                }
                            }
                        }
                        catch(Exception e){
                            log.warn(e.getMessage());
                            return null;
                        }
                        continue;
                    } //upstream:k:GENE
                    else if(tempcmd.compareTo("UPSTREAM") == 0 && st.countTokens() == 2){

                        String tempk = st.nextToken().trim();
                        String tempgene = st.nextToken().trim();

                        index = geneName.indexOf(tempgene);

                        if (HasRepeatGene(genes, index) == false){
                            genes.add(new Gene(tempgene, index));
                        }

                        int tk = 0;
                        try{
                            tk = Integer.parseInt(tempk);
                        }
                        catch(Exception e){
                            return null;
                        }
                        //Get locgene at location
                        int tempi = locgene.indexOf(tempgene);
                         while(tk > 0){
                            tempi--;
                            if(tempi < 0)
                            {
                                break;
                            }
                            for(int gi = 0; gi < geneName.size(); gi++){
                                if(geneName.get(gi).equals(locgene.get(tempi))){
                                    if (HasRepeatGene(genes, geneName.indexOf(locgene.get(tempi))) == false){
                                        genes.add(new Gene(locgene.get(tempi), geneName.indexOf(locgene.get(tempi))));
                                        tk--;
                                        break;
                                    }
                                    else{
                                        break;
                                    }
                                }
                            }
                        }

                        /*for(int c = 0; c < tk; c++){
                            tempi--;
                            if(tempi < 0){break;}

                            if (HasRepeatGene(genes, geneName.indexOf(locgene.get(tempi))) == false){
                                genes.add(new Gene(locgene.get(tempi), geneName.indexOf(locgene.get(tempi))));
                            }
                        }*/

                        continue;
                    } //downstream:k:GENE
                    else if(tempcmd.compareTo("DOWNSTREAM") == 0 && st.countTokens() == 2){

                        String tempk = st.nextToken().trim();
                        String tempgene = st.nextToken().trim();

                        index = geneName.indexOf(tempgene);

                        if (HasRepeatGene(genes, index) == false){
                            genes.add(new Gene(tempgene, index));
                        }
                        int tk = 0;
                        try{
                            tk = Integer.parseInt(tempk);}
                        catch(Exception e){
                            return null;
                        }

                        //Get locgene at location
                        int tempi = locgene.indexOf(tempgene);

                        while(tk > 0){
                            tempi++;
                            if(tempi > locgene.size())
                            {
                                break;
                            }
                            for(int gi = 0; gi < geneName.size(); gi++){
                                if(geneName.get(gi).equals(locgene.get(tempi))){
                                    if (HasRepeatGene(genes, geneName.indexOf(locgene.get(tempi))) == false){
                                        genes.add(new Gene(locgene.get(tempi), geneName.indexOf(locgene.get(tempi))));
                                        tk--;
                                        break;
                                    }
                                    else{
                                        break;
                                    }
                                }
                            }
                        }
                        /*for(int c = 0; c < tk; c++){
                            tempi++;
                            if(tempi > locgene.size())
                            {
                                break;
                            }

                            if (HasRepeatGene(genes, geneName.indexOf(locgene.get(tempi))) == false){
                                genes.add(new Gene(locgene.get(tempi), geneName.indexOf(locgene.get(tempi))));
                            }
                        }*/

                        continue;
                    }
                }

                //END PARSER
                if(aliasname.indexOf(strGenes[i]) < 0){
                        continue;
                }

                String temp = genevalue.get(aliasname.indexOf(strGenes[i]));
                index = geneName.indexOf(temp);
                if (index >= 0)
                {
                        if (HasRepeatGene(genes, index) == false)
                        {
                                genes.add(new Gene(strGenes[i], index));
                        }
                }
            }
            ArrayList<Gene> tempgenes = new ArrayList<Gene>();
            for (int i = 0; i < genes.size(); i ++)
            {
                Gene tempgene = genes.get(i);
                for (int j = 0; j <geneName.size(); j ++)
                {
                    if (tempgene.Name.equals(geneName.get(j)))
                    {
                        tempgenes.add(tempgene);
                        break;
                    }
                    String tstr = "";
                    if(aliasname.contains(tempgene.Name)){
                        tstr = genevalue.get(aliasname.indexOf(tempgene.Name));
                    }
                     if (tstr.equals(geneName.get(j)))
                    {
                       // Gene mgene = new Gene();
                       // mgene.Name = tstr;
                        tempgenes.add(tempgene);
                        break;
                    }
                }
            }    
            return tempgenes;
            //return genes;
	}

    //Check to see if there are duplicates in input
	private boolean HasRepeatGene(ArrayList<Gene> genes, int index)
	{
		if (genes.isEmpty())
		{
			return false;
		}

		for (int j = 0 ; j < genes.size() ; j ++ )
		{
			if ( genes.get(j).Id == index)
			{
				return true;
			}
		}
		return false;
	}


	public ArrayList<MatchedPhenotypeEntry> SearchByGenes(ArrayList<Gene> genes, int AlgID) throws IOException {
		// 0.1 Sorting the input genes.
		ContentComparatorGeneAscend comp1 = new ContentComparatorGeneAscend();
		Collections.sort(genes, comp1);

		// 1. Preparing the result array.
		ArrayList<MatchedPhenotypeEntry> results = new ArrayList<MatchedPhenotypeEntry>();

		// 3. geneScores has been loaded
		float[] geneScoreList = GetGeneProfile(genes);
                float [] gScore = getGeneScoreList(geneScoreList, PHENOTYPE_IN_ASSOCIATION_CNT, wMatrix, wIndex);
               
		String line = null;
		// if Laplacian or Correlation Coefficients
		if (AlgID == 1 || AlgID == 2) {
			

			// 6. put the phenotype scores into a two dimension array and call
			// the Laplacian Score or CC
			for (int i = 0; i < PHENOTYPE_CNT; i++) {
				if (AlgID == 1) {
					results.add(new MatchedPhenotypeEntry(phenotypes.get(i), getLaplacianScore(geneScoreList, phenotypeMatrix[i], wMatrix, wIndex), id));
				} else if (AlgID == 2) {
					results.add(new MatchedPhenotypeEntry(phenotypes.get(i), getCorrCoeff(phenotypeMatrix[i], gScore), id));
				}
			}
                        
			if (AlgID == 1) {
				ContentComparatorAscend comp = new ContentComparatorAscend();
				Collections.sort(results, comp);
			} else if (AlgID == 2) {
				ContentComparatorDescend comp = new ContentComparatorDescend();
				Collections.sort(results, comp);
			}
			return results;
			// Ridge Regression
		} else if (AlgID == 3) {
			float[][] RidgeRegressionMatrix = ContextManager_new.getRidgeRegressionMatrix_p(id);
			for (int i = 0; i < PHENOTYPE_CNT; i++) {
				results.add(new MatchedPhenotypeEntry(phenotypes.get(i),getRidgeRegression(RidgeRegressionMatrix[i],geneScoreList), id));
			}
			ContentComparatorDescend comp = new ContentComparatorDescend();
			Collections.sort(results, comp);
			return results;

		}
		return null;
	}


    //Get matched phenotype
    public ArrayList<MatchedPhenotypeEntry> getDisplayPhenotype(ArrayList<MatchedPhenotypeEntry> input, int queryNum){
        ArrayList<MatchedPhenotypeEntry> result = new ArrayList<MatchedPhenotypeEntry>();
        int j = 0;
        //array of the IDs of the nodes 
        ArrayList<Integer> ids = new ArrayList<Integer>();

        for(int i=0;i<queryNum;i++){
            result.add(input.get(i));
            ids.add(input.get(i).Id);
            j = i;
        }
                
        int addNum = j;
        int maxAddNum = j+20;
        while(addNum<maxAddNum){
            if(j==input.size()){
                break;
            }
                    
            if((ids.contains(input.get(j).Id)==false) && searchGPNetwork(input.get(j).Id)==true){
                result.add(input.get(j));
                ids.add(input.get(j).Id);
                addNum++;
                j++;
            }else{
                j++;
            }
        }
                
		return result;
	}

        
    public boolean searchGPNetwork(int ID){
        boolean result = false;
        for(int i=0;i<networkEntry.size();i++){
            if(ID == networkEntry.get(i).EntryID && (networkEntry.get(i).NeighbourNum > 0)){
                result = true;
            }
        }
        return result;
    }
        
        
        
        
    public ArrayList<Gene> getUnmatched(String strReqGenes) throws IOException{

            strReqGenes = strReqGenes.toUpperCase().trim();

            ArrayList<Gene> genes = new ArrayList<Gene>();

    		String[] strGenes = strReqGenes.split("\r\n");

                if(strReqGenes.isEmpty()){
                    return genes;
                }

                StringTokenizer st;
                for(int i = 0 ; i < strGenes.length; i++)
		{
                    strGenes[i] = strGenes[i].trim();
                }
		for(int i = 0 ; i < strGenes.length; i++)
		{
                    st = new StringTokenizer(strGenes[i], ":");
                    if(st.countTokens() > 1){
                        continue;
                    }
                    if(strGenes[i].equals("")){
                        continue;
                    }
                    if(aliasname.indexOf(strGenes[i]) < 0){
                            genes.add(new Gene(strGenes[i], 0));
                    }
		}
		return genes;
    }

        //Get matched genes
        public ArrayList<MatchedGeneEntry> getDisplayGenotype(String strReqGenes, int queryNum)  throws IOException{

            ArrayList<MatchedGeneEntry> result = new ArrayList<MatchedGeneEntry>();
		ArrayList<Gene> queryGene = new ArrayList<Gene>();

		queryGene = ValidateGenes(strReqGenes);
                
                // To check if 20 more top genes are added besides query genes
                ArrayList<Integer> addedIDs = new ArrayList<Integer>();
                int addNum = queryNum;
		// add query genes into the result Arraylist
		for(int i=0;i<queryGene.size();i++){
			MatchedGeneEntry temp = new MatchedGeneEntry(queryGene.get(i).Name,queryGene.get(i).Id+1,0,id);
			result.add(temp);
                        addedIDs.add(queryGene.get(i).Id+1);
		}
                float[] geneScoreList = GetGeneProfile(queryGene);
                
                 // find the minimum number in the list
                 float min = geneScoreList[0];
                 
                 for(int i=0;i<geneScoreList.length;i++){
                     if(geneScoreList[i]<min){
                        min = geneScoreList[i];
                     }
                 }
                 
		while(addNum>0){
                        float maxScore = geneScoreList[0];
			 int maxIndex=0;
			 for(int j=0;j<geneScoreList.length;j++){
				 if(geneScoreList[j]>maxScore){
					 maxScore = geneScoreList[j];
					 maxIndex = j;
				 }
			 }
                         if(addedIDs.contains(maxIndex+1) == false){
                            MatchedGeneEntry temp = new MatchedGeneEntry(geneName.get(maxIndex),maxIndex+1,maxScore,id);
                            result.add(temp);
                            addNum--;
                         }
                         geneScoreList[maxIndex] = min;
		 }

		return result;
        }

        public ArrayList<Edge> getGeneEdges(ArrayList<MatchedGeneEntry> input) throws ArrayIndexOutOfBoundsException{
		ArrayList<Edge> result = new ArrayList<Edge>();
                
                 for(int i=0;i<input.size();i++){
                    
                     ArrayList<Integer> temp = geneEntry.get((input.get(i).GeneId)-1).getNeighborList();
                     for(int j=0;j<input.size();j++){
                        if(temp.contains(input.get(j).GeneId)==true){
                           Edge tempEdge = new Edge(2,input.get(j).GeneName,input.get(i).GeneName);
                           result.add(tempEdge);
                       }
                       
                    }
                }
                
		return result;
	}

        public ArrayList<MatchedGeneEntry> displayedQueryGene(String strReqGenes, ArrayList<MatchedGeneEntry> input) throws IOException{
		// 1. Split string into string array, in which gene names are stored.
                //prepare the input of SearchByGenes
                ArrayList<MatchedGeneEntry> query = new ArrayList<MatchedGeneEntry>();
    		ArrayList<Gene> queryGene = ValidateGenes(strReqGenes);
                
                for(int i = 0; i < queryGene.size(); i++){
                    for(int j = 0; j < input.size(); j++){
                        if(input.get(j).GeneName.equalsIgnoreCase(queryGene.get(i).Name)){
                            MatchedGeneEntry temp = new MatchedGeneEntry(queryGene.get(i).Name,queryGene.get(i).Id+1,0, id);
                            query.add(temp);
                        }
                    }
                }
                
            return query;
        }
        
        public ArrayList<MatchedGeneEntry> displayedNonQueryGene(ArrayList<MatchedGeneEntry> query, ArrayList<MatchedGeneEntry> input){
            ArrayList<MatchedGeneEntry> result = new ArrayList<MatchedGeneEntry>();
            
            for(int i = 0; i < input.size(); i ++){
                for(int j = 0; j < query.size(); j++){
                    if(input.get(i).GeneName.equalsIgnoreCase(query.get(j).GeneName)){
                        break;
                    }
                    if(j == (query.size() - 1) && input.get(i).GeneName.equalsIgnoreCase(query.get(j).GeneName) == false){
                        result.add(input.get(i));
                    }
                }
            }
            return result;
        }
        //add by hy at 20121128
        public GANDP deleteUnReachableGenesAndPhenotypes(ArrayList<MatchedGeneEntry> query, ArrayList<MatchedGeneEntry> nonquery, ArrayList<MatchedPhenotypeEntry> mpe, ArrayList<Edge> geneEdge, ArrayList<Edge> phenotypeEdge, ArrayList<Edge> networkEdge)
        {
            //store the final gene and phenotype set
            ArrayList<MatchedGeneEntry> result_gene = new ArrayList<MatchedGeneEntry>();
            ArrayList<MatchedPhenotypeEntry> result_phen = new ArrayList<MatchedPhenotypeEntry>();

            //
            ArrayList<MatchedGeneEntry> g_temp = new ArrayList<MatchedGeneEntry>();
            ArrayList<MatchedPhenotypeEntry> p_temp = new ArrayList<MatchedPhenotypeEntry>();

            //store the query used for each loop
            ArrayList<MatchedGeneEntry> m_gene = new ArrayList<MatchedGeneEntry>();
            ArrayList<MatchedPhenotypeEntry> m_phenotype = new ArrayList<MatchedPhenotypeEntry>();

            //store the rest genes and phenotypes
            ArrayList<MatchedGeneEntry> restGene = nonquery;
            ArrayList<MatchedPhenotypeEntry> restPhenotype = mpe; 

            int i,j,k;
            int mflag = 0;
            for(i=0; i<query.size(); i++){
                for(j=0; j<geneEdge.size(); j++)
                {
                    if((query.get(i).GeneName.equals(geneEdge.get(j).source))||(query.get(i).GeneName.equals(geneEdge.get(j).target))){
                        result_gene.add(query.get(i));
                        g_temp.add(query.get(i));
                        m_gene.add(query.get(i));
                        mflag = 1;
                        break;
                    }
                }
                if(mflag == 0){
                    for(k=0; k<networkEdge.size(); k++)
                    {
                        if(query.get(i).GeneName.equals(networkEdge.get(k).source))
                        {
                            result_gene.add(query.get(i));
                            g_temp.add(query.get(i));
                            m_gene.add(query.get(i));
                            break;
                        }
                    }
                }
                mflag = 0;   
            }
            
            /*for(i=0; i<query.size(); i++){
                result_gene.add(query.get(i));
                g_temp.add(query.get(i));
                m_gene.add(query.get(i));
            }*/
            //int a =0;
            while(g_temp.size()!=0 || p_temp.size()!=0)
            {
                    m_gene = g_temp;
                    m_phenotype = p_temp;
                    g_temp = new ArrayList<MatchedGeneEntry>();
                    p_temp = new ArrayList<MatchedPhenotypeEntry>();;
                    boolean found = false;
                    for(i=0; i<m_gene.size(); i++)
                    {
                            for(j=0; j<restGene.size(); j++)
                            {
                                for(k=0; k<geneEdge.size(); k++)
                                {
                                    if((m_gene.get(i).GeneName.equals(geneEdge.get(k).source)
                                    && restGene.get(j).GeneName.equals(geneEdge.get(k).target))
                                    ||(m_gene.get(i).GeneName.equals(geneEdge.get(k).target)
                                    && restGene.get(j).GeneName.equals(geneEdge.get(k).source))){
                                            found = true;
                                            break;
                                    }
                                }
                                if(found == true){
                                    for(k=0; k<result_gene.size(); k++){
                                        if(result_gene.get(k).GeneName.equals(restGene.get(j).GeneName)){
                                            found = false;
                                            break;
                                        }
                                    }
                                    if(found == true){
                                        g_temp.add(restGene.get(j));
                                        result_gene.add(restGene.get(j));
                                        found = false;
                                    }

                                }
                            }
                            found = false;
                            for(j=0; j<restPhenotype.size(); j++)
                            {
                                for(k=0; k<networkEdge.size(); k++)
                                {
                                    if(m_gene.get(i).GeneName.equals(networkEdge.get(k).source)
                                    && restPhenotype.get(j).OMIM_ID.equals(networkEdge.get(k).target))
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if(found == true){
                                    for(k=0; k<result_phen.size(); k++){
                                        if(result_phen.get(k).OMIM_ID.equals(restPhenotype.get(j).OMIM_ID)){
                                            found = false;
                                            break;
                                        }
                                    }
                                    if(found == true){
                                        p_temp.add(restPhenotype.get(j));
                                        result_phen.add(restPhenotype.get(j));
                                        found = false;
                                    }
                                }
                            }
                    }



                    for(i=0; i<m_phenotype.size(); i++)
                    {
                            for(j=0; j<restPhenotype.size(); j++)
                            {
                                    for(k=0; k<phenotypeEdge.size(); k++)
                                    {
                                            if((m_phenotype.get(i).OMIM_ID.equals(phenotypeEdge.get(k).source)
                                            && restPhenotype.get(j).OMIM_ID.equals(phenotypeEdge.get(k).target))
                                            ||(m_phenotype.get(i).OMIM_ID.equals(phenotypeEdge.get(k).target)
                                            && restPhenotype.get(j).OMIM_ID.equals(phenotypeEdge.get(k).source))){
                                                    found = true;
                                            }
                                    }
                                    if(found == true){
                                            for(k=0; k<result_phen.size(); k++){
                                                if(result_phen.get(k).OMIM_ID.equals(restPhenotype.get(j).OMIM_ID)){
                                                    found = false;
                                                    break;
                                                }
                                            }
                                            if(found == true){
                                                p_temp.add(restPhenotype.get(j));
                                                result_phen.add(restPhenotype.get(j));
                                                found = false;
                                            }  
                                    }
                            }
                            found = false;
                            for(j=0; j<restGene.size(); j++)
                            {
                                    for(k=0; k<networkEdge.size(); k++)
                                    {
                                            if(m_phenotype.get(i).OMIM_ID.equals(networkEdge.get(k).target)
                                            && restGene.get(j).GeneName.equals(networkEdge.get(k).source)){
                                                    found = true;
                                            }
                                    }
                                    if(found == true){
                                            for(k=0; k<result_gene.size(); k++){
                                                if(result_gene.get(k).GeneName.equals(restGene.get(j).GeneName)){
                                                    found = false;
                                                    break;
                                                }
                                            }
                                            if(found == true){
                                                 g_temp.add(restGene.get(j));
                                                result_gene.add(restGene.get(j));
                                                found = false;
                                            }
                                           
                                    }
                            }
                    }
                    ArrayList<Edge> temp = new ArrayList<Edge>();
                    found = false;
                    for(i=0; i<geneEdge.size(); i++){
                        for(j=0; j<m_gene.size(); j++){
                            if(geneEdge.get(i).source.equals(m_gene.get(j).GeneName)||geneEdge.get(i).target.equals(m_gene.get(j).GeneName)){
                                found = true;
                                break;
                            }
                        }
                        if(found == false){
                            temp.add(geneEdge.get(i));
                        }
                        else{
                            found = false;
                        }
                    }
                    geneEdge = temp;
                    temp = new ArrayList<Edge>();
                    for(i=0; i<phenotypeEdge.size(); i++){
                        for(j=0; j<m_phenotype.size(); j++){
                            if(phenotypeEdge.get(i).source.equals(m_phenotype.get(j).OMIM_ID)||phenotypeEdge.get(i).target.equals(m_phenotype.get(j).OMIM_ID)){
                                found = true;
                                break;
                            }
                        }
                        if(found == false){
                            temp.add(phenotypeEdge.get(i));
                        }
                        else{
                            found = false;
                        }
                    }
                    phenotypeEdge = temp;
                    temp = new ArrayList<Edge>();
                    for(i=0; i<networkEdge.size(); i++){
                        for(j=0; j<m_phenotype.size(); j++){
                            if(networkEdge.get(i).target.equals(m_phenotype.get(j).OMIM_ID)){
                               found = true;
                               break;
                            }
                        }
                        for(j=0; j<m_gene.size(); j++){
                            if(networkEdge.get(i).source.equals(m_gene.get(j).GeneName)){
                                found = true;
                                break;
                            } 
                        }
                        if(found == false){
                            temp.add(networkEdge.get(i));
                        }
                        else{
                            found = false;
                        }
                    }
                    networkEdge = temp;
                    temp = new ArrayList<Edge>();
            }//end while
            GANDP gp = new GANDP();
            gp.mge = result_gene;
            gp.mpe = result_phen;
           // return result_gene;
            return gp;
        }
        public ArrayList<MatchedGeneEntry> mdeleteAloneGenes(ArrayList<Gene> genes, ArrayList<MatchedGeneEntry> input, ArrayList<Edge> geneEdge, ArrayList<Edge> networkEdge){
            ArrayList<MatchedGeneEntry> result = new ArrayList<MatchedGeneEntry>();
            int dimGene = input.size();
            int querysize = genes.size();
            MyMatrix geneMatrix = new MyMatrix(dimGene, dimGene);
            MyMatrix geneMatrix1 = new MyMatrix(dimGene, dimGene);
            //MyMatrix tempgeneMatrix = new MyMatrix(dimGene, dimGene);
            MyMatrix geneMatrix_sum = new MyMatrix(dimGene, dimGene);
            int i,j,k;  
            for(i=0;i<dimGene;i++){
                for(j=0; j<dimGene; j++){
                    for(k=0;k<geneEdge.size();k++){
                        if((input.get(i).GeneName.equals(geneEdge.get(k).source)
                            && input.get(j).GeneName.equals(geneEdge.get(k).target))){
                            geneMatrix.m_matrix[i][j] = 1;
                            geneMatrix.m_matrix[j][i] = 1;
                            geneMatrix1.m_matrix[i][j] = 1;
                            geneMatrix1.m_matrix[j][i] = 1;
                            geneMatrix_sum.m_matrix[i][j] = 1;
                            geneMatrix_sum.m_matrix[j][i] = 1;
                            break;
                        }
                    }
                }
            }
            int count = 0;
            while(count<3){
                geneMatrix.mulMatrix(geneMatrix1);
                geneMatrix_sum.addMatrix(geneMatrix);
                count ++;
            }
            int[] m_gene = new int[dimGene];
            for(i=0; i<dimGene; i++){
                for(j=0; j<querysize; j++){
                    m_gene[i] += geneMatrix_sum.m_matrix[j][i];
                }
            }
            for(i=0; i< querysize; i++){
                result.add(input.get(i));
            }
            for(i=querysize; i<dimGene; i++){
                if(m_gene[i] > 0)
                {
                    result.add(input.get(i));
                }
            }
            return result;
        }
        
        public ArrayList<MatchedPhenotypeEntry> mdeleteAlonePhenotype(ArrayList<MatchedGeneEntry> matchedGenes, ArrayList<MatchedPhenotypeEntry> input, ArrayList<Edge> phenotypeEdge, ArrayList<Edge> networkEdge){
            ArrayList<MatchedPhenotypeEntry> result = new ArrayList<MatchedPhenotypeEntry>();
            int i,j,k;
            int flag = 0;
            ArrayList<MatchedPhenotypeEntry> tempPhen = new ArrayList<MatchedPhenotypeEntry>();
            for(j=0; j<input.size(); j++){
                for(i=0; i<matchedGenes.size(); i++){
                    for(k=0; k<networkEdge.size(); k++){
                        if(matchedGenes.get(i).GeneName.equals(networkEdge.get(k).source)&&input.get(j).OMIM_ID.equals(networkEdge.get(k).target) ){
                            tempPhen.add(input.get(j));
                            flag = 1;
                            break;
                        }
                    }
                    if(flag == 1)
                    {
                        flag = 0;
                        break;
                    }
                }
            }
            int dimPhen = input.size();
            MyMatrix phenMatrix = new MyMatrix(dimPhen, dimPhen);
            MyMatrix phenMatrix1 = new MyMatrix(dimPhen, dimPhen);
            MyMatrix phenMatrix_sum = new MyMatrix(dimPhen, dimPhen);
            for(i=0;i<dimPhen;i++){
                for(j=0; j<dimPhen; j++){
                    for(k=0;k<phenotypeEdge.size();k++){
                        if((input.get(i).OMIM_ID.equals(phenotypeEdge.get(k).source)
                            && input.get(j).OMIM_ID.equals(phenotypeEdge.get(k).target))){
                            phenMatrix.m_matrix[i][j] = 1;
                            phenMatrix.m_matrix[j][i] = 1;
                            phenMatrix1.m_matrix[i][j] = 1;
                            phenMatrix1.m_matrix[j][i] = 1;
                            phenMatrix_sum.m_matrix[i][j] = 1;
                            phenMatrix_sum.m_matrix[j][i] = 1;
                            break;
                        }
                    }
                }
            }
            int count = 0;
            while(count<3){
                phenMatrix.mulMatrix(phenMatrix1);
                phenMatrix_sum.addMatrix(phenMatrix);
                count ++;
            }
            int[] m_phen = new int[dimPhen];
            for(i=0; i<dimPhen; i++){
                for(j=0; j<tempPhen.size(); j++){
                    m_phen[i] += phenMatrix_sum.m_matrix[j][i];
                }
            }
            for(i=0; i<tempPhen.size(); i++){
                result.add(input.get(i));
            }
            for(i=tempPhen.size(); i<dimPhen; i++){
                if(m_phen[i] > 0)
                {
                    result.add(input.get(i));
                }
            }
            //result = tempPhen;
            return result;
        }
        
       public ArrayList<Edge> updateGG(ArrayList<Edge> GG, ArrayList<MatchedGeneEntry> matchedgenes){
            int i,j,k;
            ArrayList<Edge> resultGG = new ArrayList<Edge>();
            int flag = 0;
            for(i=0; i<GG.size(); i++){
                for(j=0; j<matchedgenes.size(); j++){
                    if(GG.get(i).source.equals(matchedgenes.get(j).GeneName)){
                        flag = 1;
                        for(k=0; k<matchedgenes.size(); k++){
                            if(GG.get(i).target.equals(matchedgenes.get(k).GeneName)){
                                resultGG.add(GG.get(i));
                                break;
                            }
                        }
                        if(flag == 1){
                            flag = 0;
                            break;
                        }
                    }
                }
            }
            return resultGG;
        }
        public ArrayList<Edge> updateGP(ArrayList<Edge> GP, ArrayList<MatchedGeneEntry> matchedgenes, ArrayList<MatchedPhenotypeEntry> mpe){
            int i,j,k;
            ArrayList<Edge> resultGP = new ArrayList<Edge>();
            int flag = 0;
            for(i=0; i<GP.size(); i++){
                for(j=0; j<matchedgenes.size(); j++){
                    if(GP.get(i).source.equals(matchedgenes.get(j).GeneName)){
                        flag = 1;
                        for(k=0; k<mpe.size(); k++){
                            if(GP.get(i).target.equals(mpe.get(k).OMIM_ID)){
                                resultGP.add(GP.get(i));
                                break;
                            }
                        }
                        if(flag == 1){
                            flag = 0;
                            break;
                        }
                    }
                }
            }
            GP = resultGP;
            return GP;
        }
        public ArrayList<Edge> updatePP(ArrayList<Edge> PP, ArrayList<MatchedPhenotypeEntry> mpe){
            int i,j,k;
            ArrayList<Edge> resultPP = new ArrayList<Edge>();
            int flag = 0;
            for(i=0; i<PP.size(); i++){
                for(j=0; j<mpe.size(); j++){
                    if(PP.get(i).source.equals(mpe.get(j).OMIM_ID)){
                        flag = 1;
                        for(k=0; k<mpe.size(); k++){
                            if(PP.get(i).target.equals(mpe.get(k).OMIM_ID)){
                                resultPP.add(PP.get(i));
                                break;
                            }
                        }
                        if(flag == 1){
                            flag = 0;
                            break;
                        }
                    }
                }
            }
            PP = resultPP;
            return PP;
        }
        public ArrayList<MatchedGeneEntry> deleteAloneGenes(ArrayList<MatchedGeneEntry> input, ArrayList<Edge> geneEdge, ArrayList<Edge> networkEdge){
            ArrayList<MatchedGeneEntry> result = new ArrayList<MatchedGeneEntry>();
            
            for(int i=0;i<input.size();i++){
                
                boolean found = false;
                
                for(int j=0;j<geneEdge.size();j++){
                    if(input.get(i).GeneName.equals(geneEdge.get(j).source)
                         || input.get(i).GeneName.equals(geneEdge.get(j).target)){
                        found = true;
                    }
                }
                
                for(int k=0;k<networkEdge.size();k++){
                    if(input.get(i).GeneName.equals(networkEdge.get(k).source) 
                            || input.get(i).GeneName.equals(networkEdge.get(k).target)){
                        found = true;
                    }
                }
                
                if(found == true){
                    result.add(input.get(i));
                }
            }
            
            return result;
        }
        
         public ArrayList<MatchedPhenotypeEntry> deleteAlonePhenotype(ArrayList<MatchedPhenotypeEntry> input, ArrayList<Edge> phenotypeEdge, ArrayList<Edge> networkEdge){
             ArrayList<MatchedPhenotypeEntry> result = new ArrayList<MatchedPhenotypeEntry>();
             
            for(int i=0;i<input.size();i++){
                
                boolean found = false;
                
                for(int j=0;j<phenotypeEdge.size();j++){
                    if(input.get(i).OMIM_ID.equals(phenotypeEdge.get(j).source)
                         || input.get(i).OMIM_ID.equals(phenotypeEdge.get(j).target)){
                        found = true;
                    }
                }
                
                for(int k=0;k<networkEdge.size();k++){
                    if(input.get(i).OMIM_ID.equals(networkEdge.get(k).source) 
                            || input.get(i).OMIM_ID.equals(networkEdge.get(k).target)){
                        found = true;
                    }
                }
                
                if(found == true){
                    result.add(input.get(i));
                }
            }
            
            return result;
        }
        
        public ArrayList<Edge> getNetworkEdges(ArrayList<MatchedGeneEntry> input_g, ArrayList<MatchedPhenotypeEntry> input_p) throws ArrayIndexOutOfBoundsException{
		ArrayList<Edge> result = new ArrayList<Edge>();
                
                for(int i=0;i<input_p.size();i++){
                    
                    ArrayList<Integer> temp = networkEntry.get((input_p.get(i).Id)).getNeighborList();
                    
                   for(int j=0;j<input_g.size();j++){
                       
                       if(temp.contains(input_g.get(j).GeneId)==true){
                           Edge tempEdge = new Edge(2,input_g.get(j).GeneName,input_p.get(i).OMIM_ID);
                           result.add(tempEdge);
                       }
                       
                    }
                }
                
		return result;
	}

	public boolean isEntryExist(MatchedGeneEntry input_g, MatchedPhenotypeEntry input_p){
		boolean result = false;

		for(int i=0;i<networkEntry.size();i++){
			if((input_p.Id == networkEntry.get(i).EntryID) && ((networkEntry.get(i).findNeighbor(input_g.GeneId))==true)){
				result = true;
			}
		}

		return result;
	}

        public ArrayList<Edge> getPhenotypeEdges(ArrayList<MatchedPhenotypeEntry> input) throws ArrayIndexOutOfBoundsException
        {
		ArrayList<Edge> result = new ArrayList<Edge>();

                for(int i=0;i<input.size();i++){
                    
                    ArrayList<Integer> temp = phenotypeEntry.get((input.get(i).Id)).getNeighborList();
                  for(int j=0;j<input.size();j++){
                       
                       if(temp.contains(input.get(j).Id+1)==true){
                           Edge tempEdge = new Edge(2,input.get(j).OMIM_ID,input.get(i).OMIM_ID);
                           result.add(tempEdge);
                       }
                       
                    }
                }
                
		return result;
	}


	public float getLaplacianScore(float [] geneScoreList,float [] phenotypeScoreList, float []wMatrix, int [][] wIndex){
		float LaplacianScore = 0;
		float meanPhenotype = getMean(phenotypeScoreList);
		float meanGene = getMean(geneScoreList);
		float sdPhenotype = getStandardDeviation (phenotypeScoreList);
		float sdGene = getStandardDeviation (geneScoreList);
		int n = geneScoreList.length;
		int m = phenotypeScoreList.length;

		float [] geneScoreList_z = new float [n];
		float [] phenotypeScoreList_z = new float [m];
		      
		for(int i=0;i<n;i++){
			geneScoreList_z[i] = (geneScoreList[i] - meanGene) / sdGene;
		}
			   
		for(int i=0; i<m; i++){
			phenotypeScoreList_z[i] = (phenotypeScoreList[i] - meanPhenotype)/sdPhenotype;
		}
		
		for ( int i = 0 ; i < wMatrix.length ; i ++ ){
			// in wIndex matrix, 0 : phenotype, 1: gene.
			int phen_idx = wIndex[i][0] - 1;
			int gene_idx = wIndex[i][1] - 1;
			LaplacianScore += wMatrix[i] * (geneScoreList_z[gene_idx] - phenotypeScoreList_z[phen_idx])*(geneScoreList_z[gene_idx] - phenotypeScoreList_z[phen_idx]);
		}
                if (Float.isNaN(LaplacianScore))
                {
                    LaplacianScore = 100000;
                }
		return LaplacianScore;         
	}
        
        
        private static Logger log = Logger.getLogger(PhenotypeFinder.class);
        
        public float [] getGeneScoreList(float [] geneScoreList, int length, float []wMatrix, int [][] wIndex){
            float [] gScore = new float [wMatrix.length];
            for(int i=0; i<wMatrix.length; i++){
                gScore[wIndex[i][0]-1] += geneScoreList[wIndex[i][1]-1] * wMatrix[i];
            }  
            return gScore;
        }
        
        
        public float getCorrCoeff(float [] phenotypeScoreList,float [] geneScoreList){
            float CC = 0;
            float meanGene = getMean(geneScoreList);
            float meanPhenotype = getMean(phenotypeScoreList);
            int m = phenotypeScoreList.length;
            if(meanPhenotype==0 || meanGene==0){
                return CC;
            }else{
                for(int i = 0 ; i < m ; i ++){
                    CC += (geneScoreList[i] - meanGene)*(phenotypeScoreList[i] - meanPhenotype);
                }
                CC /= (getStandardDeviation(geneScoreList)*getStandardDeviation(phenotypeScoreList)*m);
                return CC;
            }
	}
        
        
        /*
	public float getCorrCoeff(float [] phenotypeScoreList,float [] geneScoreList, float []wMatrix, int [][] wIndex){
		float CC = 0;
		float meanPhenotype = getMean(phenotypeScoreList);
		float meanGene = getMean(geneScoreList);

		if(meanPhenotype==0 || meanGene==0){
			return CC;
		}else{
			int n = phenotypeScoreList.length;
			int m = geneScoreList.length;
			float [] gScore = new float [n];
                        
                        
			for(int i=0; i<wMatrix.length; i++){

				gScore[wIndex[i][0]-1] += geneScoreList[wIndex[i][1]-1] * wMatrix[i];

			}

			meanGene = getMean(gScore);
			for(int i=0;i<n;i++){
				CC += (phenotypeScoreList[i] - meanPhenotype)*(gScore[i] - meanGene);
			}
			CC /= (getStandardDeviation(phenotypeScoreList)*getStandardDeviation(gScore)*n);
                        
			return CC;
		}
	} */

	public float getRidgeRegression ( float[] inverseScoreList,float [] geneScoreList){
        float RR = 0;
        int n = inverseScoreList.length;
        for (int i=0; i<n; i++){
            RR += inverseScoreList[i]*geneScoreList[i];
        }
        return RR;
	}

	public float getStandardDeviation ( float[] data ){
        int n = data.length;
        float mean = getMean(data);
        float sum = 0;
        for ( int i=0; i<n; i++ ) {
            float v = data[i] - mean;
            sum += v * v;
        }
        return (float)Math.sqrt( sum / n );
    }

    public float getMean ( float[] data){
        float mean = 0;
        int n = data.length;
        for ( int i=0; i<n; i++ ){
            mean += data[i];
        }
        mean /= n;
        return mean;
    }

    public float[] GetGeneProfile(ArrayList<Gene> genes)
    {
        // 3.2 put the gene score into a one dimension array
        int QueryGeneSize = genes.size();
        int col_g = GENE_IN_ASSOCIATION_CNT;
        float[] geneScoreList=new float[col_g];
        float temp_score;

        for(int i = 0 ; i < col_g; i ++ ){
            temp_score = 0;
            for(int j = 0 ; j < QueryGeneSize ; j ++ ){
               /*
                if(genes.get(j).Id < 0 || genes.get(j).Id >1326){
                   continue;
                }else{
                   temp_score += GeneMatrix[genes.get(j).Id][i];
               }*/
                temp_score += GeneMatrix[genes.get(j).Id][i];
            }
            geneScoreList[i]=temp_score/QueryGeneSize;   
        }
        return geneScoreList;
    }
        
}
